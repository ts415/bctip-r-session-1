---
title: 'BCTIP Training: R Session I, May 2022'
author: "Tyler Schappe, Tina Davenport"
date: "5/27/22"
output:
  html_document:
    toc: yes
    toc_float: 
      collapsed: TRUE
      smooth_scroll: TRUE
    toc_depth: '4'
    number_sections: TRUE
    df_print: paged
    keep_md: TRUE
---


# A Brief Tour of RStudio

## Git in RStudio

# Some R Basics

- Object-oriented language
  + Objects generally "live" in the global environment (in memory)
  + Objects have attributes and can have "slots"
  + Basic object types:
    - Vector
    - Matrix
    - Dataframe
    - List
    - Function
  + Basic data types:
    - Numeric
    - Integer
    - Character
    - Factor
    - Date
    - Special types
- Memory-based
  + Does not save object to disk by default
  + Need to be intentional about saving
- R packages/libraries extend functionality of base R

# Note on the Bifurcation of Programming Styles Within R

R has **a lot** of built-in functionality from efficient and well-curated functions. However, the R syntax is somewhat counter-intuitive and verbose. In response, a programmer named Hadley Wickham created Tidyverse, a set of packages that enables alternative syntax, among other functionality. In particular, the "pipe" operator %>% enables output from one function to be input into a subsequent function. This generally results in simpler and more readable code. 

While Tidyverse is intuitive and has many advantages, being comfortable with **both** tidyverse and "base R" has advantages:

- Functions from many packages do not use tidyverse
- Scripts that you may inherit from other programmers may not use tidyverse
- Base R can be more flexible and can sometimes be faster computationally
- You will have more tools to approach difficult problems in different ways

# Setup

Source functions file
```{r}
source("functions.R")
```

## Load Libraries

```{r}
library(ggplot2); library(reshape2); library(table1)
```

## Load the MPG Dataset into the Working Environment

Convert the mpg dataset from ggplot2 into a data frame object

```{r}
mpg <- as.data.frame(mpg)
```

# Examining and Manipulating Dataset Structure

The 'str' function gives information on the type of data each variable is and the first couple of observations of each. It also provides metadata about the data object itself -- in this case that it's a data frame.

```{r}
str(mpg)
```

## Converting a Variable to a Factor

We can see that the variables for manufacturer and model are currently character variables, but for later analyses we would prefer them to be factor variables -- we can convert them using the as.factor() function

```{r}
mpg$manufacturer <- as.factor(mpg$manufacturer)
mpg$model <- as.factor(mpg$model)
```

Now let's look again at the output of str()

```{r}
str(mpg)
```

We can now see that 'manufacturer' and 'model' are factors now

## Releveling Factors

Let's first look at the levels of the variable "manufacturer"

```{r}
levels(mpg$manufacturer)
```

### Set the baseline level of the factor

Audi is the baseline level because it's listed first. Let's make it Toyota instead.

```{r}
mpg$manufacturer <- relevel(mpg$manufacturer, ref="toyota")
levels(mpg$manufacturer)
```

### Set a custom order for the levels

Let's look at the levels and use their indices to re-order them according to continent that the company is based in

```{r}
#Display current levels
levels(mpg$manufacturer)

#Re-order the levels
mpg$manufacturer <- factor(
  mpg$manufacturer,
    levels=c("honda", "toyota", "nissan", "subaru", "hyundai", "audi", "land rover", "volkswagen", "chevrolet", "dodge", "ford", "jeep", "lincoln", "mercury", "pontiac")
  )
```

Look at the levels again to verify
```{r}
levels(mpg$manufacturer)
```

# Basic Data Summary

## Summary()

The summary() function gives basic summary statistics for numeric variables and length information for character variables. We don't have missing values in this dataset, but if we did, summary() would show them.

```{r}
summary(mpg)
```

## Missing Values

Let's add a missing value for city MPG to a single row to see how summary deals with it
```{r}
mpg.miss <- mpg[, c("manufacturer", "model", "cty")]
mpg.miss[12, "cty"] <- NA

summary(mpg.miss)
```

Unfortunately, summary() doesn't deal with NAs well in characters strings
```{r}
mpg.miss$manufacturer <- as.character(mpg.miss$manufacturer)
mpg.miss$manufacturer[12] <- NA

summary(mpg.miss)
```

But it works well for factors
```{r}
mpg.miss$manufacturer <- as.factor(mpg.miss$manufacturer)

summary(mpg.miss)
```

## table()

For quick cross-tablutions, the table() function works well. Let's see how many observations there are in each combination of manufacturer and year. Including "exclude = NULL" will show NA values in the table.

```{r}
table(mpg$manufacturer, mpg$year, exclude = NULL)
```

Let's use it on our dataframe with missing varibles to see how "exclude = NULL" works

```{r}
head(table(mpg.miss$model, mpg.miss$manufacturer, exclude = NULL))
```

## 'table1' Package

The 'table1' package is very handy for producing high-quality summary tables, typically Table 1 in published manuscripts.

```{r}
table1(~ cyl + displ + cty + hwy | manufacturer,
       data = mpg)
```


# Subsetting Data

Subsetting data is an extremely useful and powerful tool, and R makes it particularly easy. As with most things, there are multiple ways to accomplish the same thing.


## subset() function

The subset() function is a quick and easy way to perform subsetting, but note that the function documentation warns that it is intended for interactive use only and instead recommends the use of square brakets "[]" for programmatic use (we'll cover those below).  

### Subsetting Columns with "select ="

An easy an quick way to subset columns is with the subset() function. Let's say we want to only keep the manufacturer, model, and year columns of the dataframe. We can use "select = "

```{r}
subset(mpg, select = c(manufacturer, model, year))
```

If we want to keep more columns than we want to drop, then we can use "-" with "select ="

```{r}
subset(mpg, select = -model)
```

### Subsetting Rows with "subset ="

To subset rows based on specific criteria, we can use "subset = ". For example, let's only keep rows with city MPG > 24.

```{r}
subset(mpg, subset = cty > 24)
```

Or we can also use multiple criteria, such as city MPG > 18 and cars with 6-cylinder engines.

```{r}
subset(mpg, subset = cty > 18 & cyl == 6)
```

## Subsetting with "[]"

One alternative to the subset() function are the square brackets "[]". Within the brackets, there are two terms separated by a comma. Terms before the comma are conditional statements that will apply to rows, terms after the comma apply to columns. If you leave one term blank, no conditions are set and all rows/columns are returned.

### Single Condition on Rows

Let's say we want to find observations with city MPG \> 18. 

Let's find all observations where the city MPG \> 18

```{r}
mpg[mpg$cty > 18 , ]
```

### Single Condition on Rows, Specific Columns

Let's again find observations with city MPG \> 18 but we only want to keep the Manufacturer, Model, and year variables

```{r}
mpg[mpg$cty > 18, c("year", "manufacturer", "model")]
```

Note that if the column that the rows are conditioned on contains an NA value, then R cautiously changes the entire row of the resulting dataset to NA because it is unable to decide whether that row meets the criteria. To remove these NA rows, you can use the which() function in the logical statement. In our case, with or without which() give the same output because we don't have missing values.

```{r}
mpg[which(mpg$cty > 18), c("year", "manufacturer", "model")]
```

### Multiple Conditions on Rows

City MPG \> 18 AND manufacturer is Toyota, return all columns

```{r}
mpg[mpg$cty > 18 & mpg$manufacturer == "honda" ,]
```

City MPG \> 18 AND manufacturer is Honda OR Toyota, return all columns

```{r}
mpg[mpg$cty > 18 & (mpg$manufacturer == "honda" | mpg$manufacturer == "toyota") ,]
```

## Subsetting with a Vector

So far, when subsetting character values, we've been interested in just a single value at a time. But sometimes, we have a vector of values that we want to subset on. For example, this is very useful if your data consists of patients with unique IDs and you want to make a subset of data that contains observations corresponding to a list of specific patients.

The tool for this is the %in% operator, which finds values of one vector in another.For example, let's say we're interested in finding a subset of cars with transmissions with 6 gears. 

First, we define a vector of the transmissions we want. We can do this using grep and searching for any strings with the number 6.
```{r}
six.speed <- unique(grep('6', 
                         mpg$trans, 
                         value = T)
              )
```



Now do that actual subsetting

```{r}
mpg[mpg$trans %in% six.speed , ]
```

## Removing NAs

### With is.na()

There is a special function in R called is.na() to identify missing values that can be used to efficiently subset.

Since there are no NA values in the mpg dataset, we'll have to create our own. Let's make a subset of the mpg dataset and add a new row with NA for city MPG. Note our use of rbind() here which combines dataframes and/or vectors row-wise.

```{r}
dat.na <- as.data.frame(rbind(
  mpg[1:3, c("manufacturer", "model", "year", "cty")],
  c("toyota", "camry", 2021, NA)
))
```

Now we can subset to only include values with NA for city MPG

```{r}
dat.na[is.na(dat.na$cty) , ]
```

To remove any values with NA, we can use the "not" operator (!)

```{r}
dat.na[!is.na(dat.na$cty) , ]
```

### With complete.cases()

The complete.cases function can be used to filter out any rows that have an NA in any column, returning a NA-free dataframe

```{r}
dat.na[which(complete.cases(dat.na)) ,]
```

# Ordering and Sorting

## Sorting a Vector

The sort() function can sort a vector, returning sorted values

```{r}
head(sort(mpg$cty, decreasing = T))
```

The order function sorts but returns indices of the sorted values

```{r}
head(order(mpg$cty, decreasing = T))
```

## Sorting a Dataframe

This can be used to sort the rows of a dataframe by one or more variables. Use the "-" to indicate descending order for that variable.

```{r}
#Sort by city MPG
mpg[order(-mpg$cty) ,]

#Sort by manufacturer, then by city MPG
mpg[order(mpg$manufacturer, -mpg$cty) ,]

```

# Regular Expressions

If you've never heard of them, regular expressions (regex) are extremely powerful pattern matching tools used across programming languages. They can be cryptic, but are worth learning.

In R, there are several functions that use them, including "grep()" and "gsub()"

## Using Regex to Subset

The "grep" function is useful for this. We pass a vector to the function with strings we want to search in, and it returns a boolean vector we can use to subset a dataframe.

Let's look for any car with a manual transmission with any number of gears. This wouldn't be easily done using the previous subsetting tools we covered

```{r}
mpg[grep("manual", mpg$trans) ,]
```

## Using Regex to Derive a New Variable

gsub() is a function that substitutes character strings using regular expressions.

Let's say we want to create a new variable for the sub-type of transmission (including the number of gears that a transmission has), regardless of whether it's an automatic or manual. One way is to use gsub() to delete the characters we don't want.

### Simple Example

Let's first delete any parentheses from the transmission variable to see how gsub() works

The first argment is the regular expression pattern in single quotes we want to replace, the second is the pattern we want to replace it with in single quotes, and the third argument is the input. The square brackets act like an "or" operator, so it will match any of the characters in the bracket. One tricky thing about regular expressions is the idea of "escaping." Because there are special characters in regex (parentheses is one), we need to specify that we want to use the "standard" meaning of the character within the pattern by putting a backslash in front of it. For technical details, in R on Windows, you actually need to double escape using two consecutive backslashes. We specify that we want to replace the parentheses with nothing using blank quotes, effectively deleting them. Finally, since the variable is long, let's just look at the first couple of items in the output using the head() function.

```{r}
head(mpg$trans)

head(gsub('[\\(\\)]', '', mpg$trans))
```

### Deriving a Variable for Number of Gears in Transmission

Now, let's isolate the number of gears by removing all other characters. One nice thing about regex is that you can specify a range of values, including letters. Here, we are searching for any lowercase letter from a to z and also any opening or closing parethesis, and replacing them with nothing -- this should leave us with the numeric value for the number of gears.

```{r}
head(gsub('[a-z\\(\\)]+', '', mpg$trans))

#Save the resulting vector to our dataset as a column called "gears"
mpg$gears <- gsub('[a-z\\(\\)]+', '', mpg$trans)
```

We can see that we have a problem -- there are some transmissions without a numeric value for number of gears (so-called continuously-variable transmissions).

### Using Subsetting to Assign New Values

Previously, we were subsetting to extract and/or view rows or columns of data, but we can also assign new values to subsets of data. In this case, we know that blank rows corresponding to transmissions with no gears, so we can subset those and change their value to NA.

```{r}
mpg$gears[mpg$gears == ""] <- NA

head(mpg$gears)
```


# Dates

## Simple Examples

R can handle dates pretty well, but it can sometimes be a pain because of the number of different date formats that exist.

First, let's make some date character strings to work with

```{r}
dates1 <- c("2021-05-18", "2020-04-12", "2007-05-21")

dates2 <- c("05/18/2021", "04/12/2020", "05/21/2007")
```

First try the easiest method of as.Date()

```{r}
dates1.date <- as.Date(dates1)
```

We find that as.Date() works for the first set of dates . . .

```{r}
dates1.date[1] - dates1.date[2]
```

```{r}
# dates2.date <- as.Date(dates2)   # <-- gives an error: "Error in charToDate(x) : character string is not in a standard unambiguous format"
```

. . . but not for the second set of dates, despite being in a fairly common format. We need to tell R what format these strings are in using a format type, which can be found in the help file of the strptime() function that is running under the hood. In this scenario, we have to define our delimiters as "/" as below:

```{r}
dates2.date <- as.Date(dates2, format = "%m/%d/%Y")

dates2.date[1] - dates2.date[2]
```

## More Challenging Examples

Let's make a more challenging example to load as a date

```{r}
dates3 <- c("3/2/2021 1:00:00 PM", "3/19/2021 4:30:00 PM")
```

Let's try as.Date but it's probably going to fail

```{r}
dates3.date <- as.Date(dates3)
dates3.date
```

It didn't give an error, but it also didn't really work. We'll need to use the formats from strptime() again, this time using %I:%M:%S for hour:minute:second and %p for "AM/PM"

```{r}
dates3.date <- as.Date(dates3, format = "%m/%d/%Y %I:%M:%S %p")

dates3.date[2] - dates3.date[1]
```

## strptime()

Notice the limitation of as.Date() above -- it "rounds" the date to the nearest day. We can use strptime() to save a more accurate version of these dates.

```{r}
dates3.strptime <- strptime(dates3, format = "%m/%d/%Y %I:%M:%S %p")

dates3.strptime[2] - dates3.strptime[1]
```

# Reshaping Datasets

There are several tools for reshaping datasets from wide to long format. I will focus on the 'reshape2' package, which is pretty intuitive.

## To Long Format

Let's say that we want a single column for MPG values with another column to indicate city vs highway. This is often the format needed for modeling and for plotting in ggplot (we'll cover that later).

We specify city and highway MPG as the "measure" variables (whose values we want to stack vertically) and other variables of interest as "ID" variables which will be separate columns to help make sense of the output dataset (these aren't actually required in this scenario)

```{r}
mpg.cty.hwy <- melt(mpg, id.vars = c("manufacturer", "model", "year", "cyl", "trans"), measure.vars = c("cty", "hwy"), value.name = "mpg", variable.name = "mpg.type")

head(mpg.cty.hwy)
```

The output should have 2x as many rows as the input dataframe -- check that.

```{r}
nrow(mpg.cty.hwy) / nrow(mpg)
```

## Long to Wide

Working off of the MPG dataset, let's make a wide version with year on the columns and city MPG in the cells.

The first step is to create a molten dataframe. Here, we want city MPG as the measure/value variable and we also keep all other identifying variables as "ID" variables.

```{r}
mpg.melt <- melt(mpg, measure.vars = "cty", id.vars = c("manufacturer", "model", "displ", "cyl", "trans", "drv", "fl", "class", "year"))

head(mpg.melt)
```

Now we can cast the molten (long) format into a dataframe with our desired shape -- anything to the right of the \~ will be on the columns of the dataframe. The measure variable is implicitly city MPG because that's how we created the molten dataframe. Note that we need to use an aggregate function to deal with repeated values in case there are any (which there are) -- we choose to simply average their values. If there weren't repeated values, this would give us a clean wide dataframe.

```{r}
mpg.year.wide <- dcast(mpg.melt, manufacturer + model + displ + cyl + trans + drv + fl + class ~ year, fun.aggregate = mean)

head(mpg.year.wide)
```

We could also accomplish the same task with one line using recast()

```{r}
mpg.year.wide2 <- recast(mpg, manufacturer + model + displ + cyl + trans + drv + fl + class ~ year, measure.var = "cty", fun.aggregate = mean)

identical(mpg.year.wide, mpg.year.wide2)
```

# Figures: ggplot2

ggplot2 is a very nice plotting tool and very widely used.

## qplot

qplot is the fastest way to make a plot in ggplot2 -- it's mostly meant for quick checks, but not for final polished figures (as you can see below)

Let's make a scatterplot of displacement vs city MPG

```{r}
qplot(displ, cty, data = mpg)
```

A boxplot of city MPG among manufacturers

```{r}
qplot(manufacturer, cty, geom = "boxplot", data = mpg)
```

We can see that these figures could use some improvements -- that will be our next topic.

## Polished figures with ggplot() function

For more polished figures in ggplot2, the best method is the generic ggplot() function. When building plots, ggplot2 takes a layered approach -- different components are built on top of each other graphically and this is reflected in both the code structure and visually in how objects are overlayed.

There are simply too many options in ggplot to cover comprehensively, so I'll try to cover the most common and useful options that I've encountered in the examples below.

### Polished scatterplot

For the plot below, we use the following options:

ggplot() builds the base figure (axes and panel) and sets the "aesthetic" variables which will be shown visually.

geom_point() builds the points on top of the base figure and inherets the aesthetics. We specify size = 1.2 to make the dots smaller than default. We don't include it in aes() to avoid creating a legend.

scale_x\_continuous() allows for customization of a continuous x axis -- here we're setting the breaks for the labels to be every 0.25 L

theme_bw() makes the overall plot have a black and white look, good for publications

theme() allows customization of many different aspects of the plot -- here, we're setting the x-axis text to be size 8 (smaller than default) and getting rid of the minor grid lines that occur between two x-axis ticks

xlab() and ylab() set the axis labels

labs(title = "") creates a nice looking title and also has the option for a subtitle if needed

Note that the plot is being saved as an object for later use, but also the whole block of code is wrapped in (), which also displays the output of the code.

```{r}
(displ.cty.scatter <- ggplot(mpg, aes(x=displ, y=cty))+
  geom_point(size = 1.2)+
  scale_x_continuous(
    breaks = seq.default(from=0, to=10, by=0.25)
  ) +
  theme_bw()+
  theme(
    axis.text.x = element_text(size=8),
    panel.grid.minor.x = element_blank()
  ) +
  xlab("Engine displacement (L)")+
  ylab("City MPG")+
  labs(title = "Engine Displacement vs. City MPG")
)
```

Add a linear regression line with geom_smooth()

```{r}
(displ.cty.scatter <- ggplot(mpg, aes(x=displ, y=cty))+
  geom_point(size = 1.2)+
  scale_x_continuous(
    breaks = seq.default(from=0, to=10, by=0.25)
  ) +
  theme_bw()+
  theme(
    axis.text.x = element_text(size=8),
    panel.grid.minor.x = element_blank()
  ) +
  xlab("Engine displacement (L)")+
  ylab("City MPG")+
  labs(title = "Engine Displacement vs. City MPG")+
  geom_smooth(method = "lm")
)
```

Now, let's save the plot to our local machine with ggsave() -- there are options for PDF, JPG, PNG, and more.

```{r}
ggsave(displ.cty.scatter, file = "C:\\Users\\ts415\\Documents\\displ.cty.scatter.pdf", width=8, height=5)

ggsave(displ.cty.scatter, file = "C:\\Users\\ts415\\Documents\\displ.cty.scatter.jpg", width=8, height=5)

ggsave(displ.cty.scatter, file = "C:\\Users\\ts415\\Documents\\displ.cty.scatter.png", width=8, height=5)
```

#### Adding Complexity

Let's add some complexity by stratifying the plot by both year and manufacturer. We'll start with the same base code, but include:

geom_point(aes(color = as.factor(year))) to differentiate years by color and add a lengend. Note that year wasn't considered a factor, so we have to convert it on the fly within the call to geom_point(). This results in the label in the legend including "as.factor(year)", so we'll manually change that below with scale_color_discrete(name = "Year").

breaks = seq.default(from=0, to=10, by=0.5) to make the x-axis ticks every 0.5

scale_color_discrete(name = "Year") to manually change the title of the legend for the color aesthetic.

axis.text.x = element_text(size=6, angle=90, hjust=1, vjust=0.4) to turn the axis text sideways and line it up with the tick marks

facet_wrap(\~ manufacturer) to make separate panels by manufacturer

labs(subtitle = "by Year and by Manufacturer") to add a subtitle to reflect the updated plot

```{r}
(displ.cty.scatter2 <- ggplot(mpg, aes(x=displ, y=cty))+
  geom_point(aes(color = as.factor(year)), size = 1.2)+
  scale_x_continuous(
    breaks = seq.default(from=0, to=10, by=0.5)
  ) +
  scale_color_discrete(
    name = "Year"
  )+
  theme_bw()+
  theme(
    axis.text.x = element_text(size=6, angle=90, hjust=1, vjust=0.4),
    panel.grid.minor.x = element_blank()
  ) +
  xlab("Engine displacement (L)")+
  ylab("City MPG")+
  labs(title = "Engine Displacement vs. City MPG by Year", subtitle = "by Year and by Manufacturer")+
  facet_wrap(~ manufacturer)
)
```

Save the plot as a PDF

```{r}
ggsave(displ.cty.scatter2, file = "C:\\Users\\ts415\\Documents\\displ.cty.scatter2.pdf", width=8, height=5)
```

### Polished Boxplots

Making polished boxplots is similar to the polished scatterplots above, except we use geom_boxplot() instead of geom_point(). We also make some other changes:

Make the color aesthetic in geom_boxplot vary by 'cyl' for number of cylinders -- convert this variable to a factor from numeric on the fly.

Change the title of the legend, the axis labels, and the plot titles

Remove 'scale_x\_continuous' because we no longer need to define the breaks on a numeric X axis

Use axis.text.x = element_text(size=9, angle=90, hjust=1, vjust=0.4) to rotate the x-axis text by 90 degrees and align it to the tick marks.

```{r}
ggplot(mpg, aes(x = manufacturer, y = cty))+
  geom_boxplot(aes(color = as.factor(cyl)))+
  scale_color_discrete(
    name = "Number of\nCylinders"
  )+
  theme_bw()+
  theme(
    axis.text.x = element_text(size=9, angle=90, hjust=1, vjust=0.4),
    panel.grid.minor.x = element_blank()
  ) +
  xlab("Manufacturer")+
  ylab("City MPG")+
  labs(title = "City MPG by Manufacturer", subtitle = "and by Number of Cylinders")
```

# Feedback From Homework

## Be Careful with Logical Operators

Make sure you think carefully about how R is interpreting your combination of logical operators. 

- is.na() returns a vector of boolean values where missing values receive a value of TRUE
- ! finds the complement, effectively flipping all of the boolean values in the vector
- & finds the intersection
- | finds the union

```{r}
a = c(1, 2, NA)
b = c(NA, 2, 3)

is.na(a)
is.na(b)

is.na(a) | is.na(b)
is.na(a) & is.na(b)
!is.na(a) & !is.na(b)
!(is.na(a) | is.na(b))
```

```{r}
temp <- data.frame(
          a = c(1, 2, NA),
          b = c(NA, 2, 3)
)

#Usage of OR operator that does not properly remove NAs
temp[!is.na(temp$a) | !is.na(temp$b) ,]

#AND operator does properly remove NAs
#This finds rows where either a or b is not missing, which is actually all rows
temp[!is.na(temp$a) & !is.na(temp$b) ,]

#Wrapping the entire OR conditional statement in the ! operator correctly removes NAs
temp[!(is.na(temp$a) | is.na(temp$b)) ,]
```

## Tidyverse & Base R

For those who used Tidyverse tools and syntax, I would encourage you to try to solve the HW problems with only 'base R.' 

# Looping (and not looping)

## Not Looping

You may have heard this before, but most functions in R are "vectorized" and R is relatively slow at performing for loops, so try to avoid them. Let's walk through some non-loop methods to accomplish common tasks.

### Operations on Columns of a Dataframe

This is the most obvious thing NOT to do in R using a loop. Let's derive a new variable for displacement (volume) per cylinder in the engine.

```{r}
mpg$vol.per.cyl <- mpg$displ / mpg$cyl
```

### Operations on Rows Using Conditional Logic

This may be a tempting task to accomplish with a for loop, but an alternative is using the subsetting methods above.

Let's increase MPG of city driving ONLY for manual transmissions by 10%. You could write a for loop to check the condition of each item, then perform the operation, but subsetting is much faster.

First let's use conditional subsetting to find the city MPG for all cars with manual transmissions using regular expressions

```{r}
mpg$cty[grep('manual', mpg$trans)]
```

Now we can build on this code by reassigning/overwriting their values with 10% increased values

```{r}
mpg$cty[grep('manual', mpg$trans)] <- mpg$cty[grep('manual', mpg$trans)]*1.1
```

Check the results
```{r}
mpg$cty[grep('manual', mpg$trans)]
```


## Looping

If you MUST loop in R, then you should know there are faster and slower, as well as verbose and less verbose, ways of doing so.

In general, using for() is both slow and verbose, but that makes it easier to begin learning.

### for

A for loop has two main parts: 1) A set of values on which to loop and 2) An execution block that says what to do for each value. An object must be initialized outside of the loop to save the loop output to. It's not a problem if the initial length of the container doesn't match the number of iterations of the loop. To save the output, you typically use the [] to assign the loop output to an index in the output container.

Here's a contrived loop example

```{r}
#Initialize the output container as a single NA value
output <- NA

#Save the ith value of i to the ith index of output
for (i in 1:10) {
  output[i] <- i
}

output
```

A more useful example would be to loop through the car manufacturers and calculate their mean city MPG. Let's try that.

```{r}
#Initialize the output container
manufac.mpg <- NA

#Create values for i that range from 1 to the number of unique manufacturers
for (i in 1:length(unique(mpg$manufacturer))) {
# for (i in seq(unique(mpg$manufacturer))) {      # <- A less verbose method of generating indices
  
  #Subset the city MPG variable to only include values for the ith unique manufacturer, and find the mean of those values. Save this in the ith index of our container object.
  manufac.mpg[i] <- mean(mpg$cty[mpg$manufacturer == unique(mpg$manufacturer)[i]])
}

#We have to assign the manufacturer names by hand using the same order as in the loop
names(manufac.mpg) <- unique(mpg$manufacturer)

manufac.mpg
```

I tend to always loop over integer indices, but some people loop over the actual values instead. Either way is acceptable, but sometimes within the loop you need to index another variable by the same loop index value (i in the example above) and in that case you need to loop over integers instead of actual values.

### Apply Family

The apply family of functions provide an alternative to for loops that are typically much faster to execute because of underlying efficiencies in R. They are also less verbose typically, but the downside is that they are more difficult to understand.

#### tapply

tapply is a very handy tool for doing an operation on one variable over the values of another -- exactly what we did in our for loop above

```{r}
tapply(mpg$cty, mpg$manufacturer, mean)
```

To perform more complex operations, you have to use a different syntax

```{r}
tapply(mpg$cty, mpg$manufacturer, FUN = function(i) mean(i)/sd(i))
```

#### lapply and sapply

lapply and sapply are more flexible versions of tapply in that you aren't limited to performing the operations on a single column in a dataframe. These can completely replace for loops and are quite a bit faster. lapply always returns a list and is very efficient at looping over list items, but can be used in other scenarios as well. sapply works similarly but attempts to return the simplest possible object as output (a vector when possible).

##### Simple Examples

Here's a very simple example to show the syntax of lapply

```{r}
lapply(1:3, FUN = function(x) x+1)
```

The syntax for sapply is similar, but it tries to return the simplest possible object -- in this case a vector

```{r}
sapply(1:3, FUN = function(x) x+1)
```

##### More Complex Examples

For example, let's find the  difference in average city MPG for manual and automatic transmissions for each year in our dataset.

```{r}
#Loop over unique values of year and define the function to apply
lapply(unique(mpg$year), FUN = function(x) {
  
  #First, create a temporary subset of the data consisting only of the xth unique year
  temp.dat <- mpg[mpg$year == x ,]
  
  #Next, calculate the difference in city MPG between manual transmissions and automatic transmissions by subsetting with grep
  mpg.diff <- mean(temp.dat$cty[grep("manual", temp.dat$trans)], na.rm = T) -
              mean(temp.dat$cty[grep("auto", temp.dat$trans)], na.rm = T)
  
  #Output the year (x) as an integer and the MPG difference we calculated as columns
  return(cbind(as.integer(x), mpg.diff))
})
```

Notice that the output is a list denoted by the double square brakets [[]]. We can wrangle this into a matrix by wrapping the loop in the handy the do.call() function and specifying to row bind the list items.

```{r}
#Wrap the lapply loop in do.call to combine the resulting list by row binding them, implicitly creating a matrix..
do.call('rbind', 
  #Loop over unique values of year and define the function to apply
  lapply(unique(mpg$year), FUN = function(x) {
    
    #First, create a temporary subset of the data consisting only of the xth unique year
    temp.dat <- mpg[mpg$year == x ,]
    
    #Next, calculate the difference in city MPG between manual transmissions and automatic transmissions by subsetting with grep
    mpg.diff <- mean(temp.dat$cty[grep("manual", temp.dat$trans)], na.rm = T) -
                mean(temp.dat$cty[grep("auto", temp.dat$trans)], na.rm = T)
    
    #Output the year (x) as an integer and the MPG difference we calculated as columns
    return(cbind(as.integer(x), mpg.diff))
  })
)
```

sapply can simplify the output without using do.call() but it column binds the output by default instead of row binding it.

```{r}
sapply(unique(mpg$year), FUN = function(x) {
  
  #First, create a temporary subset of the data consisting only of the xth unique year
  temp.dat <- mpg[mpg$year == x ,]
  
  #Next, calculate the difference in city MPG between manual transmissions and automatic transmissions by subsetting with grep
  mpg.diff <- mean(temp.dat$cty[grep("manual", temp.dat$trans)], na.rm = T) -
              mean(temp.dat$cty[grep("auto", temp.dat$trans)], na.rm = T)
  
  #Output the year (x) as an integer and the MPG difference we calculated as columns
  return(cbind(as.integer(x), mpg.diff))
})
```

#### lapply on a list

If you already have a list of items, lapply is very useful to modify them.

First, let's make a list where each list item is a dataframe corresponding to a single manufacturer using lapply.

```{r}
mpg.manufac <- lapply(unique(mpg$manufacturer), FUN = function(x) {
  mpg[mpg$manufacturer == x,]
})

#Make the names of the list items their corresponding manufacturer
names(mpg.manufac) <- unique(mpg$manufacturer)
```

Let's take a look at the first list item -- it should be a dataframe containing a single manufacturer

```{r}
mpg.manufac[[1]]
```

Now let's say we want to find the difference in city MPG between each car for a manufacturer and that manufacturer's mean city MPG, then save it as a new column in each of the manufacturer's dataframes in the list

```{r}
mpg.manufac <- lapply(mpg.manufac, FUN = function(x) {
  x$cty.diff <- x$cty - mean(x$cty)
  x
})
```

Now let's look at Honda's dataframe to see the outcome

```{r}
mpg.manufac[['honda']][, c("manufacturer", "model", "year", "cty", "cty.diff")]
```

##### Combining a List of Dataframes into One Dataframe

We could then recombine these list items into a large dataframe using do.call('rbind')

```{r}
as.data.frame(
  do.call('rbind', mpg.manufac)
)
```

#### apply

The main purpose of apply is to loop over either rows or columns of a matrix or dataframe. Use MARGIN = 1 for rows and MARGIN = 2 for columns

First, let's make a matrix from just the numeric variables in the MPG dataset

```{r}
mpg.mat <- as.matrix(mpg[, c("displ", "cyl", "cty", "hwy", "vol.per.cyl")])
```

Now let's find the means of each of these variables by looping over the columns with apply

```{r}
apply(mpg.mat, MARGIN = 2, FUN = function(x) mean(x))
```

##### Using a pre-specified function

So far, we've been declaring our custom function within the call the apply/lapply/sapply function, but we could also define the function beforehand and then apply it later.

Define a function to calculate mean and SD, then concatenate them using the paste0() function.

```{r}
meanSD <- function(x) {
  meanx <- mean(x)
  sdx <- sd(x)
  meanSDstring <- paste0(round(meanx, 2), " (", round(sdx, 2), ")")
}
```

Now apply the function to the mpg.mat matrix we created above.

```{r}
apply(mpg.mat, MARGIN = 2, FUN = meanSD)
```

Note that this will work for all other functions in the apply family as well.


# Appendix

## Matrix Algebra

R can perform matrix algebra very efficiently -- let's set up a simulated regression dataset and find Y using matrix multiplication with %\*%

```{r}
#Set a seed to obtain reproducible results
set.seed(53711)

#Simulated X matrix
X <- as.matrix(
  ncols = 3, nrows = 100,
  cbind(
    rep(1, 100),
    rnorm(n=100),
    rnorm(n=100)
  )
)

#Population beta coefficients
beta <- c(0.5, 1.2, -0.8)

#Vector of normal errors
epsilon <- rnorm(n=100, sd=0.2)

#Create Y vector as XB + e using matrix multiplication
Y <- X %*% beta + epsilon

#Run a multiple linear regression to estimate betas
coef(lm(Y ~ X[,2] + X[,3]))
```