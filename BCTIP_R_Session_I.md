---
title: 'BCTIP Training: R Session I, May 2022'
author: "Tyler Schappe, Tina Davenport"
date: "5/27/22"
output:
  html_document:
    toc: yes
    toc_float: 
      collapsed: TRUE
      smooth_scroll: TRUE
    toc_depth: '4'
    number_sections: TRUE
    df_print: paged
    keep_md: TRUE
---


# A Brief Tour of RStudio

## Git in RStudio

# Some R Basics

- Object-oriented language
  + Objects generally "live" in the global environment (in memory)
  + Objects have attributes and can have "slots"
  + Basic object types:
    - Vector
    - Matrix
    - Dataframe
    - List
    - Function
  + Basic data types:
    - Numeric
    - Integer
    - Character
    - Factor
    - Date
    - Special types
- Memory-based
  + Does not save object to disk by default
  + Need to be intentional about saving
- R packages/libraries extend functionality of base R

# Note on the Bifurcation of Programming Styles Within R

R has **a lot** of built-in functionality from efficient and well-curated functions. However, the R syntax is somewhat counter-intuitive and verbose. In response, a programmer named Hadley Wickham created Tidyverse, a set of packages that enables alternative syntax, among other functionality. In particular, the "pipe" operator %>% enables output from one function to be input into a subsequent function. This generally results in simpler and more readable code. 

While Tidyverse is intuitive and has many advantages, being comfortable with **both** tidyverse and "base R" has advantages:

- Functions from many packages do not use tidyverse
- Scripts that you may inherit from other programmers may not use tidyverse
- Base R can be more flexible and can sometimes be faster computationally
- You will have more tools to approach difficult problems in different ways

# Setup

Source functions file

```r
source("functions.R")
```

## Load Libraries


```r
library(ggplot2); library(reshape2); library(table1)
```

```
## 
## Attaching package: 'table1'
```

```
## The following objects are masked from 'package:base':
## 
##     units, units<-
```

## Load the MPG Dataset into the Working Environment

Convert the mpg dataset from ggplot2 into a data frame object


```r
mpg <- as.data.frame(mpg)
```

# Examining and Manipulating Dataset Structure

The 'str' function gives information on the type of data each variable is and the first couple of observations of each. It also provides metadata about the data object itself -- in this case that it's a data frame.


```r
str(mpg)
```

```
## 'data.frame':	234 obs. of  11 variables:
##  $ manufacturer: chr  "audi" "audi" "audi" "audi" ...
##  $ model       : chr  "a4" "a4" "a4" "a4" ...
##  $ displ       : num  1.8 1.8 2 2 2.8 2.8 3.1 1.8 1.8 2 ...
##  $ year        : int  1999 1999 2008 2008 1999 1999 2008 1999 1999 2008 ...
##  $ cyl         : int  4 4 4 4 6 6 6 4 4 4 ...
##  $ trans       : chr  "auto(l5)" "manual(m5)" "manual(m6)" "auto(av)" ...
##  $ drv         : chr  "f" "f" "f" "f" ...
##  $ cty         : int  18 21 20 21 16 18 18 18 16 20 ...
##  $ hwy         : int  29 29 31 30 26 26 27 26 25 28 ...
##  $ fl          : chr  "p" "p" "p" "p" ...
##  $ class       : chr  "compact" "compact" "compact" "compact" ...
```

## Converting a Variable to a Factor

We can see that the variables for manufacturer and model are currently character variables, but for later analyses we would prefer them to be factor variables -- we can convert them using the as.factor() function


```r
mpg$manufacturer <- as.factor(mpg$manufacturer)
mpg$model <- as.factor(mpg$model)
```

Now let's look again at the output of str()


```r
str(mpg)
```

```
## 'data.frame':	234 obs. of  11 variables:
##  $ manufacturer: Factor w/ 15 levels "audi","chevrolet",..: 1 1 1 1 1 1 1 1 1 1 ...
##  $ model       : Factor w/ 38 levels "4runner 4wd",..: 2 2 2 2 2 2 2 3 3 3 ...
##  $ displ       : num  1.8 1.8 2 2 2.8 2.8 3.1 1.8 1.8 2 ...
##  $ year        : int  1999 1999 2008 2008 1999 1999 2008 1999 1999 2008 ...
##  $ cyl         : int  4 4 4 4 6 6 6 4 4 4 ...
##  $ trans       : chr  "auto(l5)" "manual(m5)" "manual(m6)" "auto(av)" ...
##  $ drv         : chr  "f" "f" "f" "f" ...
##  $ cty         : int  18 21 20 21 16 18 18 18 16 20 ...
##  $ hwy         : int  29 29 31 30 26 26 27 26 25 28 ...
##  $ fl          : chr  "p" "p" "p" "p" ...
##  $ class       : chr  "compact" "compact" "compact" "compact" ...
```

We can now see that 'manufacturer' and 'model' are factors now

## Releveling Factors

Let's first look at the levels of the variable "manufacturer"


```r
levels(mpg$manufacturer)
```

```
##  [1] "audi"       "chevrolet"  "dodge"      "ford"       "honda"     
##  [6] "hyundai"    "jeep"       "land rover" "lincoln"    "mercury"   
## [11] "nissan"     "pontiac"    "subaru"     "toyota"     "volkswagen"
```

### Set the baseline level of the factor

Audi is the baseline level because it's listed first. Let's make it Toyota instead.


```r
mpg$manufacturer <- relevel(mpg$manufacturer, ref="toyota")
levels(mpg$manufacturer)
```

```
##  [1] "toyota"     "audi"       "chevrolet"  "dodge"      "ford"      
##  [6] "honda"      "hyundai"    "jeep"       "land rover" "lincoln"   
## [11] "mercury"    "nissan"     "pontiac"    "subaru"     "volkswagen"
```

### Set a custom order for the levels

Let's look at the levels and use their indices to re-order them according to continent that the company is based in


```r
#Display current levels
levels(mpg$manufacturer)
```

```
##  [1] "toyota"     "audi"       "chevrolet"  "dodge"      "ford"      
##  [6] "honda"      "hyundai"    "jeep"       "land rover" "lincoln"   
## [11] "mercury"    "nissan"     "pontiac"    "subaru"     "volkswagen"
```

```r
#Re-order the levels
mpg$manufacturer <- factor(
  mpg$manufacturer,
    levels=c("honda", "toyota", "nissan", "subaru", "hyundai", "audi", "land rover", "volkswagen", "chevrolet", "dodge", "ford", "jeep", "lincoln", "mercury", "pontiac")
  )
```

Look at the levels again to verify

```r
levels(mpg$manufacturer)
```

```
##  [1] "honda"      "toyota"     "nissan"     "subaru"     "hyundai"   
##  [6] "audi"       "land rover" "volkswagen" "chevrolet"  "dodge"     
## [11] "ford"       "jeep"       "lincoln"    "mercury"    "pontiac"
```

# Basic Data Summary

## Summary()

The summary() function gives basic summary statistics for numeric variables and length information for character variables. We don't have missing values in this dataset, but if we did, summary() would show them.


```r
summary(mpg)
```

```
##      manufacturer                 model         displ            year     
##  dodge     :37    caravan 2wd        : 11   Min.   :1.600   Min.   :1999  
##  toyota    :34    ram 1500 pickup 4wd: 10   1st Qu.:2.400   1st Qu.:1999  
##  volkswagen:27    civic              :  9   Median :3.300   Median :2004  
##  ford      :25    dakota pickup 4wd  :  9   Mean   :3.472   Mean   :2004  
##  chevrolet :19    jetta              :  9   3rd Qu.:4.600   3rd Qu.:2008  
##  audi      :18    mustang            :  9   Max.   :7.000   Max.   :2008  
##  (Other)   :74    (Other)            :177                                 
##       cyl           trans               drv                 cty       
##  Min.   :4.000   Length:234         Length:234         Min.   : 9.00  
##  1st Qu.:4.000   Class :character   Class :character   1st Qu.:14.00  
##  Median :6.000   Mode  :character   Mode  :character   Median :17.00  
##  Mean   :5.889                                         Mean   :16.86  
##  3rd Qu.:8.000                                         3rd Qu.:19.00  
##  Max.   :8.000                                         Max.   :35.00  
##                                                                       
##       hwy             fl               class          
##  Min.   :12.00   Length:234         Length:234        
##  1st Qu.:18.00   Class :character   Class :character  
##  Median :24.00   Mode  :character   Mode  :character  
##  Mean   :23.44                                        
##  3rd Qu.:27.00                                        
##  Max.   :44.00                                        
## 
```

## Missing Values

Let's add a missing value for city MPG to a single row to see how summary deals with it

```r
mpg.miss <- mpg[, c("manufacturer", "model", "cty")]
mpg.miss[12, "cty"] <- NA

summary(mpg.miss)
```

```
##      manufacturer                 model          cty       
##  dodge     :37    caravan 2wd        : 11   Min.   : 9.00  
##  toyota    :34    ram 1500 pickup 4wd: 10   1st Qu.:14.00  
##  volkswagen:27    civic              :  9   Median :17.00  
##  ford      :25    dakota pickup 4wd  :  9   Mean   :16.87  
##  chevrolet :19    jetta              :  9   3rd Qu.:19.00  
##  audi      :18    mustang            :  9   Max.   :35.00  
##  (Other)   :74    (Other)            :177   NA's   :1
```

Unfortunately, summary() doesn't deal with NAs well in characters strings

```r
mpg.miss$manufacturer <- as.character(mpg.miss$manufacturer)
mpg.miss$manufacturer[12] <- NA

summary(mpg.miss)
```

```
##  manufacturer                       model          cty       
##  Length:234         caravan 2wd        : 11   Min.   : 9.00  
##  Class :character   ram 1500 pickup 4wd: 10   1st Qu.:14.00  
##  Mode  :character   civic              :  9   Median :17.00  
##                     dakota pickup 4wd  :  9   Mean   :16.87  
##                     jetta              :  9   3rd Qu.:19.00  
##                     mustang            :  9   Max.   :35.00  
##                     (Other)            :177   NA's   :1
```

But it works well for factors

```r
mpg.miss$manufacturer <- as.factor(mpg.miss$manufacturer)

summary(mpg.miss)
```

```
##      manufacturer                 model          cty       
##  dodge     :37    caravan 2wd        : 11   Min.   : 9.00  
##  toyota    :34    ram 1500 pickup 4wd: 10   1st Qu.:14.00  
##  volkswagen:27    civic              :  9   Median :17.00  
##  ford      :25    dakota pickup 4wd  :  9   Mean   :16.87  
##  chevrolet :19    jetta              :  9   3rd Qu.:19.00  
##  (Other)   :91    mustang            :  9   Max.   :35.00  
##  NA's      : 1    (Other)            :177   NA's   :1
```

## table()

For quick cross-tablutions, the table() function works well. Let's see how many observations there are in each combination of manufacturer and year. Including "exclude = NULL" will show NA values in the table.


```r
table(mpg$manufacturer, mpg$year, exclude = NULL)
```

```
##             
##              1999 2008
##   honda         5    4
##   toyota       20   14
##   nissan        6    7
##   subaru        6    8
##   hyundai       6    8
##   audi          9    9
##   land rover    2    2
##   volkswagen   16   11
##   chevrolet     7   12
##   dodge        16   21
##   ford         15   10
##   jeep          2    6
##   lincoln       2    1
##   mercury       2    2
##   pontiac       3    2
```

Let's use it on our dataframe with missing varibles to see how "exclude = NULL" works


```r
head(table(mpg.miss$model, mpg.miss$manufacturer, exclude = NULL))
```

```
##                     
##                      audi chevrolet dodge ford honda hyundai jeep land rover
##   4runner 4wd           0         0     0    0     0       0    0          0
##   a4                    7         0     0    0     0       0    0          0
##   a4 quattro            7         0     0    0     0       0    0          0
##   a6 quattro            3         0     0    0     0       0    0          0
##   altima                0         0     0    0     0       0    0          0
##   c1500 suburban 2wd    0         5     0    0     0       0    0          0
##                     
##                      lincoln mercury nissan pontiac subaru toyota volkswagen
##   4runner 4wd              0       0      0       0      0      6          0
##   a4                       0       0      0       0      0      0          0
##   a4 quattro               0       0      0       0      0      0          0
##   a6 quattro               0       0      0       0      0      0          0
##   altima                   0       0      6       0      0      0          0
##   c1500 suburban 2wd       0       0      0       0      0      0          0
##                     
##                      <NA>
##   4runner 4wd           0
##   a4                    0
##   a4 quattro            1
##   a6 quattro            0
##   altima                0
##   c1500 suburban 2wd    0
```

## 'table1' Package

The 'table1' package is very handy for producing high-quality summary tables, typically Table 1 in published manuscripts.


```r
table1(~ cyl + displ + cty + hwy | manufacturer,
       data = mpg)
```

```
## Warning in .table1.internal(x = x, labels = labels, groupspan = groupspan, :
## Table has 16 columns. Are you sure this is what you want?
```

```{=html}
<div class="Rtable1"><table class="Rtable1">
<thead>
<tr>
<th class='rowlabel firstrow lastrow'></th>
<th class='firstrow lastrow'><span class='stratlabel'>honda<br><span class='stratn'>(N=9)</span></span></th>
<th class='firstrow lastrow'><span class='stratlabel'>toyota<br><span class='stratn'>(N=34)</span></span></th>
<th class='firstrow lastrow'><span class='stratlabel'>nissan<br><span class='stratn'>(N=13)</span></span></th>
<th class='firstrow lastrow'><span class='stratlabel'>subaru<br><span class='stratn'>(N=14)</span></span></th>
<th class='firstrow lastrow'><span class='stratlabel'>hyundai<br><span class='stratn'>(N=14)</span></span></th>
<th class='firstrow lastrow'><span class='stratlabel'>audi<br><span class='stratn'>(N=18)</span></span></th>
<th class='firstrow lastrow'><span class='stratlabel'>land rover<br><span class='stratn'>(N=4)</span></span></th>
<th class='firstrow lastrow'><span class='stratlabel'>volkswagen<br><span class='stratn'>(N=27)</span></span></th>
<th class='firstrow lastrow'><span class='stratlabel'>chevrolet<br><span class='stratn'>(N=19)</span></span></th>
<th class='firstrow lastrow'><span class='stratlabel'>dodge<br><span class='stratn'>(N=37)</span></span></th>
<th class='firstrow lastrow'><span class='stratlabel'>ford<br><span class='stratn'>(N=25)</span></span></th>
<th class='firstrow lastrow'><span class='stratlabel'>jeep<br><span class='stratn'>(N=8)</span></span></th>
<th class='firstrow lastrow'><span class='stratlabel'>lincoln<br><span class='stratn'>(N=3)</span></span></th>
<th class='firstrow lastrow'><span class='stratlabel'>mercury<br><span class='stratn'>(N=4)</span></span></th>
<th class='firstrow lastrow'><span class='stratlabel'>pontiac<br><span class='stratn'>(N=5)</span></span></th>
<th class='firstrow lastrow'><span class='stratlabel'>Overall<br><span class='stratn'>(N=234)</span></span></th>
</tr>
</thead>
<tbody>
<tr>
<td class='rowlabel firstrow'>cyl</td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
</tr>
<tr>
<td class='rowlabel'>Mean (SD)</td>
<td>4.00 (0)</td>
<td>5.12 (1.32)</td>
<td>5.54 (1.20)</td>
<td>4.00 (0)</td>
<td>4.86 (1.03)</td>
<td>5.22 (1.22)</td>
<td>8.00 (0)</td>
<td>4.59 (0.844)</td>
<td>7.26 (1.37)</td>
<td>7.08 (1.12)</td>
<td>7.20 (1.00)</td>
<td>7.25 (1.04)</td>
<td>8.00 (0)</td>
<td>7.00 (1.15)</td>
<td>6.40 (0.894)</td>
<td>5.89 (1.61)</td>
</tr>
<tr>
<td class='rowlabel lastrow'>Median [Min, Max]</td>
<td class='lastrow'>4.00 [4.00, 4.00]</td>
<td class='lastrow'>4.00 [4.00, 8.00]</td>
<td class='lastrow'>6.00 [4.00, 8.00]</td>
<td class='lastrow'>4.00 [4.00, 4.00]</td>
<td class='lastrow'>4.00 [4.00, 6.00]</td>
<td class='lastrow'>6.00 [4.00, 8.00]</td>
<td class='lastrow'>8.00 [8.00, 8.00]</td>
<td class='lastrow'>4.00 [4.00, 6.00]</td>
<td class='lastrow'>8.00 [4.00, 8.00]</td>
<td class='lastrow'>8.00 [4.00, 8.00]</td>
<td class='lastrow'>8.00 [6.00, 8.00]</td>
<td class='lastrow'>8.00 [6.00, 8.00]</td>
<td class='lastrow'>8.00 [8.00, 8.00]</td>
<td class='lastrow'>7.00 [6.00, 8.00]</td>
<td class='lastrow'>6.00 [6.00, 8.00]</td>
<td class='lastrow'>6.00 [4.00, 8.00]</td>
</tr>
<tr>
<td class='rowlabel firstrow'>displ</td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
</tr>
<tr>
<td class='rowlabel'>Mean (SD)</td>
<td>1.71 (0.145)</td>
<td>2.95 (0.931)</td>
<td>3.27 (0.864)</td>
<td>2.46 (0.109)</td>
<td>2.43 (0.365)</td>
<td>2.54 (0.673)</td>
<td>4.30 (0.258)</td>
<td>2.26 (0.443)</td>
<td>5.06 (1.37)</td>
<td>4.38 (0.868)</td>
<td>4.54 (0.541)</td>
<td>4.58 (1.02)</td>
<td>5.40 (0)</td>
<td>4.40 (0.490)</td>
<td>3.96 (0.808)</td>
<td>3.47 (1.29)</td>
</tr>
<tr>
<td class='rowlabel lastrow'>Median [Min, Max]</td>
<td class='lastrow'>1.60 [1.60, 2.00]</td>
<td class='lastrow'>2.70 [1.80, 5.70]</td>
<td class='lastrow'>3.30 [2.40, 5.60]</td>
<td class='lastrow'>2.50 [2.20, 2.50]</td>
<td class='lastrow'>2.40 [2.00, 3.30]</td>
<td class='lastrow'>2.80 [1.80, 4.20]</td>
<td class='lastrow'>4.30 [4.00, 4.60]</td>
<td class='lastrow'>2.00 [1.80, 3.60]</td>
<td class='lastrow'>5.30 [2.40, 7.00]</td>
<td class='lastrow'>4.70 [2.40, 5.90]</td>
<td class='lastrow'>4.60 [3.80, 5.40]</td>
<td class='lastrow'>4.70 [3.00, 6.10]</td>
<td class='lastrow'>5.40 [5.40, 5.40]</td>
<td class='lastrow'>4.30 [4.00, 5.00]</td>
<td class='lastrow'>3.80 [3.10, 5.30]</td>
<td class='lastrow'>3.30 [1.60, 7.00]</td>
</tr>
<tr>
<td class='rowlabel firstrow'>cty</td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
</tr>
<tr>
<td class='rowlabel'>Mean (SD)</td>
<td>24.4 (1.94)</td>
<td>18.5 (4.05)</td>
<td>18.1 (3.43)</td>
<td>19.3 (0.914)</td>
<td>18.6 (1.50)</td>
<td>17.6 (1.97)</td>
<td>11.5 (0.577)</td>
<td>20.9 (4.56)</td>
<td>15.0 (2.92)</td>
<td>13.1 (2.49)</td>
<td>14.0 (1.91)</td>
<td>13.5 (2.51)</td>
<td>11.3 (0.577)</td>
<td>13.3 (0.500)</td>
<td>17.0 (1.00)</td>
<td>16.9 (4.26)</td>
</tr>
<tr>
<td class='rowlabel lastrow'>Median [Min, Max]</td>
<td class='lastrow'>24.0 [21.0, 28.0]</td>
<td class='lastrow'>18.0 [11.0, 28.0]</td>
<td class='lastrow'>19.0 [12.0, 23.0]</td>
<td class='lastrow'>19.0 [18.0, 21.0]</td>
<td class='lastrow'>18.5 [16.0, 21.0]</td>
<td class='lastrow'>17.5 [15.0, 21.0]</td>
<td class='lastrow'>11.5 [11.0, 12.0]</td>
<td class='lastrow'>21.0 [16.0, 35.0]</td>
<td class='lastrow'>15.0 [11.0, 22.0]</td>
<td class='lastrow'>13.0 [9.00, 18.0]</td>
<td class='lastrow'>14.0 [11.0, 18.0]</td>
<td class='lastrow'>14.0 [9.00, 17.0]</td>
<td class='lastrow'>11.0 [11.0, 12.0]</td>
<td class='lastrow'>13.0 [13.0, 14.0]</td>
<td class='lastrow'>17.0 [16.0, 18.0]</td>
<td class='lastrow'>17.0 [9.00, 35.0]</td>
</tr>
<tr>
<td class='rowlabel firstrow'>hwy</td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
<td class='firstrow'></td>
</tr>
<tr>
<td class='rowlabel'>Mean (SD)</td>
<td>32.6 (2.55)</td>
<td>24.9 (6.17)</td>
<td>24.6 (5.09)</td>
<td>25.6 (1.16)</td>
<td>26.9 (2.18)</td>
<td>26.4 (2.18)</td>
<td>16.5 (1.73)</td>
<td>29.2 (5.32)</td>
<td>21.9 (5.11)</td>
<td>17.9 (3.57)</td>
<td>19.4 (3.33)</td>
<td>17.6 (3.25)</td>
<td>17.0 (1.00)</td>
<td>18.0 (1.15)</td>
<td>26.4 (1.14)</td>
<td>23.4 (5.95)</td>
</tr>
<tr>
<td class='rowlabel lastrow'>Median [Min, Max]</td>
<td class='lastrow'>32.0 [29.0, 36.0]</td>
<td class='lastrow'>26.0 [15.0, 37.0]</td>
<td class='lastrow'>26.0 [17.0, 32.0]</td>
<td class='lastrow'>26.0 [23.0, 27.0]</td>
<td class='lastrow'>26.5 [24.0, 31.0]</td>
<td class='lastrow'>26.0 [23.0, 31.0]</td>
<td class='lastrow'>16.5 [15.0, 18.0]</td>
<td class='lastrow'>29.0 [23.0, 44.0]</td>
<td class='lastrow'>23.0 [14.0, 30.0]</td>
<td class='lastrow'>17.0 [12.0, 24.0]</td>
<td class='lastrow'>18.0 [15.0, 26.0]</td>
<td class='lastrow'>18.5 [12.0, 22.0]</td>
<td class='lastrow'>17.0 [16.0, 18.0]</td>
<td class='lastrow'>18.0 [17.0, 19.0]</td>
<td class='lastrow'>26.0 [25.0, 28.0]</td>
<td class='lastrow'>24.0 [12.0, 44.0]</td>
</tr>
</tbody>
</table>
</div>
```


# Subsetting Data

Subsetting data is an extremely useful and powerful tool, and R makes it particularly easy. As with most things, there are multiple ways to accomplish the same thing.


## subset() function

The subset() function is a quick and easy way to perform subsetting, but note that the function documentation warns that it is intended for interactive use only and instead recommends the use of square brakets "[]" for programmatic use (we'll cover those below).  

### Subsetting Columns with "select ="

An easy an quick way to subset columns is with the subset() function. Let's say we want to only keep the manufacturer, model, and year columns of the dataframe. We can use "select = "


```r
subset(mpg, select = c(manufacturer, model, year))
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["manufacturer"],"name":[1],"type":["fct"],"align":["left"]},{"label":["model"],"name":[2],"type":["fct"],"align":["left"]},{"label":["year"],"name":[3],"type":["int"],"align":["right"]}],"data":[{"1":"audi","2":"a4","3":"1999","_rn_":"1"},{"1":"audi","2":"a4","3":"1999","_rn_":"2"},{"1":"audi","2":"a4","3":"2008","_rn_":"3"},{"1":"audi","2":"a4","3":"2008","_rn_":"4"},{"1":"audi","2":"a4","3":"1999","_rn_":"5"},{"1":"audi","2":"a4","3":"1999","_rn_":"6"},{"1":"audi","2":"a4","3":"2008","_rn_":"7"},{"1":"audi","2":"a4 quattro","3":"1999","_rn_":"8"},{"1":"audi","2":"a4 quattro","3":"1999","_rn_":"9"},{"1":"audi","2":"a4 quattro","3":"2008","_rn_":"10"},{"1":"audi","2":"a4 quattro","3":"2008","_rn_":"11"},{"1":"audi","2":"a4 quattro","3":"1999","_rn_":"12"},{"1":"audi","2":"a4 quattro","3":"1999","_rn_":"13"},{"1":"audi","2":"a4 quattro","3":"2008","_rn_":"14"},{"1":"audi","2":"a4 quattro","3":"2008","_rn_":"15"},{"1":"audi","2":"a6 quattro","3":"1999","_rn_":"16"},{"1":"audi","2":"a6 quattro","3":"2008","_rn_":"17"},{"1":"audi","2":"a6 quattro","3":"2008","_rn_":"18"},{"1":"chevrolet","2":"c1500 suburban 2wd","3":"2008","_rn_":"19"},{"1":"chevrolet","2":"c1500 suburban 2wd","3":"2008","_rn_":"20"},{"1":"chevrolet","2":"c1500 suburban 2wd","3":"2008","_rn_":"21"},{"1":"chevrolet","2":"c1500 suburban 2wd","3":"1999","_rn_":"22"},{"1":"chevrolet","2":"c1500 suburban 2wd","3":"2008","_rn_":"23"},{"1":"chevrolet","2":"corvette","3":"1999","_rn_":"24"},{"1":"chevrolet","2":"corvette","3":"1999","_rn_":"25"},{"1":"chevrolet","2":"corvette","3":"2008","_rn_":"26"},{"1":"chevrolet","2":"corvette","3":"2008","_rn_":"27"},{"1":"chevrolet","2":"corvette","3":"2008","_rn_":"28"},{"1":"chevrolet","2":"k1500 tahoe 4wd","3":"2008","_rn_":"29"},{"1":"chevrolet","2":"k1500 tahoe 4wd","3":"2008","_rn_":"30"},{"1":"chevrolet","2":"k1500 tahoe 4wd","3":"1999","_rn_":"31"},{"1":"chevrolet","2":"k1500 tahoe 4wd","3":"1999","_rn_":"32"},{"1":"chevrolet","2":"malibu","3":"1999","_rn_":"33"},{"1":"chevrolet","2":"malibu","3":"2008","_rn_":"34"},{"1":"chevrolet","2":"malibu","3":"1999","_rn_":"35"},{"1":"chevrolet","2":"malibu","3":"2008","_rn_":"36"},{"1":"chevrolet","2":"malibu","3":"2008","_rn_":"37"},{"1":"dodge","2":"caravan 2wd","3":"1999","_rn_":"38"},{"1":"dodge","2":"caravan 2wd","3":"1999","_rn_":"39"},{"1":"dodge","2":"caravan 2wd","3":"1999","_rn_":"40"},{"1":"dodge","2":"caravan 2wd","3":"1999","_rn_":"41"},{"1":"dodge","2":"caravan 2wd","3":"2008","_rn_":"42"},{"1":"dodge","2":"caravan 2wd","3":"2008","_rn_":"43"},{"1":"dodge","2":"caravan 2wd","3":"2008","_rn_":"44"},{"1":"dodge","2":"caravan 2wd","3":"1999","_rn_":"45"},{"1":"dodge","2":"caravan 2wd","3":"1999","_rn_":"46"},{"1":"dodge","2":"caravan 2wd","3":"2008","_rn_":"47"},{"1":"dodge","2":"caravan 2wd","3":"2008","_rn_":"48"},{"1":"dodge","2":"dakota pickup 4wd","3":"2008","_rn_":"49"},{"1":"dodge","2":"dakota pickup 4wd","3":"2008","_rn_":"50"},{"1":"dodge","2":"dakota pickup 4wd","3":"1999","_rn_":"51"},{"1":"dodge","2":"dakota pickup 4wd","3":"1999","_rn_":"52"},{"1":"dodge","2":"dakota pickup 4wd","3":"2008","_rn_":"53"},{"1":"dodge","2":"dakota pickup 4wd","3":"2008","_rn_":"54"},{"1":"dodge","2":"dakota pickup 4wd","3":"2008","_rn_":"55"},{"1":"dodge","2":"dakota pickup 4wd","3":"1999","_rn_":"56"},{"1":"dodge","2":"dakota pickup 4wd","3":"1999","_rn_":"57"},{"1":"dodge","2":"durango 4wd","3":"1999","_rn_":"58"},{"1":"dodge","2":"durango 4wd","3":"2008","_rn_":"59"},{"1":"dodge","2":"durango 4wd","3":"2008","_rn_":"60"},{"1":"dodge","2":"durango 4wd","3":"2008","_rn_":"61"},{"1":"dodge","2":"durango 4wd","3":"1999","_rn_":"62"},{"1":"dodge","2":"durango 4wd","3":"2008","_rn_":"63"},{"1":"dodge","2":"durango 4wd","3":"1999","_rn_":"64"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"2008","_rn_":"65"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"2008","_rn_":"66"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"2008","_rn_":"67"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"2008","_rn_":"68"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"2008","_rn_":"69"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"2008","_rn_":"70"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"1999","_rn_":"71"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"1999","_rn_":"72"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"2008","_rn_":"73"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"1999","_rn_":"74"},{"1":"ford","2":"expedition 2wd","3":"1999","_rn_":"75"},{"1":"ford","2":"expedition 2wd","3":"1999","_rn_":"76"},{"1":"ford","2":"expedition 2wd","3":"2008","_rn_":"77"},{"1":"ford","2":"explorer 4wd","3":"1999","_rn_":"78"},{"1":"ford","2":"explorer 4wd","3":"1999","_rn_":"79"},{"1":"ford","2":"explorer 4wd","3":"1999","_rn_":"80"},{"1":"ford","2":"explorer 4wd","3":"2008","_rn_":"81"},{"1":"ford","2":"explorer 4wd","3":"2008","_rn_":"82"},{"1":"ford","2":"explorer 4wd","3":"1999","_rn_":"83"},{"1":"ford","2":"f150 pickup 4wd","3":"1999","_rn_":"84"},{"1":"ford","2":"f150 pickup 4wd","3":"1999","_rn_":"85"},{"1":"ford","2":"f150 pickup 4wd","3":"1999","_rn_":"86"},{"1":"ford","2":"f150 pickup 4wd","3":"1999","_rn_":"87"},{"1":"ford","2":"f150 pickup 4wd","3":"2008","_rn_":"88"},{"1":"ford","2":"f150 pickup 4wd","3":"1999","_rn_":"89"},{"1":"ford","2":"f150 pickup 4wd","3":"2008","_rn_":"90"},{"1":"ford","2":"mustang","3":"1999","_rn_":"91"},{"1":"ford","2":"mustang","3":"1999","_rn_":"92"},{"1":"ford","2":"mustang","3":"2008","_rn_":"93"},{"1":"ford","2":"mustang","3":"2008","_rn_":"94"},{"1":"ford","2":"mustang","3":"1999","_rn_":"95"},{"1":"ford","2":"mustang","3":"1999","_rn_":"96"},{"1":"ford","2":"mustang","3":"2008","_rn_":"97"},{"1":"ford","2":"mustang","3":"2008","_rn_":"98"},{"1":"ford","2":"mustang","3":"2008","_rn_":"99"},{"1":"honda","2":"civic","3":"1999","_rn_":"100"},{"1":"honda","2":"civic","3":"1999","_rn_":"101"},{"1":"honda","2":"civic","3":"1999","_rn_":"102"},{"1":"honda","2":"civic","3":"1999","_rn_":"103"},{"1":"honda","2":"civic","3":"1999","_rn_":"104"},{"1":"honda","2":"civic","3":"2008","_rn_":"105"},{"1":"honda","2":"civic","3":"2008","_rn_":"106"},{"1":"honda","2":"civic","3":"2008","_rn_":"107"},{"1":"honda","2":"civic","3":"2008","_rn_":"108"},{"1":"hyundai","2":"sonata","3":"1999","_rn_":"109"},{"1":"hyundai","2":"sonata","3":"1999","_rn_":"110"},{"1":"hyundai","2":"sonata","3":"2008","_rn_":"111"},{"1":"hyundai","2":"sonata","3":"2008","_rn_":"112"},{"1":"hyundai","2":"sonata","3":"1999","_rn_":"113"},{"1":"hyundai","2":"sonata","3":"1999","_rn_":"114"},{"1":"hyundai","2":"sonata","3":"2008","_rn_":"115"},{"1":"hyundai","2":"tiburon","3":"1999","_rn_":"116"},{"1":"hyundai","2":"tiburon","3":"1999","_rn_":"117"},{"1":"hyundai","2":"tiburon","3":"2008","_rn_":"118"},{"1":"hyundai","2":"tiburon","3":"2008","_rn_":"119"},{"1":"hyundai","2":"tiburon","3":"2008","_rn_":"120"},{"1":"hyundai","2":"tiburon","3":"2008","_rn_":"121"},{"1":"hyundai","2":"tiburon","3":"2008","_rn_":"122"},{"1":"jeep","2":"grand cherokee 4wd","3":"2008","_rn_":"123"},{"1":"jeep","2":"grand cherokee 4wd","3":"2008","_rn_":"124"},{"1":"jeep","2":"grand cherokee 4wd","3":"1999","_rn_":"125"},{"1":"jeep","2":"grand cherokee 4wd","3":"1999","_rn_":"126"},{"1":"jeep","2":"grand cherokee 4wd","3":"2008","_rn_":"127"},{"1":"jeep","2":"grand cherokee 4wd","3":"2008","_rn_":"128"},{"1":"jeep","2":"grand cherokee 4wd","3":"2008","_rn_":"129"},{"1":"jeep","2":"grand cherokee 4wd","3":"2008","_rn_":"130"},{"1":"land rover","2":"range rover","3":"1999","_rn_":"131"},{"1":"land rover","2":"range rover","3":"2008","_rn_":"132"},{"1":"land rover","2":"range rover","3":"2008","_rn_":"133"},{"1":"land rover","2":"range rover","3":"1999","_rn_":"134"},{"1":"lincoln","2":"navigator 2wd","3":"1999","_rn_":"135"},{"1":"lincoln","2":"navigator 2wd","3":"1999","_rn_":"136"},{"1":"lincoln","2":"navigator 2wd","3":"2008","_rn_":"137"},{"1":"mercury","2":"mountaineer 4wd","3":"1999","_rn_":"138"},{"1":"mercury","2":"mountaineer 4wd","3":"2008","_rn_":"139"},{"1":"mercury","2":"mountaineer 4wd","3":"2008","_rn_":"140"},{"1":"mercury","2":"mountaineer 4wd","3":"1999","_rn_":"141"},{"1":"nissan","2":"altima","3":"1999","_rn_":"142"},{"1":"nissan","2":"altima","3":"1999","_rn_":"143"},{"1":"nissan","2":"altima","3":"2008","_rn_":"144"},{"1":"nissan","2":"altima","3":"2008","_rn_":"145"},{"1":"nissan","2":"altima","3":"2008","_rn_":"146"},{"1":"nissan","2":"altima","3":"2008","_rn_":"147"},{"1":"nissan","2":"maxima","3":"1999","_rn_":"148"},{"1":"nissan","2":"maxima","3":"1999","_rn_":"149"},{"1":"nissan","2":"maxima","3":"2008","_rn_":"150"},{"1":"nissan","2":"pathfinder 4wd","3":"1999","_rn_":"151"},{"1":"nissan","2":"pathfinder 4wd","3":"1999","_rn_":"152"},{"1":"nissan","2":"pathfinder 4wd","3":"2008","_rn_":"153"},{"1":"nissan","2":"pathfinder 4wd","3":"2008","_rn_":"154"},{"1":"pontiac","2":"grand prix","3":"1999","_rn_":"155"},{"1":"pontiac","2":"grand prix","3":"1999","_rn_":"156"},{"1":"pontiac","2":"grand prix","3":"1999","_rn_":"157"},{"1":"pontiac","2":"grand prix","3":"2008","_rn_":"158"},{"1":"pontiac","2":"grand prix","3":"2008","_rn_":"159"},{"1":"subaru","2":"forester awd","3":"1999","_rn_":"160"},{"1":"subaru","2":"forester awd","3":"1999","_rn_":"161"},{"1":"subaru","2":"forester awd","3":"2008","_rn_":"162"},{"1":"subaru","2":"forester awd","3":"2008","_rn_":"163"},{"1":"subaru","2":"forester awd","3":"2008","_rn_":"164"},{"1":"subaru","2":"forester awd","3":"2008","_rn_":"165"},{"1":"subaru","2":"impreza awd","3":"1999","_rn_":"166"},{"1":"subaru","2":"impreza awd","3":"1999","_rn_":"167"},{"1":"subaru","2":"impreza awd","3":"1999","_rn_":"168"},{"1":"subaru","2":"impreza awd","3":"1999","_rn_":"169"},{"1":"subaru","2":"impreza awd","3":"2008","_rn_":"170"},{"1":"subaru","2":"impreza awd","3":"2008","_rn_":"171"},{"1":"subaru","2":"impreza awd","3":"2008","_rn_":"172"},{"1":"subaru","2":"impreza awd","3":"2008","_rn_":"173"},{"1":"toyota","2":"4runner 4wd","3":"1999","_rn_":"174"},{"1":"toyota","2":"4runner 4wd","3":"1999","_rn_":"175"},{"1":"toyota","2":"4runner 4wd","3":"1999","_rn_":"176"},{"1":"toyota","2":"4runner 4wd","3":"1999","_rn_":"177"},{"1":"toyota","2":"4runner 4wd","3":"2008","_rn_":"178"},{"1":"toyota","2":"4runner 4wd","3":"2008","_rn_":"179"},{"1":"toyota","2":"camry","3":"1999","_rn_":"180"},{"1":"toyota","2":"camry","3":"1999","_rn_":"181"},{"1":"toyota","2":"camry","3":"2008","_rn_":"182"},{"1":"toyota","2":"camry","3":"2008","_rn_":"183"},{"1":"toyota","2":"camry","3":"1999","_rn_":"184"},{"1":"toyota","2":"camry","3":"1999","_rn_":"185"},{"1":"toyota","2":"camry","3":"2008","_rn_":"186"},{"1":"toyota","2":"camry solara","3":"1999","_rn_":"187"},{"1":"toyota","2":"camry solara","3":"1999","_rn_":"188"},{"1":"toyota","2":"camry solara","3":"2008","_rn_":"189"},{"1":"toyota","2":"camry solara","3":"2008","_rn_":"190"},{"1":"toyota","2":"camry solara","3":"1999","_rn_":"191"},{"1":"toyota","2":"camry solara","3":"1999","_rn_":"192"},{"1":"toyota","2":"camry solara","3":"2008","_rn_":"193"},{"1":"toyota","2":"corolla","3":"1999","_rn_":"194"},{"1":"toyota","2":"corolla","3":"1999","_rn_":"195"},{"1":"toyota","2":"corolla","3":"1999","_rn_":"196"},{"1":"toyota","2":"corolla","3":"2008","_rn_":"197"},{"1":"toyota","2":"corolla","3":"2008","_rn_":"198"},{"1":"toyota","2":"land cruiser wagon 4wd","3":"1999","_rn_":"199"},{"1":"toyota","2":"land cruiser wagon 4wd","3":"2008","_rn_":"200"},{"1":"toyota","2":"toyota tacoma 4wd","3":"1999","_rn_":"201"},{"1":"toyota","2":"toyota tacoma 4wd","3":"1999","_rn_":"202"},{"1":"toyota","2":"toyota tacoma 4wd","3":"2008","_rn_":"203"},{"1":"toyota","2":"toyota tacoma 4wd","3":"1999","_rn_":"204"},{"1":"toyota","2":"toyota tacoma 4wd","3":"1999","_rn_":"205"},{"1":"toyota","2":"toyota tacoma 4wd","3":"2008","_rn_":"206"},{"1":"toyota","2":"toyota tacoma 4wd","3":"2008","_rn_":"207"},{"1":"volkswagen","2":"gti","3":"1999","_rn_":"208"},{"1":"volkswagen","2":"gti","3":"1999","_rn_":"209"},{"1":"volkswagen","2":"gti","3":"2008","_rn_":"210"},{"1":"volkswagen","2":"gti","3":"2008","_rn_":"211"},{"1":"volkswagen","2":"gti","3":"1999","_rn_":"212"},{"1":"volkswagen","2":"jetta","3":"1999","_rn_":"213"},{"1":"volkswagen","2":"jetta","3":"1999","_rn_":"214"},{"1":"volkswagen","2":"jetta","3":"1999","_rn_":"215"},{"1":"volkswagen","2":"jetta","3":"2008","_rn_":"216"},{"1":"volkswagen","2":"jetta","3":"2008","_rn_":"217"},{"1":"volkswagen","2":"jetta","3":"2008","_rn_":"218"},{"1":"volkswagen","2":"jetta","3":"2008","_rn_":"219"},{"1":"volkswagen","2":"jetta","3":"1999","_rn_":"220"},{"1":"volkswagen","2":"jetta","3":"1999","_rn_":"221"},{"1":"volkswagen","2":"new beetle","3":"1999","_rn_":"222"},{"1":"volkswagen","2":"new beetle","3":"1999","_rn_":"223"},{"1":"volkswagen","2":"new beetle","3":"1999","_rn_":"224"},{"1":"volkswagen","2":"new beetle","3":"1999","_rn_":"225"},{"1":"volkswagen","2":"new beetle","3":"2008","_rn_":"226"},{"1":"volkswagen","2":"new beetle","3":"2008","_rn_":"227"},{"1":"volkswagen","2":"passat","3":"1999","_rn_":"228"},{"1":"volkswagen","2":"passat","3":"1999","_rn_":"229"},{"1":"volkswagen","2":"passat","3":"2008","_rn_":"230"},{"1":"volkswagen","2":"passat","3":"2008","_rn_":"231"},{"1":"volkswagen","2":"passat","3":"1999","_rn_":"232"},{"1":"volkswagen","2":"passat","3":"1999","_rn_":"233"},{"1":"volkswagen","2":"passat","3":"2008","_rn_":"234"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

If we want to keep more columns than we want to drop, then we can use "-" with "select ="


```r
subset(mpg, select = -model)
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["manufacturer"],"name":[1],"type":["fct"],"align":["left"]},{"label":["displ"],"name":[2],"type":["dbl"],"align":["right"]},{"label":["year"],"name":[3],"type":["int"],"align":["right"]},{"label":["cyl"],"name":[4],"type":["int"],"align":["right"]},{"label":["trans"],"name":[5],"type":["chr"],"align":["left"]},{"label":["drv"],"name":[6],"type":["chr"],"align":["left"]},{"label":["cty"],"name":[7],"type":["int"],"align":["right"]},{"label":["hwy"],"name":[8],"type":["int"],"align":["right"]},{"label":["fl"],"name":[9],"type":["chr"],"align":["left"]},{"label":["class"],"name":[10],"type":["chr"],"align":["left"]}],"data":[{"1":"audi","2":"1.8","3":"1999","4":"4","5":"auto(l5)","6":"f","7":"18","8":"29","9":"p","10":"compact","_rn_":"1"},{"1":"audi","2":"1.8","3":"1999","4":"4","5":"manual(m5)","6":"f","7":"21","8":"29","9":"p","10":"compact","_rn_":"2"},{"1":"audi","2":"2.0","3":"2008","4":"4","5":"manual(m6)","6":"f","7":"20","8":"31","9":"p","10":"compact","_rn_":"3"},{"1":"audi","2":"2.0","3":"2008","4":"4","5":"auto(av)","6":"f","7":"21","8":"30","9":"p","10":"compact","_rn_":"4"},{"1":"audi","2":"2.8","3":"1999","4":"6","5":"auto(l5)","6":"f","7":"16","8":"26","9":"p","10":"compact","_rn_":"5"},{"1":"audi","2":"2.8","3":"1999","4":"6","5":"manual(m5)","6":"f","7":"18","8":"26","9":"p","10":"compact","_rn_":"6"},{"1":"audi","2":"3.1","3":"2008","4":"6","5":"auto(av)","6":"f","7":"18","8":"27","9":"p","10":"compact","_rn_":"7"},{"1":"audi","2":"1.8","3":"1999","4":"4","5":"manual(m5)","6":"4","7":"18","8":"26","9":"p","10":"compact","_rn_":"8"},{"1":"audi","2":"1.8","3":"1999","4":"4","5":"auto(l5)","6":"4","7":"16","8":"25","9":"p","10":"compact","_rn_":"9"},{"1":"audi","2":"2.0","3":"2008","4":"4","5":"manual(m6)","6":"4","7":"20","8":"28","9":"p","10":"compact","_rn_":"10"},{"1":"audi","2":"2.0","3":"2008","4":"4","5":"auto(s6)","6":"4","7":"19","8":"27","9":"p","10":"compact","_rn_":"11"},{"1":"audi","2":"2.8","3":"1999","4":"6","5":"auto(l5)","6":"4","7":"15","8":"25","9":"p","10":"compact","_rn_":"12"},{"1":"audi","2":"2.8","3":"1999","4":"6","5":"manual(m5)","6":"4","7":"17","8":"25","9":"p","10":"compact","_rn_":"13"},{"1":"audi","2":"3.1","3":"2008","4":"6","5":"auto(s6)","6":"4","7":"17","8":"25","9":"p","10":"compact","_rn_":"14"},{"1":"audi","2":"3.1","3":"2008","4":"6","5":"manual(m6)","6":"4","7":"15","8":"25","9":"p","10":"compact","_rn_":"15"},{"1":"audi","2":"2.8","3":"1999","4":"6","5":"auto(l5)","6":"4","7":"15","8":"24","9":"p","10":"midsize","_rn_":"16"},{"1":"audi","2":"3.1","3":"2008","4":"6","5":"auto(s6)","6":"4","7":"17","8":"25","9":"p","10":"midsize","_rn_":"17"},{"1":"audi","2":"4.2","3":"2008","4":"8","5":"auto(s6)","6":"4","7":"16","8":"23","9":"p","10":"midsize","_rn_":"18"},{"1":"chevrolet","2":"5.3","3":"2008","4":"8","5":"auto(l4)","6":"r","7":"14","8":"20","9":"r","10":"suv","_rn_":"19"},{"1":"chevrolet","2":"5.3","3":"2008","4":"8","5":"auto(l4)","6":"r","7":"11","8":"15","9":"e","10":"suv","_rn_":"20"},{"1":"chevrolet","2":"5.3","3":"2008","4":"8","5":"auto(l4)","6":"r","7":"14","8":"20","9":"r","10":"suv","_rn_":"21"},{"1":"chevrolet","2":"5.7","3":"1999","4":"8","5":"auto(l4)","6":"r","7":"13","8":"17","9":"r","10":"suv","_rn_":"22"},{"1":"chevrolet","2":"6.0","3":"2008","4":"8","5":"auto(l4)","6":"r","7":"12","8":"17","9":"r","10":"suv","_rn_":"23"},{"1":"chevrolet","2":"5.7","3":"1999","4":"8","5":"manual(m6)","6":"r","7":"16","8":"26","9":"p","10":"2seater","_rn_":"24"},{"1":"chevrolet","2":"5.7","3":"1999","4":"8","5":"auto(l4)","6":"r","7":"15","8":"23","9":"p","10":"2seater","_rn_":"25"},{"1":"chevrolet","2":"6.2","3":"2008","4":"8","5":"manual(m6)","6":"r","7":"16","8":"26","9":"p","10":"2seater","_rn_":"26"},{"1":"chevrolet","2":"6.2","3":"2008","4":"8","5":"auto(s6)","6":"r","7":"15","8":"25","9":"p","10":"2seater","_rn_":"27"},{"1":"chevrolet","2":"7.0","3":"2008","4":"8","5":"manual(m6)","6":"r","7":"15","8":"24","9":"p","10":"2seater","_rn_":"28"},{"1":"chevrolet","2":"5.3","3":"2008","4":"8","5":"auto(l4)","6":"4","7":"14","8":"19","9":"r","10":"suv","_rn_":"29"},{"1":"chevrolet","2":"5.3","3":"2008","4":"8","5":"auto(l4)","6":"4","7":"11","8":"14","9":"e","10":"suv","_rn_":"30"},{"1":"chevrolet","2":"5.7","3":"1999","4":"8","5":"auto(l4)","6":"4","7":"11","8":"15","9":"r","10":"suv","_rn_":"31"},{"1":"chevrolet","2":"6.5","3":"1999","4":"8","5":"auto(l4)","6":"4","7":"14","8":"17","9":"d","10":"suv","_rn_":"32"},{"1":"chevrolet","2":"2.4","3":"1999","4":"4","5":"auto(l4)","6":"f","7":"19","8":"27","9":"r","10":"midsize","_rn_":"33"},{"1":"chevrolet","2":"2.4","3":"2008","4":"4","5":"auto(l4)","6":"f","7":"22","8":"30","9":"r","10":"midsize","_rn_":"34"},{"1":"chevrolet","2":"3.1","3":"1999","4":"6","5":"auto(l4)","6":"f","7":"18","8":"26","9":"r","10":"midsize","_rn_":"35"},{"1":"chevrolet","2":"3.5","3":"2008","4":"6","5":"auto(l4)","6":"f","7":"18","8":"29","9":"r","10":"midsize","_rn_":"36"},{"1":"chevrolet","2":"3.6","3":"2008","4":"6","5":"auto(s6)","6":"f","7":"17","8":"26","9":"r","10":"midsize","_rn_":"37"},{"1":"dodge","2":"2.4","3":"1999","4":"4","5":"auto(l3)","6":"f","7":"18","8":"24","9":"r","10":"minivan","_rn_":"38"},{"1":"dodge","2":"3.0","3":"1999","4":"6","5":"auto(l4)","6":"f","7":"17","8":"24","9":"r","10":"minivan","_rn_":"39"},{"1":"dodge","2":"3.3","3":"1999","4":"6","5":"auto(l4)","6":"f","7":"16","8":"22","9":"r","10":"minivan","_rn_":"40"},{"1":"dodge","2":"3.3","3":"1999","4":"6","5":"auto(l4)","6":"f","7":"16","8":"22","9":"r","10":"minivan","_rn_":"41"},{"1":"dodge","2":"3.3","3":"2008","4":"6","5":"auto(l4)","6":"f","7":"17","8":"24","9":"r","10":"minivan","_rn_":"42"},{"1":"dodge","2":"3.3","3":"2008","4":"6","5":"auto(l4)","6":"f","7":"17","8":"24","9":"r","10":"minivan","_rn_":"43"},{"1":"dodge","2":"3.3","3":"2008","4":"6","5":"auto(l4)","6":"f","7":"11","8":"17","9":"e","10":"minivan","_rn_":"44"},{"1":"dodge","2":"3.8","3":"1999","4":"6","5":"auto(l4)","6":"f","7":"15","8":"22","9":"r","10":"minivan","_rn_":"45"},{"1":"dodge","2":"3.8","3":"1999","4":"6","5":"auto(l4)","6":"f","7":"15","8":"21","9":"r","10":"minivan","_rn_":"46"},{"1":"dodge","2":"3.8","3":"2008","4":"6","5":"auto(l6)","6":"f","7":"16","8":"23","9":"r","10":"minivan","_rn_":"47"},{"1":"dodge","2":"4.0","3":"2008","4":"6","5":"auto(l6)","6":"f","7":"16","8":"23","9":"r","10":"minivan","_rn_":"48"},{"1":"dodge","2":"3.7","3":"2008","4":"6","5":"manual(m6)","6":"4","7":"15","8":"19","9":"r","10":"pickup","_rn_":"49"},{"1":"dodge","2":"3.7","3":"2008","4":"6","5":"auto(l4)","6":"4","7":"14","8":"18","9":"r","10":"pickup","_rn_":"50"},{"1":"dodge","2":"3.9","3":"1999","4":"6","5":"auto(l4)","6":"4","7":"13","8":"17","9":"r","10":"pickup","_rn_":"51"},{"1":"dodge","2":"3.9","3":"1999","4":"6","5":"manual(m5)","6":"4","7":"14","8":"17","9":"r","10":"pickup","_rn_":"52"},{"1":"dodge","2":"4.7","3":"2008","4":"8","5":"auto(l5)","6":"4","7":"14","8":"19","9":"r","10":"pickup","_rn_":"53"},{"1":"dodge","2":"4.7","3":"2008","4":"8","5":"auto(l5)","6":"4","7":"14","8":"19","9":"r","10":"pickup","_rn_":"54"},{"1":"dodge","2":"4.7","3":"2008","4":"8","5":"auto(l5)","6":"4","7":"9","8":"12","9":"e","10":"pickup","_rn_":"55"},{"1":"dodge","2":"5.2","3":"1999","4":"8","5":"manual(m5)","6":"4","7":"11","8":"17","9":"r","10":"pickup","_rn_":"56"},{"1":"dodge","2":"5.2","3":"1999","4":"8","5":"auto(l4)","6":"4","7":"11","8":"15","9":"r","10":"pickup","_rn_":"57"},{"1":"dodge","2":"3.9","3":"1999","4":"6","5":"auto(l4)","6":"4","7":"13","8":"17","9":"r","10":"suv","_rn_":"58"},{"1":"dodge","2":"4.7","3":"2008","4":"8","5":"auto(l5)","6":"4","7":"13","8":"17","9":"r","10":"suv","_rn_":"59"},{"1":"dodge","2":"4.7","3":"2008","4":"8","5":"auto(l5)","6":"4","7":"9","8":"12","9":"e","10":"suv","_rn_":"60"},{"1":"dodge","2":"4.7","3":"2008","4":"8","5":"auto(l5)","6":"4","7":"13","8":"17","9":"r","10":"suv","_rn_":"61"},{"1":"dodge","2":"5.2","3":"1999","4":"8","5":"auto(l4)","6":"4","7":"11","8":"16","9":"r","10":"suv","_rn_":"62"},{"1":"dodge","2":"5.7","3":"2008","4":"8","5":"auto(l5)","6":"4","7":"13","8":"18","9":"r","10":"suv","_rn_":"63"},{"1":"dodge","2":"5.9","3":"1999","4":"8","5":"auto(l4)","6":"4","7":"11","8":"15","9":"r","10":"suv","_rn_":"64"},{"1":"dodge","2":"4.7","3":"2008","4":"8","5":"manual(m6)","6":"4","7":"12","8":"16","9":"r","10":"pickup","_rn_":"65"},{"1":"dodge","2":"4.7","3":"2008","4":"8","5":"auto(l5)","6":"4","7":"9","8":"12","9":"e","10":"pickup","_rn_":"66"},{"1":"dodge","2":"4.7","3":"2008","4":"8","5":"auto(l5)","6":"4","7":"13","8":"17","9":"r","10":"pickup","_rn_":"67"},{"1":"dodge","2":"4.7","3":"2008","4":"8","5":"auto(l5)","6":"4","7":"13","8":"17","9":"r","10":"pickup","_rn_":"68"},{"1":"dodge","2":"4.7","3":"2008","4":"8","5":"manual(m6)","6":"4","7":"12","8":"16","9":"r","10":"pickup","_rn_":"69"},{"1":"dodge","2":"4.7","3":"2008","4":"8","5":"manual(m6)","6":"4","7":"9","8":"12","9":"e","10":"pickup","_rn_":"70"},{"1":"dodge","2":"5.2","3":"1999","4":"8","5":"auto(l4)","6":"4","7":"11","8":"15","9":"r","10":"pickup","_rn_":"71"},{"1":"dodge","2":"5.2","3":"1999","4":"8","5":"manual(m5)","6":"4","7":"11","8":"16","9":"r","10":"pickup","_rn_":"72"},{"1":"dodge","2":"5.7","3":"2008","4":"8","5":"auto(l5)","6":"4","7":"13","8":"17","9":"r","10":"pickup","_rn_":"73"},{"1":"dodge","2":"5.9","3":"1999","4":"8","5":"auto(l4)","6":"4","7":"11","8":"15","9":"r","10":"pickup","_rn_":"74"},{"1":"ford","2":"4.6","3":"1999","4":"8","5":"auto(l4)","6":"r","7":"11","8":"17","9":"r","10":"suv","_rn_":"75"},{"1":"ford","2":"5.4","3":"1999","4":"8","5":"auto(l4)","6":"r","7":"11","8":"17","9":"r","10":"suv","_rn_":"76"},{"1":"ford","2":"5.4","3":"2008","4":"8","5":"auto(l6)","6":"r","7":"12","8":"18","9":"r","10":"suv","_rn_":"77"},{"1":"ford","2":"4.0","3":"1999","4":"6","5":"auto(l5)","6":"4","7":"14","8":"17","9":"r","10":"suv","_rn_":"78"},{"1":"ford","2":"4.0","3":"1999","4":"6","5":"manual(m5)","6":"4","7":"15","8":"19","9":"r","10":"suv","_rn_":"79"},{"1":"ford","2":"4.0","3":"1999","4":"6","5":"auto(l5)","6":"4","7":"14","8":"17","9":"r","10":"suv","_rn_":"80"},{"1":"ford","2":"4.0","3":"2008","4":"6","5":"auto(l5)","6":"4","7":"13","8":"19","9":"r","10":"suv","_rn_":"81"},{"1":"ford","2":"4.6","3":"2008","4":"8","5":"auto(l6)","6":"4","7":"13","8":"19","9":"r","10":"suv","_rn_":"82"},{"1":"ford","2":"5.0","3":"1999","4":"8","5":"auto(l4)","6":"4","7":"13","8":"17","9":"r","10":"suv","_rn_":"83"},{"1":"ford","2":"4.2","3":"1999","4":"6","5":"auto(l4)","6":"4","7":"14","8":"17","9":"r","10":"pickup","_rn_":"84"},{"1":"ford","2":"4.2","3":"1999","4":"6","5":"manual(m5)","6":"4","7":"14","8":"17","9":"r","10":"pickup","_rn_":"85"},{"1":"ford","2":"4.6","3":"1999","4":"8","5":"manual(m5)","6":"4","7":"13","8":"16","9":"r","10":"pickup","_rn_":"86"},{"1":"ford","2":"4.6","3":"1999","4":"8","5":"auto(l4)","6":"4","7":"13","8":"16","9":"r","10":"pickup","_rn_":"87"},{"1":"ford","2":"4.6","3":"2008","4":"8","5":"auto(l4)","6":"4","7":"13","8":"17","9":"r","10":"pickup","_rn_":"88"},{"1":"ford","2":"5.4","3":"1999","4":"8","5":"auto(l4)","6":"4","7":"11","8":"15","9":"r","10":"pickup","_rn_":"89"},{"1":"ford","2":"5.4","3":"2008","4":"8","5":"auto(l4)","6":"4","7":"13","8":"17","9":"r","10":"pickup","_rn_":"90"},{"1":"ford","2":"3.8","3":"1999","4":"6","5":"manual(m5)","6":"r","7":"18","8":"26","9":"r","10":"subcompact","_rn_":"91"},{"1":"ford","2":"3.8","3":"1999","4":"6","5":"auto(l4)","6":"r","7":"18","8":"25","9":"r","10":"subcompact","_rn_":"92"},{"1":"ford","2":"4.0","3":"2008","4":"6","5":"manual(m5)","6":"r","7":"17","8":"26","9":"r","10":"subcompact","_rn_":"93"},{"1":"ford","2":"4.0","3":"2008","4":"6","5":"auto(l5)","6":"r","7":"16","8":"24","9":"r","10":"subcompact","_rn_":"94"},{"1":"ford","2":"4.6","3":"1999","4":"8","5":"auto(l4)","6":"r","7":"15","8":"21","9":"r","10":"subcompact","_rn_":"95"},{"1":"ford","2":"4.6","3":"1999","4":"8","5":"manual(m5)","6":"r","7":"15","8":"22","9":"r","10":"subcompact","_rn_":"96"},{"1":"ford","2":"4.6","3":"2008","4":"8","5":"manual(m5)","6":"r","7":"15","8":"23","9":"r","10":"subcompact","_rn_":"97"},{"1":"ford","2":"4.6","3":"2008","4":"8","5":"auto(l5)","6":"r","7":"15","8":"22","9":"r","10":"subcompact","_rn_":"98"},{"1":"ford","2":"5.4","3":"2008","4":"8","5":"manual(m6)","6":"r","7":"14","8":"20","9":"p","10":"subcompact","_rn_":"99"},{"1":"honda","2":"1.6","3":"1999","4":"4","5":"manual(m5)","6":"f","7":"28","8":"33","9":"r","10":"subcompact","_rn_":"100"},{"1":"honda","2":"1.6","3":"1999","4":"4","5":"auto(l4)","6":"f","7":"24","8":"32","9":"r","10":"subcompact","_rn_":"101"},{"1":"honda","2":"1.6","3":"1999","4":"4","5":"manual(m5)","6":"f","7":"25","8":"32","9":"r","10":"subcompact","_rn_":"102"},{"1":"honda","2":"1.6","3":"1999","4":"4","5":"manual(m5)","6":"f","7":"23","8":"29","9":"p","10":"subcompact","_rn_":"103"},{"1":"honda","2":"1.6","3":"1999","4":"4","5":"auto(l4)","6":"f","7":"24","8":"32","9":"r","10":"subcompact","_rn_":"104"},{"1":"honda","2":"1.8","3":"2008","4":"4","5":"manual(m5)","6":"f","7":"26","8":"34","9":"r","10":"subcompact","_rn_":"105"},{"1":"honda","2":"1.8","3":"2008","4":"4","5":"auto(l5)","6":"f","7":"25","8":"36","9":"r","10":"subcompact","_rn_":"106"},{"1":"honda","2":"1.8","3":"2008","4":"4","5":"auto(l5)","6":"f","7":"24","8":"36","9":"c","10":"subcompact","_rn_":"107"},{"1":"honda","2":"2.0","3":"2008","4":"4","5":"manual(m6)","6":"f","7":"21","8":"29","9":"p","10":"subcompact","_rn_":"108"},{"1":"hyundai","2":"2.4","3":"1999","4":"4","5":"auto(l4)","6":"f","7":"18","8":"26","9":"r","10":"midsize","_rn_":"109"},{"1":"hyundai","2":"2.4","3":"1999","4":"4","5":"manual(m5)","6":"f","7":"18","8":"27","9":"r","10":"midsize","_rn_":"110"},{"1":"hyundai","2":"2.4","3":"2008","4":"4","5":"auto(l4)","6":"f","7":"21","8":"30","9":"r","10":"midsize","_rn_":"111"},{"1":"hyundai","2":"2.4","3":"2008","4":"4","5":"manual(m5)","6":"f","7":"21","8":"31","9":"r","10":"midsize","_rn_":"112"},{"1":"hyundai","2":"2.5","3":"1999","4":"6","5":"auto(l4)","6":"f","7":"18","8":"26","9":"r","10":"midsize","_rn_":"113"},{"1":"hyundai","2":"2.5","3":"1999","4":"6","5":"manual(m5)","6":"f","7":"18","8":"26","9":"r","10":"midsize","_rn_":"114"},{"1":"hyundai","2":"3.3","3":"2008","4":"6","5":"auto(l5)","6":"f","7":"19","8":"28","9":"r","10":"midsize","_rn_":"115"},{"1":"hyundai","2":"2.0","3":"1999","4":"4","5":"auto(l4)","6":"f","7":"19","8":"26","9":"r","10":"subcompact","_rn_":"116"},{"1":"hyundai","2":"2.0","3":"1999","4":"4","5":"manual(m5)","6":"f","7":"19","8":"29","9":"r","10":"subcompact","_rn_":"117"},{"1":"hyundai","2":"2.0","3":"2008","4":"4","5":"manual(m5)","6":"f","7":"20","8":"28","9":"r","10":"subcompact","_rn_":"118"},{"1":"hyundai","2":"2.0","3":"2008","4":"4","5":"auto(l4)","6":"f","7":"20","8":"27","9":"r","10":"subcompact","_rn_":"119"},{"1":"hyundai","2":"2.7","3":"2008","4":"6","5":"auto(l4)","6":"f","7":"17","8":"24","9":"r","10":"subcompact","_rn_":"120"},{"1":"hyundai","2":"2.7","3":"2008","4":"6","5":"manual(m6)","6":"f","7":"16","8":"24","9":"r","10":"subcompact","_rn_":"121"},{"1":"hyundai","2":"2.7","3":"2008","4":"6","5":"manual(m5)","6":"f","7":"17","8":"24","9":"r","10":"subcompact","_rn_":"122"},{"1":"jeep","2":"3.0","3":"2008","4":"6","5":"auto(l5)","6":"4","7":"17","8":"22","9":"d","10":"suv","_rn_":"123"},{"1":"jeep","2":"3.7","3":"2008","4":"6","5":"auto(l5)","6":"4","7":"15","8":"19","9":"r","10":"suv","_rn_":"124"},{"1":"jeep","2":"4.0","3":"1999","4":"6","5":"auto(l4)","6":"4","7":"15","8":"20","9":"r","10":"suv","_rn_":"125"},{"1":"jeep","2":"4.7","3":"1999","4":"8","5":"auto(l4)","6":"4","7":"14","8":"17","9":"r","10":"suv","_rn_":"126"},{"1":"jeep","2":"4.7","3":"2008","4":"8","5":"auto(l5)","6":"4","7":"9","8":"12","9":"e","10":"suv","_rn_":"127"},{"1":"jeep","2":"4.7","3":"2008","4":"8","5":"auto(l5)","6":"4","7":"14","8":"19","9":"r","10":"suv","_rn_":"128"},{"1":"jeep","2":"5.7","3":"2008","4":"8","5":"auto(l5)","6":"4","7":"13","8":"18","9":"r","10":"suv","_rn_":"129"},{"1":"jeep","2":"6.1","3":"2008","4":"8","5":"auto(l5)","6":"4","7":"11","8":"14","9":"p","10":"suv","_rn_":"130"},{"1":"land rover","2":"4.0","3":"1999","4":"8","5":"auto(l4)","6":"4","7":"11","8":"15","9":"p","10":"suv","_rn_":"131"},{"1":"land rover","2":"4.2","3":"2008","4":"8","5":"auto(s6)","6":"4","7":"12","8":"18","9":"r","10":"suv","_rn_":"132"},{"1":"land rover","2":"4.4","3":"2008","4":"8","5":"auto(s6)","6":"4","7":"12","8":"18","9":"r","10":"suv","_rn_":"133"},{"1":"land rover","2":"4.6","3":"1999","4":"8","5":"auto(l4)","6":"4","7":"11","8":"15","9":"p","10":"suv","_rn_":"134"},{"1":"lincoln","2":"5.4","3":"1999","4":"8","5":"auto(l4)","6":"r","7":"11","8":"17","9":"r","10":"suv","_rn_":"135"},{"1":"lincoln","2":"5.4","3":"1999","4":"8","5":"auto(l4)","6":"r","7":"11","8":"16","9":"p","10":"suv","_rn_":"136"},{"1":"lincoln","2":"5.4","3":"2008","4":"8","5":"auto(l6)","6":"r","7":"12","8":"18","9":"r","10":"suv","_rn_":"137"},{"1":"mercury","2":"4.0","3":"1999","4":"6","5":"auto(l5)","6":"4","7":"14","8":"17","9":"r","10":"suv","_rn_":"138"},{"1":"mercury","2":"4.0","3":"2008","4":"6","5":"auto(l5)","6":"4","7":"13","8":"19","9":"r","10":"suv","_rn_":"139"},{"1":"mercury","2":"4.6","3":"2008","4":"8","5":"auto(l6)","6":"4","7":"13","8":"19","9":"r","10":"suv","_rn_":"140"},{"1":"mercury","2":"5.0","3":"1999","4":"8","5":"auto(l4)","6":"4","7":"13","8":"17","9":"r","10":"suv","_rn_":"141"},{"1":"nissan","2":"2.4","3":"1999","4":"4","5":"manual(m5)","6":"f","7":"21","8":"29","9":"r","10":"compact","_rn_":"142"},{"1":"nissan","2":"2.4","3":"1999","4":"4","5":"auto(l4)","6":"f","7":"19","8":"27","9":"r","10":"compact","_rn_":"143"},{"1":"nissan","2":"2.5","3":"2008","4":"4","5":"auto(av)","6":"f","7":"23","8":"31","9":"r","10":"midsize","_rn_":"144"},{"1":"nissan","2":"2.5","3":"2008","4":"4","5":"manual(m6)","6":"f","7":"23","8":"32","9":"r","10":"midsize","_rn_":"145"},{"1":"nissan","2":"3.5","3":"2008","4":"6","5":"manual(m6)","6":"f","7":"19","8":"27","9":"p","10":"midsize","_rn_":"146"},{"1":"nissan","2":"3.5","3":"2008","4":"6","5":"auto(av)","6":"f","7":"19","8":"26","9":"p","10":"midsize","_rn_":"147"},{"1":"nissan","2":"3.0","3":"1999","4":"6","5":"auto(l4)","6":"f","7":"18","8":"26","9":"r","10":"midsize","_rn_":"148"},{"1":"nissan","2":"3.0","3":"1999","4":"6","5":"manual(m5)","6":"f","7":"19","8":"25","9":"r","10":"midsize","_rn_":"149"},{"1":"nissan","2":"3.5","3":"2008","4":"6","5":"auto(av)","6":"f","7":"19","8":"25","9":"p","10":"midsize","_rn_":"150"},{"1":"nissan","2":"3.3","3":"1999","4":"6","5":"auto(l4)","6":"4","7":"14","8":"17","9":"r","10":"suv","_rn_":"151"},{"1":"nissan","2":"3.3","3":"1999","4":"6","5":"manual(m5)","6":"4","7":"15","8":"17","9":"r","10":"suv","_rn_":"152"},{"1":"nissan","2":"4.0","3":"2008","4":"6","5":"auto(l5)","6":"4","7":"14","8":"20","9":"p","10":"suv","_rn_":"153"},{"1":"nissan","2":"5.6","3":"2008","4":"8","5":"auto(s5)","6":"4","7":"12","8":"18","9":"p","10":"suv","_rn_":"154"},{"1":"pontiac","2":"3.1","3":"1999","4":"6","5":"auto(l4)","6":"f","7":"18","8":"26","9":"r","10":"midsize","_rn_":"155"},{"1":"pontiac","2":"3.8","3":"1999","4":"6","5":"auto(l4)","6":"f","7":"16","8":"26","9":"p","10":"midsize","_rn_":"156"},{"1":"pontiac","2":"3.8","3":"1999","4":"6","5":"auto(l4)","6":"f","7":"17","8":"27","9":"r","10":"midsize","_rn_":"157"},{"1":"pontiac","2":"3.8","3":"2008","4":"6","5":"auto(l4)","6":"f","7":"18","8":"28","9":"r","10":"midsize","_rn_":"158"},{"1":"pontiac","2":"5.3","3":"2008","4":"8","5":"auto(s4)","6":"f","7":"16","8":"25","9":"p","10":"midsize","_rn_":"159"},{"1":"subaru","2":"2.5","3":"1999","4":"4","5":"manual(m5)","6":"4","7":"18","8":"25","9":"r","10":"suv","_rn_":"160"},{"1":"subaru","2":"2.5","3":"1999","4":"4","5":"auto(l4)","6":"4","7":"18","8":"24","9":"r","10":"suv","_rn_":"161"},{"1":"subaru","2":"2.5","3":"2008","4":"4","5":"manual(m5)","6":"4","7":"20","8":"27","9":"r","10":"suv","_rn_":"162"},{"1":"subaru","2":"2.5","3":"2008","4":"4","5":"manual(m5)","6":"4","7":"19","8":"25","9":"p","10":"suv","_rn_":"163"},{"1":"subaru","2":"2.5","3":"2008","4":"4","5":"auto(l4)","6":"4","7":"20","8":"26","9":"r","10":"suv","_rn_":"164"},{"1":"subaru","2":"2.5","3":"2008","4":"4","5":"auto(l4)","6":"4","7":"18","8":"23","9":"p","10":"suv","_rn_":"165"},{"1":"subaru","2":"2.2","3":"1999","4":"4","5":"auto(l4)","6":"4","7":"21","8":"26","9":"r","10":"subcompact","_rn_":"166"},{"1":"subaru","2":"2.2","3":"1999","4":"4","5":"manual(m5)","6":"4","7":"19","8":"26","9":"r","10":"subcompact","_rn_":"167"},{"1":"subaru","2":"2.5","3":"1999","4":"4","5":"manual(m5)","6":"4","7":"19","8":"26","9":"r","10":"subcompact","_rn_":"168"},{"1":"subaru","2":"2.5","3":"1999","4":"4","5":"auto(l4)","6":"4","7":"19","8":"26","9":"r","10":"subcompact","_rn_":"169"},{"1":"subaru","2":"2.5","3":"2008","4":"4","5":"auto(s4)","6":"4","7":"20","8":"25","9":"p","10":"compact","_rn_":"170"},{"1":"subaru","2":"2.5","3":"2008","4":"4","5":"auto(s4)","6":"4","7":"20","8":"27","9":"r","10":"compact","_rn_":"171"},{"1":"subaru","2":"2.5","3":"2008","4":"4","5":"manual(m5)","6":"4","7":"19","8":"25","9":"p","10":"compact","_rn_":"172"},{"1":"subaru","2":"2.5","3":"2008","4":"4","5":"manual(m5)","6":"4","7":"20","8":"27","9":"r","10":"compact","_rn_":"173"},{"1":"toyota","2":"2.7","3":"1999","4":"4","5":"manual(m5)","6":"4","7":"15","8":"20","9":"r","10":"suv","_rn_":"174"},{"1":"toyota","2":"2.7","3":"1999","4":"4","5":"auto(l4)","6":"4","7":"16","8":"20","9":"r","10":"suv","_rn_":"175"},{"1":"toyota","2":"3.4","3":"1999","4":"6","5":"auto(l4)","6":"4","7":"15","8":"19","9":"r","10":"suv","_rn_":"176"},{"1":"toyota","2":"3.4","3":"1999","4":"6","5":"manual(m5)","6":"4","7":"15","8":"17","9":"r","10":"suv","_rn_":"177"},{"1":"toyota","2":"4.0","3":"2008","4":"6","5":"auto(l5)","6":"4","7":"16","8":"20","9":"r","10":"suv","_rn_":"178"},{"1":"toyota","2":"4.7","3":"2008","4":"8","5":"auto(l5)","6":"4","7":"14","8":"17","9":"r","10":"suv","_rn_":"179"},{"1":"toyota","2":"2.2","3":"1999","4":"4","5":"manual(m5)","6":"f","7":"21","8":"29","9":"r","10":"midsize","_rn_":"180"},{"1":"toyota","2":"2.2","3":"1999","4":"4","5":"auto(l4)","6":"f","7":"21","8":"27","9":"r","10":"midsize","_rn_":"181"},{"1":"toyota","2":"2.4","3":"2008","4":"4","5":"manual(m5)","6":"f","7":"21","8":"31","9":"r","10":"midsize","_rn_":"182"},{"1":"toyota","2":"2.4","3":"2008","4":"4","5":"auto(l5)","6":"f","7":"21","8":"31","9":"r","10":"midsize","_rn_":"183"},{"1":"toyota","2":"3.0","3":"1999","4":"6","5":"auto(l4)","6":"f","7":"18","8":"26","9":"r","10":"midsize","_rn_":"184"},{"1":"toyota","2":"3.0","3":"1999","4":"6","5":"manual(m5)","6":"f","7":"18","8":"26","9":"r","10":"midsize","_rn_":"185"},{"1":"toyota","2":"3.5","3":"2008","4":"6","5":"auto(s6)","6":"f","7":"19","8":"28","9":"r","10":"midsize","_rn_":"186"},{"1":"toyota","2":"2.2","3":"1999","4":"4","5":"auto(l4)","6":"f","7":"21","8":"27","9":"r","10":"compact","_rn_":"187"},{"1":"toyota","2":"2.2","3":"1999","4":"4","5":"manual(m5)","6":"f","7":"21","8":"29","9":"r","10":"compact","_rn_":"188"},{"1":"toyota","2":"2.4","3":"2008","4":"4","5":"manual(m5)","6":"f","7":"21","8":"31","9":"r","10":"compact","_rn_":"189"},{"1":"toyota","2":"2.4","3":"2008","4":"4","5":"auto(s5)","6":"f","7":"22","8":"31","9":"r","10":"compact","_rn_":"190"},{"1":"toyota","2":"3.0","3":"1999","4":"6","5":"auto(l4)","6":"f","7":"18","8":"26","9":"r","10":"compact","_rn_":"191"},{"1":"toyota","2":"3.0","3":"1999","4":"6","5":"manual(m5)","6":"f","7":"18","8":"26","9":"r","10":"compact","_rn_":"192"},{"1":"toyota","2":"3.3","3":"2008","4":"6","5":"auto(s5)","6":"f","7":"18","8":"27","9":"r","10":"compact","_rn_":"193"},{"1":"toyota","2":"1.8","3":"1999","4":"4","5":"auto(l3)","6":"f","7":"24","8":"30","9":"r","10":"compact","_rn_":"194"},{"1":"toyota","2":"1.8","3":"1999","4":"4","5":"auto(l4)","6":"f","7":"24","8":"33","9":"r","10":"compact","_rn_":"195"},{"1":"toyota","2":"1.8","3":"1999","4":"4","5":"manual(m5)","6":"f","7":"26","8":"35","9":"r","10":"compact","_rn_":"196"},{"1":"toyota","2":"1.8","3":"2008","4":"4","5":"manual(m5)","6":"f","7":"28","8":"37","9":"r","10":"compact","_rn_":"197"},{"1":"toyota","2":"1.8","3":"2008","4":"4","5":"auto(l4)","6":"f","7":"26","8":"35","9":"r","10":"compact","_rn_":"198"},{"1":"toyota","2":"4.7","3":"1999","4":"8","5":"auto(l4)","6":"4","7":"11","8":"15","9":"r","10":"suv","_rn_":"199"},{"1":"toyota","2":"5.7","3":"2008","4":"8","5":"auto(s6)","6":"4","7":"13","8":"18","9":"r","10":"suv","_rn_":"200"},{"1":"toyota","2":"2.7","3":"1999","4":"4","5":"manual(m5)","6":"4","7":"15","8":"20","9":"r","10":"pickup","_rn_":"201"},{"1":"toyota","2":"2.7","3":"1999","4":"4","5":"auto(l4)","6":"4","7":"16","8":"20","9":"r","10":"pickup","_rn_":"202"},{"1":"toyota","2":"2.7","3":"2008","4":"4","5":"manual(m5)","6":"4","7":"17","8":"22","9":"r","10":"pickup","_rn_":"203"},{"1":"toyota","2":"3.4","3":"1999","4":"6","5":"manual(m5)","6":"4","7":"15","8":"17","9":"r","10":"pickup","_rn_":"204"},{"1":"toyota","2":"3.4","3":"1999","4":"6","5":"auto(l4)","6":"4","7":"15","8":"19","9":"r","10":"pickup","_rn_":"205"},{"1":"toyota","2":"4.0","3":"2008","4":"6","5":"manual(m6)","6":"4","7":"15","8":"18","9":"r","10":"pickup","_rn_":"206"},{"1":"toyota","2":"4.0","3":"2008","4":"6","5":"auto(l5)","6":"4","7":"16","8":"20","9":"r","10":"pickup","_rn_":"207"},{"1":"volkswagen","2":"2.0","3":"1999","4":"4","5":"manual(m5)","6":"f","7":"21","8":"29","9":"r","10":"compact","_rn_":"208"},{"1":"volkswagen","2":"2.0","3":"1999","4":"4","5":"auto(l4)","6":"f","7":"19","8":"26","9":"r","10":"compact","_rn_":"209"},{"1":"volkswagen","2":"2.0","3":"2008","4":"4","5":"manual(m6)","6":"f","7":"21","8":"29","9":"p","10":"compact","_rn_":"210"},{"1":"volkswagen","2":"2.0","3":"2008","4":"4","5":"auto(s6)","6":"f","7":"22","8":"29","9":"p","10":"compact","_rn_":"211"},{"1":"volkswagen","2":"2.8","3":"1999","4":"6","5":"manual(m5)","6":"f","7":"17","8":"24","9":"r","10":"compact","_rn_":"212"},{"1":"volkswagen","2":"1.9","3":"1999","4":"4","5":"manual(m5)","6":"f","7":"33","8":"44","9":"d","10":"compact","_rn_":"213"},{"1":"volkswagen","2":"2.0","3":"1999","4":"4","5":"manual(m5)","6":"f","7":"21","8":"29","9":"r","10":"compact","_rn_":"214"},{"1":"volkswagen","2":"2.0","3":"1999","4":"4","5":"auto(l4)","6":"f","7":"19","8":"26","9":"r","10":"compact","_rn_":"215"},{"1":"volkswagen","2":"2.0","3":"2008","4":"4","5":"auto(s6)","6":"f","7":"22","8":"29","9":"p","10":"compact","_rn_":"216"},{"1":"volkswagen","2":"2.0","3":"2008","4":"4","5":"manual(m6)","6":"f","7":"21","8":"29","9":"p","10":"compact","_rn_":"217"},{"1":"volkswagen","2":"2.5","3":"2008","4":"5","5":"auto(s6)","6":"f","7":"21","8":"29","9":"r","10":"compact","_rn_":"218"},{"1":"volkswagen","2":"2.5","3":"2008","4":"5","5":"manual(m5)","6":"f","7":"21","8":"29","9":"r","10":"compact","_rn_":"219"},{"1":"volkswagen","2":"2.8","3":"1999","4":"6","5":"auto(l4)","6":"f","7":"16","8":"23","9":"r","10":"compact","_rn_":"220"},{"1":"volkswagen","2":"2.8","3":"1999","4":"6","5":"manual(m5)","6":"f","7":"17","8":"24","9":"r","10":"compact","_rn_":"221"},{"1":"volkswagen","2":"1.9","3":"1999","4":"4","5":"manual(m5)","6":"f","7":"35","8":"44","9":"d","10":"subcompact","_rn_":"222"},{"1":"volkswagen","2":"1.9","3":"1999","4":"4","5":"auto(l4)","6":"f","7":"29","8":"41","9":"d","10":"subcompact","_rn_":"223"},{"1":"volkswagen","2":"2.0","3":"1999","4":"4","5":"manual(m5)","6":"f","7":"21","8":"29","9":"r","10":"subcompact","_rn_":"224"},{"1":"volkswagen","2":"2.0","3":"1999","4":"4","5":"auto(l4)","6":"f","7":"19","8":"26","9":"r","10":"subcompact","_rn_":"225"},{"1":"volkswagen","2":"2.5","3":"2008","4":"5","5":"manual(m5)","6":"f","7":"20","8":"28","9":"r","10":"subcompact","_rn_":"226"},{"1":"volkswagen","2":"2.5","3":"2008","4":"5","5":"auto(s6)","6":"f","7":"20","8":"29","9":"r","10":"subcompact","_rn_":"227"},{"1":"volkswagen","2":"1.8","3":"1999","4":"4","5":"manual(m5)","6":"f","7":"21","8":"29","9":"p","10":"midsize","_rn_":"228"},{"1":"volkswagen","2":"1.8","3":"1999","4":"4","5":"auto(l5)","6":"f","7":"18","8":"29","9":"p","10":"midsize","_rn_":"229"},{"1":"volkswagen","2":"2.0","3":"2008","4":"4","5":"auto(s6)","6":"f","7":"19","8":"28","9":"p","10":"midsize","_rn_":"230"},{"1":"volkswagen","2":"2.0","3":"2008","4":"4","5":"manual(m6)","6":"f","7":"21","8":"29","9":"p","10":"midsize","_rn_":"231"},{"1":"volkswagen","2":"2.8","3":"1999","4":"6","5":"auto(l5)","6":"f","7":"16","8":"26","9":"p","10":"midsize","_rn_":"232"},{"1":"volkswagen","2":"2.8","3":"1999","4":"6","5":"manual(m5)","6":"f","7":"18","8":"26","9":"p","10":"midsize","_rn_":"233"},{"1":"volkswagen","2":"3.6","3":"2008","4":"6","5":"auto(s6)","6":"f","7":"17","8":"26","9":"p","10":"midsize","_rn_":"234"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

### Subsetting Rows with "subset ="

To subset rows based on specific criteria, we can use "subset = ". For example, let's only keep rows with city MPG > 24.


```r
subset(mpg, subset = cty > 24)
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["manufacturer"],"name":[1],"type":["fct"],"align":["left"]},{"label":["model"],"name":[2],"type":["fct"],"align":["left"]},{"label":["displ"],"name":[3],"type":["dbl"],"align":["right"]},{"label":["year"],"name":[4],"type":["int"],"align":["right"]},{"label":["cyl"],"name":[5],"type":["int"],"align":["right"]},{"label":["trans"],"name":[6],"type":["chr"],"align":["left"]},{"label":["drv"],"name":[7],"type":["chr"],"align":["left"]},{"label":["cty"],"name":[8],"type":["int"],"align":["right"]},{"label":["hwy"],"name":[9],"type":["int"],"align":["right"]},{"label":["fl"],"name":[10],"type":["chr"],"align":["left"]},{"label":["class"],"name":[11],"type":["chr"],"align":["left"]}],"data":[{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"28","9":"33","10":"r","11":"subcompact","_rn_":"100"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"25","9":"32","10":"r","11":"subcompact","_rn_":"102"},{"1":"honda","2":"civic","3":"1.8","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"26","9":"34","10":"r","11":"subcompact","_rn_":"105"},{"1":"honda","2":"civic","3":"1.8","4":"2008","5":"4","6":"auto(l5)","7":"f","8":"25","9":"36","10":"r","11":"subcompact","_rn_":"106"},{"1":"toyota","2":"corolla","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"26","9":"35","10":"r","11":"compact","_rn_":"196"},{"1":"toyota","2":"corolla","3":"1.8","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"28","9":"37","10":"r","11":"compact","_rn_":"197"},{"1":"toyota","2":"corolla","3":"1.8","4":"2008","5":"4","6":"auto(l4)","7":"f","8":"26","9":"35","10":"r","11":"compact","_rn_":"198"},{"1":"volkswagen","2":"jetta","3":"1.9","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"33","9":"44","10":"d","11":"compact","_rn_":"213"},{"1":"volkswagen","2":"new beetle","3":"1.9","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"35","9":"44","10":"d","11":"subcompact","_rn_":"222"},{"1":"volkswagen","2":"new beetle","3":"1.9","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"29","9":"41","10":"d","11":"subcompact","_rn_":"223"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

Or we can also use multiple criteria, such as city MPG > 18 and cars with 6-cylinder engines.


```r
subset(mpg, subset = cty > 18 & cyl == 6)
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["manufacturer"],"name":[1],"type":["fct"],"align":["left"]},{"label":["model"],"name":[2],"type":["fct"],"align":["left"]},{"label":["displ"],"name":[3],"type":["dbl"],"align":["right"]},{"label":["year"],"name":[4],"type":["int"],"align":["right"]},{"label":["cyl"],"name":[5],"type":["int"],"align":["right"]},{"label":["trans"],"name":[6],"type":["chr"],"align":["left"]},{"label":["drv"],"name":[7],"type":["chr"],"align":["left"]},{"label":["cty"],"name":[8],"type":["int"],"align":["right"]},{"label":["hwy"],"name":[9],"type":["int"],"align":["right"]},{"label":["fl"],"name":[10],"type":["chr"],"align":["left"]},{"label":["class"],"name":[11],"type":["chr"],"align":["left"]}],"data":[{"1":"hyundai","2":"sonata","3":"3.3","4":"2008","5":"6","6":"auto(l5)","7":"f","8":"19","9":"28","10":"r","11":"midsize","_rn_":"115"},{"1":"nissan","2":"altima","3":"3.5","4":"2008","5":"6","6":"manual(m6)","7":"f","8":"19","9":"27","10":"p","11":"midsize","_rn_":"146"},{"1":"nissan","2":"altima","3":"3.5","4":"2008","5":"6","6":"auto(av)","7":"f","8":"19","9":"26","10":"p","11":"midsize","_rn_":"147"},{"1":"nissan","2":"maxima","3":"3.0","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"19","9":"25","10":"r","11":"midsize","_rn_":"149"},{"1":"nissan","2":"maxima","3":"3.5","4":"2008","5":"6","6":"auto(av)","7":"f","8":"19","9":"25","10":"p","11":"midsize","_rn_":"150"},{"1":"toyota","2":"camry","3":"3.5","4":"2008","5":"6","6":"auto(s6)","7":"f","8":"19","9":"28","10":"r","11":"midsize","_rn_":"186"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

## Subsetting with "[]"

One alternative to the subset() function are the square brackets "[]". Within the brackets, there are two terms separated by a comma. Terms before the comma are conditional statements that will apply to rows, terms after the comma apply to columns. If you leave one term blank, no conditions are set and all rows/columns are returned.

### Single Condition on Rows

Let's say we want to find observations with city MPG \> 18. 

Let's find all observations where the city MPG \> 18


```r
mpg[mpg$cty > 18 , ]
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["manufacturer"],"name":[1],"type":["fct"],"align":["left"]},{"label":["model"],"name":[2],"type":["fct"],"align":["left"]},{"label":["displ"],"name":[3],"type":["dbl"],"align":["right"]},{"label":["year"],"name":[4],"type":["int"],"align":["right"]},{"label":["cyl"],"name":[5],"type":["int"],"align":["right"]},{"label":["trans"],"name":[6],"type":["chr"],"align":["left"]},{"label":["drv"],"name":[7],"type":["chr"],"align":["left"]},{"label":["cty"],"name":[8],"type":["int"],"align":["right"]},{"label":["hwy"],"name":[9],"type":["int"],"align":["right"]},{"label":["fl"],"name":[10],"type":["chr"],"align":["left"]},{"label":["class"],"name":[11],"type":["chr"],"align":["left"]}],"data":[{"1":"audi","2":"a4","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"p","11":"compact","_rn_":"2"},{"1":"audi","2":"a4","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"20","9":"31","10":"p","11":"compact","_rn_":"3"},{"1":"audi","2":"a4","3":"2.0","4":"2008","5":"4","6":"auto(av)","7":"f","8":"21","9":"30","10":"p","11":"compact","_rn_":"4"},{"1":"audi","2":"a4 quattro","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"4","8":"20","9":"28","10":"p","11":"compact","_rn_":"10"},{"1":"audi","2":"a4 quattro","3":"2.0","4":"2008","5":"4","6":"auto(s6)","7":"4","8":"19","9":"27","10":"p","11":"compact","_rn_":"11"},{"1":"chevrolet","2":"malibu","3":"2.4","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19","9":"27","10":"r","11":"midsize","_rn_":"33"},{"1":"chevrolet","2":"malibu","3":"2.4","4":"2008","5":"4","6":"auto(l4)","7":"f","8":"22","9":"30","10":"r","11":"midsize","_rn_":"34"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"28","9":"33","10":"r","11":"subcompact","_rn_":"100"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"24","9":"32","10":"r","11":"subcompact","_rn_":"101"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"25","9":"32","10":"r","11":"subcompact","_rn_":"102"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"23","9":"29","10":"p","11":"subcompact","_rn_":"103"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"24","9":"32","10":"r","11":"subcompact","_rn_":"104"},{"1":"honda","2":"civic","3":"1.8","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"26","9":"34","10":"r","11":"subcompact","_rn_":"105"},{"1":"honda","2":"civic","3":"1.8","4":"2008","5":"4","6":"auto(l5)","7":"f","8":"25","9":"36","10":"r","11":"subcompact","_rn_":"106"},{"1":"honda","2":"civic","3":"1.8","4":"2008","5":"4","6":"auto(l5)","7":"f","8":"24","9":"36","10":"c","11":"subcompact","_rn_":"107"},{"1":"honda","2":"civic","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"21","9":"29","10":"p","11":"subcompact","_rn_":"108"},{"1":"hyundai","2":"sonata","3":"2.4","4":"2008","5":"4","6":"auto(l4)","7":"f","8":"21","9":"30","10":"r","11":"midsize","_rn_":"111"},{"1":"hyundai","2":"sonata","3":"2.4","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"21","9":"31","10":"r","11":"midsize","_rn_":"112"},{"1":"hyundai","2":"sonata","3":"3.3","4":"2008","5":"6","6":"auto(l5)","7":"f","8":"19","9":"28","10":"r","11":"midsize","_rn_":"115"},{"1":"hyundai","2":"tiburon","3":"2.0","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19","9":"26","10":"r","11":"subcompact","_rn_":"116"},{"1":"hyundai","2":"tiburon","3":"2.0","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"19","9":"29","10":"r","11":"subcompact","_rn_":"117"},{"1":"hyundai","2":"tiburon","3":"2.0","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"20","9":"28","10":"r","11":"subcompact","_rn_":"118"},{"1":"hyundai","2":"tiburon","3":"2.0","4":"2008","5":"4","6":"auto(l4)","7":"f","8":"20","9":"27","10":"r","11":"subcompact","_rn_":"119"},{"1":"nissan","2":"altima","3":"2.4","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"142"},{"1":"nissan","2":"altima","3":"2.4","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19","9":"27","10":"r","11":"compact","_rn_":"143"},{"1":"nissan","2":"altima","3":"2.5","4":"2008","5":"4","6":"auto(av)","7":"f","8":"23","9":"31","10":"r","11":"midsize","_rn_":"144"},{"1":"nissan","2":"altima","3":"2.5","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"23","9":"32","10":"r","11":"midsize","_rn_":"145"},{"1":"nissan","2":"altima","3":"3.5","4":"2008","5":"6","6":"manual(m6)","7":"f","8":"19","9":"27","10":"p","11":"midsize","_rn_":"146"},{"1":"nissan","2":"altima","3":"3.5","4":"2008","5":"6","6":"auto(av)","7":"f","8":"19","9":"26","10":"p","11":"midsize","_rn_":"147"},{"1":"nissan","2":"maxima","3":"3.0","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"19","9":"25","10":"r","11":"midsize","_rn_":"149"},{"1":"nissan","2":"maxima","3":"3.5","4":"2008","5":"6","6":"auto(av)","7":"f","8":"19","9":"25","10":"p","11":"midsize","_rn_":"150"},{"1":"subaru","2":"forester awd","3":"2.5","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"20","9":"27","10":"r","11":"suv","_rn_":"162"},{"1":"subaru","2":"forester awd","3":"2.5","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"19","9":"25","10":"p","11":"suv","_rn_":"163"},{"1":"subaru","2":"forester awd","3":"2.5","4":"2008","5":"4","6":"auto(l4)","7":"4","8":"20","9":"26","10":"r","11":"suv","_rn_":"164"},{"1":"subaru","2":"impreza awd","3":"2.2","4":"1999","5":"4","6":"auto(l4)","7":"4","8":"21","9":"26","10":"r","11":"subcompact","_rn_":"166"},{"1":"subaru","2":"impreza awd","3":"2.2","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"19","9":"26","10":"r","11":"subcompact","_rn_":"167"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"19","9":"26","10":"r","11":"subcompact","_rn_":"168"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"1999","5":"4","6":"auto(l4)","7":"4","8":"19","9":"26","10":"r","11":"subcompact","_rn_":"169"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"2008","5":"4","6":"auto(s4)","7":"4","8":"20","9":"25","10":"p","11":"compact","_rn_":"170"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"2008","5":"4","6":"auto(s4)","7":"4","8":"20","9":"27","10":"r","11":"compact","_rn_":"171"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"19","9":"25","10":"p","11":"compact","_rn_":"172"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"20","9":"27","10":"r","11":"compact","_rn_":"173"},{"1":"toyota","2":"camry","3":"2.2","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"midsize","_rn_":"180"},{"1":"toyota","2":"camry","3":"2.2","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"21","9":"27","10":"r","11":"midsize","_rn_":"181"},{"1":"toyota","2":"camry","3":"2.4","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"21","9":"31","10":"r","11":"midsize","_rn_":"182"},{"1":"toyota","2":"camry","3":"2.4","4":"2008","5":"4","6":"auto(l5)","7":"f","8":"21","9":"31","10":"r","11":"midsize","_rn_":"183"},{"1":"toyota","2":"camry","3":"3.5","4":"2008","5":"6","6":"auto(s6)","7":"f","8":"19","9":"28","10":"r","11":"midsize","_rn_":"186"},{"1":"toyota","2":"camry solara","3":"2.2","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"21","9":"27","10":"r","11":"compact","_rn_":"187"},{"1":"toyota","2":"camry solara","3":"2.2","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"188"},{"1":"toyota","2":"camry solara","3":"2.4","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"21","9":"31","10":"r","11":"compact","_rn_":"189"},{"1":"toyota","2":"camry solara","3":"2.4","4":"2008","5":"4","6":"auto(s5)","7":"f","8":"22","9":"31","10":"r","11":"compact","_rn_":"190"},{"1":"toyota","2":"corolla","3":"1.8","4":"1999","5":"4","6":"auto(l3)","7":"f","8":"24","9":"30","10":"r","11":"compact","_rn_":"194"},{"1":"toyota","2":"corolla","3":"1.8","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"24","9":"33","10":"r","11":"compact","_rn_":"195"},{"1":"toyota","2":"corolla","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"26","9":"35","10":"r","11":"compact","_rn_":"196"},{"1":"toyota","2":"corolla","3":"1.8","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"28","9":"37","10":"r","11":"compact","_rn_":"197"},{"1":"toyota","2":"corolla","3":"1.8","4":"2008","5":"4","6":"auto(l4)","7":"f","8":"26","9":"35","10":"r","11":"compact","_rn_":"198"},{"1":"volkswagen","2":"gti","3":"2.0","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"208"},{"1":"volkswagen","2":"gti","3":"2.0","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19","9":"26","10":"r","11":"compact","_rn_":"209"},{"1":"volkswagen","2":"gti","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"21","9":"29","10":"p","11":"compact","_rn_":"210"},{"1":"volkswagen","2":"gti","3":"2.0","4":"2008","5":"4","6":"auto(s6)","7":"f","8":"22","9":"29","10":"p","11":"compact","_rn_":"211"},{"1":"volkswagen","2":"jetta","3":"1.9","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"33","9":"44","10":"d","11":"compact","_rn_":"213"},{"1":"volkswagen","2":"jetta","3":"2.0","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"214"},{"1":"volkswagen","2":"jetta","3":"2.0","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19","9":"26","10":"r","11":"compact","_rn_":"215"},{"1":"volkswagen","2":"jetta","3":"2.0","4":"2008","5":"4","6":"auto(s6)","7":"f","8":"22","9":"29","10":"p","11":"compact","_rn_":"216"},{"1":"volkswagen","2":"jetta","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"21","9":"29","10":"p","11":"compact","_rn_":"217"},{"1":"volkswagen","2":"jetta","3":"2.5","4":"2008","5":"5","6":"auto(s6)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"218"},{"1":"volkswagen","2":"jetta","3":"2.5","4":"2008","5":"5","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"219"},{"1":"volkswagen","2":"new beetle","3":"1.9","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"35","9":"44","10":"d","11":"subcompact","_rn_":"222"},{"1":"volkswagen","2":"new beetle","3":"1.9","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"29","9":"41","10":"d","11":"subcompact","_rn_":"223"},{"1":"volkswagen","2":"new beetle","3":"2.0","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"subcompact","_rn_":"224"},{"1":"volkswagen","2":"new beetle","3":"2.0","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19","9":"26","10":"r","11":"subcompact","_rn_":"225"},{"1":"volkswagen","2":"new beetle","3":"2.5","4":"2008","5":"5","6":"manual(m5)","7":"f","8":"20","9":"28","10":"r","11":"subcompact","_rn_":"226"},{"1":"volkswagen","2":"new beetle","3":"2.5","4":"2008","5":"5","6":"auto(s6)","7":"f","8":"20","9":"29","10":"r","11":"subcompact","_rn_":"227"},{"1":"volkswagen","2":"passat","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"p","11":"midsize","_rn_":"228"},{"1":"volkswagen","2":"passat","3":"2.0","4":"2008","5":"4","6":"auto(s6)","7":"f","8":"19","9":"28","10":"p","11":"midsize","_rn_":"230"},{"1":"volkswagen","2":"passat","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"21","9":"29","10":"p","11":"midsize","_rn_":"231"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

### Single Condition on Rows, Specific Columns

Let's again find observations with city MPG \> 18 but we only want to keep the Manufacturer, Model, and year variables


```r
mpg[mpg$cty > 18, c("year", "manufacturer", "model")]
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["year"],"name":[1],"type":["int"],"align":["right"]},{"label":["manufacturer"],"name":[2],"type":["fct"],"align":["left"]},{"label":["model"],"name":[3],"type":["fct"],"align":["left"]}],"data":[{"1":"1999","2":"audi","3":"a4","_rn_":"2"},{"1":"2008","2":"audi","3":"a4","_rn_":"3"},{"1":"2008","2":"audi","3":"a4","_rn_":"4"},{"1":"2008","2":"audi","3":"a4 quattro","_rn_":"10"},{"1":"2008","2":"audi","3":"a4 quattro","_rn_":"11"},{"1":"1999","2":"chevrolet","3":"malibu","_rn_":"33"},{"1":"2008","2":"chevrolet","3":"malibu","_rn_":"34"},{"1":"1999","2":"honda","3":"civic","_rn_":"100"},{"1":"1999","2":"honda","3":"civic","_rn_":"101"},{"1":"1999","2":"honda","3":"civic","_rn_":"102"},{"1":"1999","2":"honda","3":"civic","_rn_":"103"},{"1":"1999","2":"honda","3":"civic","_rn_":"104"},{"1":"2008","2":"honda","3":"civic","_rn_":"105"},{"1":"2008","2":"honda","3":"civic","_rn_":"106"},{"1":"2008","2":"honda","3":"civic","_rn_":"107"},{"1":"2008","2":"honda","3":"civic","_rn_":"108"},{"1":"2008","2":"hyundai","3":"sonata","_rn_":"111"},{"1":"2008","2":"hyundai","3":"sonata","_rn_":"112"},{"1":"2008","2":"hyundai","3":"sonata","_rn_":"115"},{"1":"1999","2":"hyundai","3":"tiburon","_rn_":"116"},{"1":"1999","2":"hyundai","3":"tiburon","_rn_":"117"},{"1":"2008","2":"hyundai","3":"tiburon","_rn_":"118"},{"1":"2008","2":"hyundai","3":"tiburon","_rn_":"119"},{"1":"1999","2":"nissan","3":"altima","_rn_":"142"},{"1":"1999","2":"nissan","3":"altima","_rn_":"143"},{"1":"2008","2":"nissan","3":"altima","_rn_":"144"},{"1":"2008","2":"nissan","3":"altima","_rn_":"145"},{"1":"2008","2":"nissan","3":"altima","_rn_":"146"},{"1":"2008","2":"nissan","3":"altima","_rn_":"147"},{"1":"1999","2":"nissan","3":"maxima","_rn_":"149"},{"1":"2008","2":"nissan","3":"maxima","_rn_":"150"},{"1":"2008","2":"subaru","3":"forester awd","_rn_":"162"},{"1":"2008","2":"subaru","3":"forester awd","_rn_":"163"},{"1":"2008","2":"subaru","3":"forester awd","_rn_":"164"},{"1":"1999","2":"subaru","3":"impreza awd","_rn_":"166"},{"1":"1999","2":"subaru","3":"impreza awd","_rn_":"167"},{"1":"1999","2":"subaru","3":"impreza awd","_rn_":"168"},{"1":"1999","2":"subaru","3":"impreza awd","_rn_":"169"},{"1":"2008","2":"subaru","3":"impreza awd","_rn_":"170"},{"1":"2008","2":"subaru","3":"impreza awd","_rn_":"171"},{"1":"2008","2":"subaru","3":"impreza awd","_rn_":"172"},{"1":"2008","2":"subaru","3":"impreza awd","_rn_":"173"},{"1":"1999","2":"toyota","3":"camry","_rn_":"180"},{"1":"1999","2":"toyota","3":"camry","_rn_":"181"},{"1":"2008","2":"toyota","3":"camry","_rn_":"182"},{"1":"2008","2":"toyota","3":"camry","_rn_":"183"},{"1":"2008","2":"toyota","3":"camry","_rn_":"186"},{"1":"1999","2":"toyota","3":"camry solara","_rn_":"187"},{"1":"1999","2":"toyota","3":"camry solara","_rn_":"188"},{"1":"2008","2":"toyota","3":"camry solara","_rn_":"189"},{"1":"2008","2":"toyota","3":"camry solara","_rn_":"190"},{"1":"1999","2":"toyota","3":"corolla","_rn_":"194"},{"1":"1999","2":"toyota","3":"corolla","_rn_":"195"},{"1":"1999","2":"toyota","3":"corolla","_rn_":"196"},{"1":"2008","2":"toyota","3":"corolla","_rn_":"197"},{"1":"2008","2":"toyota","3":"corolla","_rn_":"198"},{"1":"1999","2":"volkswagen","3":"gti","_rn_":"208"},{"1":"1999","2":"volkswagen","3":"gti","_rn_":"209"},{"1":"2008","2":"volkswagen","3":"gti","_rn_":"210"},{"1":"2008","2":"volkswagen","3":"gti","_rn_":"211"},{"1":"1999","2":"volkswagen","3":"jetta","_rn_":"213"},{"1":"1999","2":"volkswagen","3":"jetta","_rn_":"214"},{"1":"1999","2":"volkswagen","3":"jetta","_rn_":"215"},{"1":"2008","2":"volkswagen","3":"jetta","_rn_":"216"},{"1":"2008","2":"volkswagen","3":"jetta","_rn_":"217"},{"1":"2008","2":"volkswagen","3":"jetta","_rn_":"218"},{"1":"2008","2":"volkswagen","3":"jetta","_rn_":"219"},{"1":"1999","2":"volkswagen","3":"new beetle","_rn_":"222"},{"1":"1999","2":"volkswagen","3":"new beetle","_rn_":"223"},{"1":"1999","2":"volkswagen","3":"new beetle","_rn_":"224"},{"1":"1999","2":"volkswagen","3":"new beetle","_rn_":"225"},{"1":"2008","2":"volkswagen","3":"new beetle","_rn_":"226"},{"1":"2008","2":"volkswagen","3":"new beetle","_rn_":"227"},{"1":"1999","2":"volkswagen","3":"passat","_rn_":"228"},{"1":"2008","2":"volkswagen","3":"passat","_rn_":"230"},{"1":"2008","2":"volkswagen","3":"passat","_rn_":"231"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

Note that if the column that the rows are conditioned on contains an NA value, then R cautiously changes the entire row of the resulting dataset to NA because it is unable to decide whether that row meets the criteria. To remove these NA rows, you can use the which() function in the logical statement. In our case, with or without which() give the same output because we don't have missing values.


```r
mpg[which(mpg$cty > 18), c("year", "manufacturer", "model")]
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["year"],"name":[1],"type":["int"],"align":["right"]},{"label":["manufacturer"],"name":[2],"type":["fct"],"align":["left"]},{"label":["model"],"name":[3],"type":["fct"],"align":["left"]}],"data":[{"1":"1999","2":"audi","3":"a4","_rn_":"2"},{"1":"2008","2":"audi","3":"a4","_rn_":"3"},{"1":"2008","2":"audi","3":"a4","_rn_":"4"},{"1":"2008","2":"audi","3":"a4 quattro","_rn_":"10"},{"1":"2008","2":"audi","3":"a4 quattro","_rn_":"11"},{"1":"1999","2":"chevrolet","3":"malibu","_rn_":"33"},{"1":"2008","2":"chevrolet","3":"malibu","_rn_":"34"},{"1":"1999","2":"honda","3":"civic","_rn_":"100"},{"1":"1999","2":"honda","3":"civic","_rn_":"101"},{"1":"1999","2":"honda","3":"civic","_rn_":"102"},{"1":"1999","2":"honda","3":"civic","_rn_":"103"},{"1":"1999","2":"honda","3":"civic","_rn_":"104"},{"1":"2008","2":"honda","3":"civic","_rn_":"105"},{"1":"2008","2":"honda","3":"civic","_rn_":"106"},{"1":"2008","2":"honda","3":"civic","_rn_":"107"},{"1":"2008","2":"honda","3":"civic","_rn_":"108"},{"1":"2008","2":"hyundai","3":"sonata","_rn_":"111"},{"1":"2008","2":"hyundai","3":"sonata","_rn_":"112"},{"1":"2008","2":"hyundai","3":"sonata","_rn_":"115"},{"1":"1999","2":"hyundai","3":"tiburon","_rn_":"116"},{"1":"1999","2":"hyundai","3":"tiburon","_rn_":"117"},{"1":"2008","2":"hyundai","3":"tiburon","_rn_":"118"},{"1":"2008","2":"hyundai","3":"tiburon","_rn_":"119"},{"1":"1999","2":"nissan","3":"altima","_rn_":"142"},{"1":"1999","2":"nissan","3":"altima","_rn_":"143"},{"1":"2008","2":"nissan","3":"altima","_rn_":"144"},{"1":"2008","2":"nissan","3":"altima","_rn_":"145"},{"1":"2008","2":"nissan","3":"altima","_rn_":"146"},{"1":"2008","2":"nissan","3":"altima","_rn_":"147"},{"1":"1999","2":"nissan","3":"maxima","_rn_":"149"},{"1":"2008","2":"nissan","3":"maxima","_rn_":"150"},{"1":"2008","2":"subaru","3":"forester awd","_rn_":"162"},{"1":"2008","2":"subaru","3":"forester awd","_rn_":"163"},{"1":"2008","2":"subaru","3":"forester awd","_rn_":"164"},{"1":"1999","2":"subaru","3":"impreza awd","_rn_":"166"},{"1":"1999","2":"subaru","3":"impreza awd","_rn_":"167"},{"1":"1999","2":"subaru","3":"impreza awd","_rn_":"168"},{"1":"1999","2":"subaru","3":"impreza awd","_rn_":"169"},{"1":"2008","2":"subaru","3":"impreza awd","_rn_":"170"},{"1":"2008","2":"subaru","3":"impreza awd","_rn_":"171"},{"1":"2008","2":"subaru","3":"impreza awd","_rn_":"172"},{"1":"2008","2":"subaru","3":"impreza awd","_rn_":"173"},{"1":"1999","2":"toyota","3":"camry","_rn_":"180"},{"1":"1999","2":"toyota","3":"camry","_rn_":"181"},{"1":"2008","2":"toyota","3":"camry","_rn_":"182"},{"1":"2008","2":"toyota","3":"camry","_rn_":"183"},{"1":"2008","2":"toyota","3":"camry","_rn_":"186"},{"1":"1999","2":"toyota","3":"camry solara","_rn_":"187"},{"1":"1999","2":"toyota","3":"camry solara","_rn_":"188"},{"1":"2008","2":"toyota","3":"camry solara","_rn_":"189"},{"1":"2008","2":"toyota","3":"camry solara","_rn_":"190"},{"1":"1999","2":"toyota","3":"corolla","_rn_":"194"},{"1":"1999","2":"toyota","3":"corolla","_rn_":"195"},{"1":"1999","2":"toyota","3":"corolla","_rn_":"196"},{"1":"2008","2":"toyota","3":"corolla","_rn_":"197"},{"1":"2008","2":"toyota","3":"corolla","_rn_":"198"},{"1":"1999","2":"volkswagen","3":"gti","_rn_":"208"},{"1":"1999","2":"volkswagen","3":"gti","_rn_":"209"},{"1":"2008","2":"volkswagen","3":"gti","_rn_":"210"},{"1":"2008","2":"volkswagen","3":"gti","_rn_":"211"},{"1":"1999","2":"volkswagen","3":"jetta","_rn_":"213"},{"1":"1999","2":"volkswagen","3":"jetta","_rn_":"214"},{"1":"1999","2":"volkswagen","3":"jetta","_rn_":"215"},{"1":"2008","2":"volkswagen","3":"jetta","_rn_":"216"},{"1":"2008","2":"volkswagen","3":"jetta","_rn_":"217"},{"1":"2008","2":"volkswagen","3":"jetta","_rn_":"218"},{"1":"2008","2":"volkswagen","3":"jetta","_rn_":"219"},{"1":"1999","2":"volkswagen","3":"new beetle","_rn_":"222"},{"1":"1999","2":"volkswagen","3":"new beetle","_rn_":"223"},{"1":"1999","2":"volkswagen","3":"new beetle","_rn_":"224"},{"1":"1999","2":"volkswagen","3":"new beetle","_rn_":"225"},{"1":"2008","2":"volkswagen","3":"new beetle","_rn_":"226"},{"1":"2008","2":"volkswagen","3":"new beetle","_rn_":"227"},{"1":"1999","2":"volkswagen","3":"passat","_rn_":"228"},{"1":"2008","2":"volkswagen","3":"passat","_rn_":"230"},{"1":"2008","2":"volkswagen","3":"passat","_rn_":"231"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

### Multiple Conditions on Rows

City MPG \> 18 AND manufacturer is Toyota, return all columns


```r
mpg[mpg$cty > 18 & mpg$manufacturer == "honda" ,]
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["manufacturer"],"name":[1],"type":["fct"],"align":["left"]},{"label":["model"],"name":[2],"type":["fct"],"align":["left"]},{"label":["displ"],"name":[3],"type":["dbl"],"align":["right"]},{"label":["year"],"name":[4],"type":["int"],"align":["right"]},{"label":["cyl"],"name":[5],"type":["int"],"align":["right"]},{"label":["trans"],"name":[6],"type":["chr"],"align":["left"]},{"label":["drv"],"name":[7],"type":["chr"],"align":["left"]},{"label":["cty"],"name":[8],"type":["int"],"align":["right"]},{"label":["hwy"],"name":[9],"type":["int"],"align":["right"]},{"label":["fl"],"name":[10],"type":["chr"],"align":["left"]},{"label":["class"],"name":[11],"type":["chr"],"align":["left"]}],"data":[{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"28","9":"33","10":"r","11":"subcompact","_rn_":"100"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"24","9":"32","10":"r","11":"subcompact","_rn_":"101"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"25","9":"32","10":"r","11":"subcompact","_rn_":"102"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"23","9":"29","10":"p","11":"subcompact","_rn_":"103"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"24","9":"32","10":"r","11":"subcompact","_rn_":"104"},{"1":"honda","2":"civic","3":"1.8","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"26","9":"34","10":"r","11":"subcompact","_rn_":"105"},{"1":"honda","2":"civic","3":"1.8","4":"2008","5":"4","6":"auto(l5)","7":"f","8":"25","9":"36","10":"r","11":"subcompact","_rn_":"106"},{"1":"honda","2":"civic","3":"1.8","4":"2008","5":"4","6":"auto(l5)","7":"f","8":"24","9":"36","10":"c","11":"subcompact","_rn_":"107"},{"1":"honda","2":"civic","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"21","9":"29","10":"p","11":"subcompact","_rn_":"108"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

City MPG \> 18 AND manufacturer is Honda OR Toyota, return all columns


```r
mpg[mpg$cty > 18 & (mpg$manufacturer == "honda" | mpg$manufacturer == "toyota") ,]
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["manufacturer"],"name":[1],"type":["fct"],"align":["left"]},{"label":["model"],"name":[2],"type":["fct"],"align":["left"]},{"label":["displ"],"name":[3],"type":["dbl"],"align":["right"]},{"label":["year"],"name":[4],"type":["int"],"align":["right"]},{"label":["cyl"],"name":[5],"type":["int"],"align":["right"]},{"label":["trans"],"name":[6],"type":["chr"],"align":["left"]},{"label":["drv"],"name":[7],"type":["chr"],"align":["left"]},{"label":["cty"],"name":[8],"type":["int"],"align":["right"]},{"label":["hwy"],"name":[9],"type":["int"],"align":["right"]},{"label":["fl"],"name":[10],"type":["chr"],"align":["left"]},{"label":["class"],"name":[11],"type":["chr"],"align":["left"]}],"data":[{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"28","9":"33","10":"r","11":"subcompact","_rn_":"100"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"24","9":"32","10":"r","11":"subcompact","_rn_":"101"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"25","9":"32","10":"r","11":"subcompact","_rn_":"102"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"23","9":"29","10":"p","11":"subcompact","_rn_":"103"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"24","9":"32","10":"r","11":"subcompact","_rn_":"104"},{"1":"honda","2":"civic","3":"1.8","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"26","9":"34","10":"r","11":"subcompact","_rn_":"105"},{"1":"honda","2":"civic","3":"1.8","4":"2008","5":"4","6":"auto(l5)","7":"f","8":"25","9":"36","10":"r","11":"subcompact","_rn_":"106"},{"1":"honda","2":"civic","3":"1.8","4":"2008","5":"4","6":"auto(l5)","7":"f","8":"24","9":"36","10":"c","11":"subcompact","_rn_":"107"},{"1":"honda","2":"civic","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"21","9":"29","10":"p","11":"subcompact","_rn_":"108"},{"1":"toyota","2":"camry","3":"2.2","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"midsize","_rn_":"180"},{"1":"toyota","2":"camry","3":"2.2","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"21","9":"27","10":"r","11":"midsize","_rn_":"181"},{"1":"toyota","2":"camry","3":"2.4","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"21","9":"31","10":"r","11":"midsize","_rn_":"182"},{"1":"toyota","2":"camry","3":"2.4","4":"2008","5":"4","6":"auto(l5)","7":"f","8":"21","9":"31","10":"r","11":"midsize","_rn_":"183"},{"1":"toyota","2":"camry","3":"3.5","4":"2008","5":"6","6":"auto(s6)","7":"f","8":"19","9":"28","10":"r","11":"midsize","_rn_":"186"},{"1":"toyota","2":"camry solara","3":"2.2","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"21","9":"27","10":"r","11":"compact","_rn_":"187"},{"1":"toyota","2":"camry solara","3":"2.2","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"188"},{"1":"toyota","2":"camry solara","3":"2.4","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"21","9":"31","10":"r","11":"compact","_rn_":"189"},{"1":"toyota","2":"camry solara","3":"2.4","4":"2008","5":"4","6":"auto(s5)","7":"f","8":"22","9":"31","10":"r","11":"compact","_rn_":"190"},{"1":"toyota","2":"corolla","3":"1.8","4":"1999","5":"4","6":"auto(l3)","7":"f","8":"24","9":"30","10":"r","11":"compact","_rn_":"194"},{"1":"toyota","2":"corolla","3":"1.8","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"24","9":"33","10":"r","11":"compact","_rn_":"195"},{"1":"toyota","2":"corolla","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"26","9":"35","10":"r","11":"compact","_rn_":"196"},{"1":"toyota","2":"corolla","3":"1.8","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"28","9":"37","10":"r","11":"compact","_rn_":"197"},{"1":"toyota","2":"corolla","3":"1.8","4":"2008","5":"4","6":"auto(l4)","7":"f","8":"26","9":"35","10":"r","11":"compact","_rn_":"198"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

## Subsetting with a Vector

So far, when subsetting character values, we've been interested in just a single value at a time. But sometimes, we have a vector of values that we want to subset on. For example, this is very useful if your data consists of patients with unique IDs and you want to make a subset of data that contains observations corresponding to a list of specific patients.

The tool for this is the %in% operator, which finds values of one vector in another.For example, let's say we're interested in finding a subset of cars with transmissions with 6 gears. 

First, we define a vector of the transmissions we want. We can do this using grep and searching for any strings with the number 6.

```r
six.speed <- unique(grep('6', 
                         mpg$trans, 
                         value = T)
              )
```



Now do that actual subsetting


```r
mpg[mpg$trans %in% six.speed , ]
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["manufacturer"],"name":[1],"type":["fct"],"align":["left"]},{"label":["model"],"name":[2],"type":["fct"],"align":["left"]},{"label":["displ"],"name":[3],"type":["dbl"],"align":["right"]},{"label":["year"],"name":[4],"type":["int"],"align":["right"]},{"label":["cyl"],"name":[5],"type":["int"],"align":["right"]},{"label":["trans"],"name":[6],"type":["chr"],"align":["left"]},{"label":["drv"],"name":[7],"type":["chr"],"align":["left"]},{"label":["cty"],"name":[8],"type":["int"],"align":["right"]},{"label":["hwy"],"name":[9],"type":["int"],"align":["right"]},{"label":["fl"],"name":[10],"type":["chr"],"align":["left"]},{"label":["class"],"name":[11],"type":["chr"],"align":["left"]}],"data":[{"1":"audi","2":"a4","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"20","9":"31","10":"p","11":"compact","_rn_":"3"},{"1":"audi","2":"a4 quattro","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"4","8":"20","9":"28","10":"p","11":"compact","_rn_":"10"},{"1":"audi","2":"a4 quattro","3":"2.0","4":"2008","5":"4","6":"auto(s6)","7":"4","8":"19","9":"27","10":"p","11":"compact","_rn_":"11"},{"1":"audi","2":"a4 quattro","3":"3.1","4":"2008","5":"6","6":"auto(s6)","7":"4","8":"17","9":"25","10":"p","11":"compact","_rn_":"14"},{"1":"audi","2":"a4 quattro","3":"3.1","4":"2008","5":"6","6":"manual(m6)","7":"4","8":"15","9":"25","10":"p","11":"compact","_rn_":"15"},{"1":"audi","2":"a6 quattro","3":"3.1","4":"2008","5":"6","6":"auto(s6)","7":"4","8":"17","9":"25","10":"p","11":"midsize","_rn_":"17"},{"1":"audi","2":"a6 quattro","3":"4.2","4":"2008","5":"8","6":"auto(s6)","7":"4","8":"16","9":"23","10":"p","11":"midsize","_rn_":"18"},{"1":"chevrolet","2":"corvette","3":"5.7","4":"1999","5":"8","6":"manual(m6)","7":"r","8":"16","9":"26","10":"p","11":"2seater","_rn_":"24"},{"1":"chevrolet","2":"corvette","3":"6.2","4":"2008","5":"8","6":"manual(m6)","7":"r","8":"16","9":"26","10":"p","11":"2seater","_rn_":"26"},{"1":"chevrolet","2":"corvette","3":"6.2","4":"2008","5":"8","6":"auto(s6)","7":"r","8":"15","9":"25","10":"p","11":"2seater","_rn_":"27"},{"1":"chevrolet","2":"corvette","3":"7.0","4":"2008","5":"8","6":"manual(m6)","7":"r","8":"15","9":"24","10":"p","11":"2seater","_rn_":"28"},{"1":"chevrolet","2":"malibu","3":"3.6","4":"2008","5":"6","6":"auto(s6)","7":"f","8":"17","9":"26","10":"r","11":"midsize","_rn_":"37"},{"1":"dodge","2":"caravan 2wd","3":"3.8","4":"2008","5":"6","6":"auto(l6)","7":"f","8":"16","9":"23","10":"r","11":"minivan","_rn_":"47"},{"1":"dodge","2":"caravan 2wd","3":"4.0","4":"2008","5":"6","6":"auto(l6)","7":"f","8":"16","9":"23","10":"r","11":"minivan","_rn_":"48"},{"1":"dodge","2":"dakota pickup 4wd","3":"3.7","4":"2008","5":"6","6":"manual(m6)","7":"4","8":"15","9":"19","10":"r","11":"pickup","_rn_":"49"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"manual(m6)","7":"4","8":"12","9":"16","10":"r","11":"pickup","_rn_":"65"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"manual(m6)","7":"4","8":"12","9":"16","10":"r","11":"pickup","_rn_":"69"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"manual(m6)","7":"4","8":"9","9":"12","10":"e","11":"pickup","_rn_":"70"},{"1":"ford","2":"expedition 2wd","3":"5.4","4":"2008","5":"8","6":"auto(l6)","7":"r","8":"12","9":"18","10":"r","11":"suv","_rn_":"77"},{"1":"ford","2":"explorer 4wd","3":"4.6","4":"2008","5":"8","6":"auto(l6)","7":"4","8":"13","9":"19","10":"r","11":"suv","_rn_":"82"},{"1":"ford","2":"mustang","3":"5.4","4":"2008","5":"8","6":"manual(m6)","7":"r","8":"14","9":"20","10":"p","11":"subcompact","_rn_":"99"},{"1":"honda","2":"civic","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"21","9":"29","10":"p","11":"subcompact","_rn_":"108"},{"1":"hyundai","2":"tiburon","3":"2.7","4":"2008","5":"6","6":"manual(m6)","7":"f","8":"16","9":"24","10":"r","11":"subcompact","_rn_":"121"},{"1":"land rover","2":"range rover","3":"4.2","4":"2008","5":"8","6":"auto(s6)","7":"4","8":"12","9":"18","10":"r","11":"suv","_rn_":"132"},{"1":"land rover","2":"range rover","3":"4.4","4":"2008","5":"8","6":"auto(s6)","7":"4","8":"12","9":"18","10":"r","11":"suv","_rn_":"133"},{"1":"lincoln","2":"navigator 2wd","3":"5.4","4":"2008","5":"8","6":"auto(l6)","7":"r","8":"12","9":"18","10":"r","11":"suv","_rn_":"137"},{"1":"mercury","2":"mountaineer 4wd","3":"4.6","4":"2008","5":"8","6":"auto(l6)","7":"4","8":"13","9":"19","10":"r","11":"suv","_rn_":"140"},{"1":"nissan","2":"altima","3":"2.5","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"23","9":"32","10":"r","11":"midsize","_rn_":"145"},{"1":"nissan","2":"altima","3":"3.5","4":"2008","5":"6","6":"manual(m6)","7":"f","8":"19","9":"27","10":"p","11":"midsize","_rn_":"146"},{"1":"toyota","2":"camry","3":"3.5","4":"2008","5":"6","6":"auto(s6)","7":"f","8":"19","9":"28","10":"r","11":"midsize","_rn_":"186"},{"1":"toyota","2":"land cruiser wagon 4wd","3":"5.7","4":"2008","5":"8","6":"auto(s6)","7":"4","8":"13","9":"18","10":"r","11":"suv","_rn_":"200"},{"1":"toyota","2":"toyota tacoma 4wd","3":"4.0","4":"2008","5":"6","6":"manual(m6)","7":"4","8":"15","9":"18","10":"r","11":"pickup","_rn_":"206"},{"1":"volkswagen","2":"gti","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"21","9":"29","10":"p","11":"compact","_rn_":"210"},{"1":"volkswagen","2":"gti","3":"2.0","4":"2008","5":"4","6":"auto(s6)","7":"f","8":"22","9":"29","10":"p","11":"compact","_rn_":"211"},{"1":"volkswagen","2":"jetta","3":"2.0","4":"2008","5":"4","6":"auto(s6)","7":"f","8":"22","9":"29","10":"p","11":"compact","_rn_":"216"},{"1":"volkswagen","2":"jetta","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"21","9":"29","10":"p","11":"compact","_rn_":"217"},{"1":"volkswagen","2":"jetta","3":"2.5","4":"2008","5":"5","6":"auto(s6)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"218"},{"1":"volkswagen","2":"new beetle","3":"2.5","4":"2008","5":"5","6":"auto(s6)","7":"f","8":"20","9":"29","10":"r","11":"subcompact","_rn_":"227"},{"1":"volkswagen","2":"passat","3":"2.0","4":"2008","5":"4","6":"auto(s6)","7":"f","8":"19","9":"28","10":"p","11":"midsize","_rn_":"230"},{"1":"volkswagen","2":"passat","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"21","9":"29","10":"p","11":"midsize","_rn_":"231"},{"1":"volkswagen","2":"passat","3":"3.6","4":"2008","5":"6","6":"auto(s6)","7":"f","8":"17","9":"26","10":"p","11":"midsize","_rn_":"234"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

## Removing NAs

### With is.na()

There is a special function in R called is.na() to identify missing values that can be used to efficiently subset.

Since there are no NA values in the mpg dataset, we'll have to create our own. Let's make a subset of the mpg dataset and add a new row with NA for city MPG. Note our use of rbind() here which combines dataframes and/or vectors row-wise.


```r
dat.na <- as.data.frame(rbind(
  mpg[1:3, c("manufacturer", "model", "year", "cty")],
  c("toyota", "camry", 2021, NA)
))
```

Now we can subset to only include values with NA for city MPG


```r
dat.na[is.na(dat.na$cty) , ]
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["manufacturer"],"name":[1],"type":["fct"],"align":["left"]},{"label":["model"],"name":[2],"type":["fct"],"align":["left"]},{"label":["year"],"name":[3],"type":["chr"],"align":["left"]},{"label":["cty"],"name":[4],"type":["chr"],"align":["left"]}],"data":[{"1":"toyota","2":"camry","3":"2021","4":"NA","_rn_":"4"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

To remove any values with NA, we can use the "not" operator (!)


```r
dat.na[!is.na(dat.na$cty) , ]
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["manufacturer"],"name":[1],"type":["fct"],"align":["left"]},{"label":["model"],"name":[2],"type":["fct"],"align":["left"]},{"label":["year"],"name":[3],"type":["chr"],"align":["left"]},{"label":["cty"],"name":[4],"type":["chr"],"align":["left"]}],"data":[{"1":"audi","2":"a4","3":"1999","4":"18","_rn_":"1"},{"1":"audi","2":"a4","3":"1999","4":"21","_rn_":"2"},{"1":"audi","2":"a4","3":"2008","4":"20","_rn_":"3"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

### With complete.cases()

The complete.cases function can be used to filter out any rows that have an NA in any column, returning a NA-free dataframe


```r
dat.na[which(complete.cases(dat.na)) ,]
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["manufacturer"],"name":[1],"type":["fct"],"align":["left"]},{"label":["model"],"name":[2],"type":["fct"],"align":["left"]},{"label":["year"],"name":[3],"type":["chr"],"align":["left"]},{"label":["cty"],"name":[4],"type":["chr"],"align":["left"]}],"data":[{"1":"audi","2":"a4","3":"1999","4":"18","_rn_":"1"},{"1":"audi","2":"a4","3":"1999","4":"21","_rn_":"2"},{"1":"audi","2":"a4","3":"2008","4":"20","_rn_":"3"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

# Ordering and Sorting

## Sorting a Vector

The sort() function can sort a vector, returning sorted values


```r
head(sort(mpg$cty, decreasing = T))
```

```
## [1] 35 33 29 28 28 26
```

The order function sorts but returns indices of the sorted values


```r
head(order(mpg$cty, decreasing = T))
```

```
## [1] 222 213 223 100 197 105
```

## Sorting a Dataframe

This can be used to sort the rows of a dataframe by one or more variables. Use the "-" to indicate descending order for that variable.


```r
#Sort by city MPG
mpg[order(-mpg$cty) ,]
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["manufacturer"],"name":[1],"type":["fct"],"align":["left"]},{"label":["model"],"name":[2],"type":["fct"],"align":["left"]},{"label":["displ"],"name":[3],"type":["dbl"],"align":["right"]},{"label":["year"],"name":[4],"type":["int"],"align":["right"]},{"label":["cyl"],"name":[5],"type":["int"],"align":["right"]},{"label":["trans"],"name":[6],"type":["chr"],"align":["left"]},{"label":["drv"],"name":[7],"type":["chr"],"align":["left"]},{"label":["cty"],"name":[8],"type":["int"],"align":["right"]},{"label":["hwy"],"name":[9],"type":["int"],"align":["right"]},{"label":["fl"],"name":[10],"type":["chr"],"align":["left"]},{"label":["class"],"name":[11],"type":["chr"],"align":["left"]}],"data":[{"1":"volkswagen","2":"new beetle","3":"1.9","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"35","9":"44","10":"d","11":"subcompact","_rn_":"222"},{"1":"volkswagen","2":"jetta","3":"1.9","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"33","9":"44","10":"d","11":"compact","_rn_":"213"},{"1":"volkswagen","2":"new beetle","3":"1.9","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"29","9":"41","10":"d","11":"subcompact","_rn_":"223"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"28","9":"33","10":"r","11":"subcompact","_rn_":"100"},{"1":"toyota","2":"corolla","3":"1.8","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"28","9":"37","10":"r","11":"compact","_rn_":"197"},{"1":"honda","2":"civic","3":"1.8","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"26","9":"34","10":"r","11":"subcompact","_rn_":"105"},{"1":"toyota","2":"corolla","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"26","9":"35","10":"r","11":"compact","_rn_":"196"},{"1":"toyota","2":"corolla","3":"1.8","4":"2008","5":"4","6":"auto(l4)","7":"f","8":"26","9":"35","10":"r","11":"compact","_rn_":"198"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"25","9":"32","10":"r","11":"subcompact","_rn_":"102"},{"1":"honda","2":"civic","3":"1.8","4":"2008","5":"4","6":"auto(l5)","7":"f","8":"25","9":"36","10":"r","11":"subcompact","_rn_":"106"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"24","9":"32","10":"r","11":"subcompact","_rn_":"101"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"24","9":"32","10":"r","11":"subcompact","_rn_":"104"},{"1":"honda","2":"civic","3":"1.8","4":"2008","5":"4","6":"auto(l5)","7":"f","8":"24","9":"36","10":"c","11":"subcompact","_rn_":"107"},{"1":"toyota","2":"corolla","3":"1.8","4":"1999","5":"4","6":"auto(l3)","7":"f","8":"24","9":"30","10":"r","11":"compact","_rn_":"194"},{"1":"toyota","2":"corolla","3":"1.8","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"24","9":"33","10":"r","11":"compact","_rn_":"195"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"23","9":"29","10":"p","11":"subcompact","_rn_":"103"},{"1":"nissan","2":"altima","3":"2.5","4":"2008","5":"4","6":"auto(av)","7":"f","8":"23","9":"31","10":"r","11":"midsize","_rn_":"144"},{"1":"nissan","2":"altima","3":"2.5","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"23","9":"32","10":"r","11":"midsize","_rn_":"145"},{"1":"chevrolet","2":"malibu","3":"2.4","4":"2008","5":"4","6":"auto(l4)","7":"f","8":"22","9":"30","10":"r","11":"midsize","_rn_":"34"},{"1":"toyota","2":"camry solara","3":"2.4","4":"2008","5":"4","6":"auto(s5)","7":"f","8":"22","9":"31","10":"r","11":"compact","_rn_":"190"},{"1":"volkswagen","2":"gti","3":"2.0","4":"2008","5":"4","6":"auto(s6)","7":"f","8":"22","9":"29","10":"p","11":"compact","_rn_":"211"},{"1":"volkswagen","2":"jetta","3":"2.0","4":"2008","5":"4","6":"auto(s6)","7":"f","8":"22","9":"29","10":"p","11":"compact","_rn_":"216"},{"1":"audi","2":"a4","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"p","11":"compact","_rn_":"2"},{"1":"audi","2":"a4","3":"2.0","4":"2008","5":"4","6":"auto(av)","7":"f","8":"21","9":"30","10":"p","11":"compact","_rn_":"4"},{"1":"honda","2":"civic","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"21","9":"29","10":"p","11":"subcompact","_rn_":"108"},{"1":"hyundai","2":"sonata","3":"2.4","4":"2008","5":"4","6":"auto(l4)","7":"f","8":"21","9":"30","10":"r","11":"midsize","_rn_":"111"},{"1":"hyundai","2":"sonata","3":"2.4","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"21","9":"31","10":"r","11":"midsize","_rn_":"112"},{"1":"nissan","2":"altima","3":"2.4","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"142"},{"1":"subaru","2":"impreza awd","3":"2.2","4":"1999","5":"4","6":"auto(l4)","7":"4","8":"21","9":"26","10":"r","11":"subcompact","_rn_":"166"},{"1":"toyota","2":"camry","3":"2.2","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"midsize","_rn_":"180"},{"1":"toyota","2":"camry","3":"2.2","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"21","9":"27","10":"r","11":"midsize","_rn_":"181"},{"1":"toyota","2":"camry","3":"2.4","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"21","9":"31","10":"r","11":"midsize","_rn_":"182"},{"1":"toyota","2":"camry","3":"2.4","4":"2008","5":"4","6":"auto(l5)","7":"f","8":"21","9":"31","10":"r","11":"midsize","_rn_":"183"},{"1":"toyota","2":"camry solara","3":"2.2","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"21","9":"27","10":"r","11":"compact","_rn_":"187"},{"1":"toyota","2":"camry solara","3":"2.2","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"188"},{"1":"toyota","2":"camry solara","3":"2.4","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"21","9":"31","10":"r","11":"compact","_rn_":"189"},{"1":"volkswagen","2":"gti","3":"2.0","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"208"},{"1":"volkswagen","2":"gti","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"21","9":"29","10":"p","11":"compact","_rn_":"210"},{"1":"volkswagen","2":"jetta","3":"2.0","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"214"},{"1":"volkswagen","2":"jetta","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"21","9":"29","10":"p","11":"compact","_rn_":"217"},{"1":"volkswagen","2":"jetta","3":"2.5","4":"2008","5":"5","6":"auto(s6)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"218"},{"1":"volkswagen","2":"jetta","3":"2.5","4":"2008","5":"5","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"219"},{"1":"volkswagen","2":"new beetle","3":"2.0","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"subcompact","_rn_":"224"},{"1":"volkswagen","2":"passat","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"p","11":"midsize","_rn_":"228"},{"1":"volkswagen","2":"passat","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"21","9":"29","10":"p","11":"midsize","_rn_":"231"},{"1":"audi","2":"a4","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"20","9":"31","10":"p","11":"compact","_rn_":"3"},{"1":"audi","2":"a4 quattro","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"4","8":"20","9":"28","10":"p","11":"compact","_rn_":"10"},{"1":"hyundai","2":"tiburon","3":"2.0","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"20","9":"28","10":"r","11":"subcompact","_rn_":"118"},{"1":"hyundai","2":"tiburon","3":"2.0","4":"2008","5":"4","6":"auto(l4)","7":"f","8":"20","9":"27","10":"r","11":"subcompact","_rn_":"119"},{"1":"subaru","2":"forester awd","3":"2.5","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"20","9":"27","10":"r","11":"suv","_rn_":"162"},{"1":"subaru","2":"forester awd","3":"2.5","4":"2008","5":"4","6":"auto(l4)","7":"4","8":"20","9":"26","10":"r","11":"suv","_rn_":"164"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"2008","5":"4","6":"auto(s4)","7":"4","8":"20","9":"25","10":"p","11":"compact","_rn_":"170"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"2008","5":"4","6":"auto(s4)","7":"4","8":"20","9":"27","10":"r","11":"compact","_rn_":"171"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"20","9":"27","10":"r","11":"compact","_rn_":"173"},{"1":"volkswagen","2":"new beetle","3":"2.5","4":"2008","5":"5","6":"manual(m5)","7":"f","8":"20","9":"28","10":"r","11":"subcompact","_rn_":"226"},{"1":"volkswagen","2":"new beetle","3":"2.5","4":"2008","5":"5","6":"auto(s6)","7":"f","8":"20","9":"29","10":"r","11":"subcompact","_rn_":"227"},{"1":"audi","2":"a4 quattro","3":"2.0","4":"2008","5":"4","6":"auto(s6)","7":"4","8":"19","9":"27","10":"p","11":"compact","_rn_":"11"},{"1":"chevrolet","2":"malibu","3":"2.4","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19","9":"27","10":"r","11":"midsize","_rn_":"33"},{"1":"hyundai","2":"sonata","3":"3.3","4":"2008","5":"6","6":"auto(l5)","7":"f","8":"19","9":"28","10":"r","11":"midsize","_rn_":"115"},{"1":"hyundai","2":"tiburon","3":"2.0","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19","9":"26","10":"r","11":"subcompact","_rn_":"116"},{"1":"hyundai","2":"tiburon","3":"2.0","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"19","9":"29","10":"r","11":"subcompact","_rn_":"117"},{"1":"nissan","2":"altima","3":"2.4","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19","9":"27","10":"r","11":"compact","_rn_":"143"},{"1":"nissan","2":"altima","3":"3.5","4":"2008","5":"6","6":"manual(m6)","7":"f","8":"19","9":"27","10":"p","11":"midsize","_rn_":"146"},{"1":"nissan","2":"altima","3":"3.5","4":"2008","5":"6","6":"auto(av)","7":"f","8":"19","9":"26","10":"p","11":"midsize","_rn_":"147"},{"1":"nissan","2":"maxima","3":"3.0","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"19","9":"25","10":"r","11":"midsize","_rn_":"149"},{"1":"nissan","2":"maxima","3":"3.5","4":"2008","5":"6","6":"auto(av)","7":"f","8":"19","9":"25","10":"p","11":"midsize","_rn_":"150"},{"1":"subaru","2":"forester awd","3":"2.5","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"19","9":"25","10":"p","11":"suv","_rn_":"163"},{"1":"subaru","2":"impreza awd","3":"2.2","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"19","9":"26","10":"r","11":"subcompact","_rn_":"167"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"19","9":"26","10":"r","11":"subcompact","_rn_":"168"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"1999","5":"4","6":"auto(l4)","7":"4","8":"19","9":"26","10":"r","11":"subcompact","_rn_":"169"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"19","9":"25","10":"p","11":"compact","_rn_":"172"},{"1":"toyota","2":"camry","3":"3.5","4":"2008","5":"6","6":"auto(s6)","7":"f","8":"19","9":"28","10":"r","11":"midsize","_rn_":"186"},{"1":"volkswagen","2":"gti","3":"2.0","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19","9":"26","10":"r","11":"compact","_rn_":"209"},{"1":"volkswagen","2":"jetta","3":"2.0","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19","9":"26","10":"r","11":"compact","_rn_":"215"},{"1":"volkswagen","2":"new beetle","3":"2.0","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19","9":"26","10":"r","11":"subcompact","_rn_":"225"},{"1":"volkswagen","2":"passat","3":"2.0","4":"2008","5":"4","6":"auto(s6)","7":"f","8":"19","9":"28","10":"p","11":"midsize","_rn_":"230"},{"1":"audi","2":"a4","3":"1.8","4":"1999","5":"4","6":"auto(l5)","7":"f","8":"18","9":"29","10":"p","11":"compact","_rn_":"1"},{"1":"audi","2":"a4","3":"2.8","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"18","9":"26","10":"p","11":"compact","_rn_":"6"},{"1":"audi","2":"a4","3":"3.1","4":"2008","5":"6","6":"auto(av)","7":"f","8":"18","9":"27","10":"p","11":"compact","_rn_":"7"},{"1":"audi","2":"a4 quattro","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"18","9":"26","10":"p","11":"compact","_rn_":"8"},{"1":"chevrolet","2":"malibu","3":"3.1","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"18","9":"26","10":"r","11":"midsize","_rn_":"35"},{"1":"chevrolet","2":"malibu","3":"3.5","4":"2008","5":"6","6":"auto(l4)","7":"f","8":"18","9":"29","10":"r","11":"midsize","_rn_":"36"},{"1":"dodge","2":"caravan 2wd","3":"2.4","4":"1999","5":"4","6":"auto(l3)","7":"f","8":"18","9":"24","10":"r","11":"minivan","_rn_":"38"},{"1":"ford","2":"mustang","3":"3.8","4":"1999","5":"6","6":"manual(m5)","7":"r","8":"18","9":"26","10":"r","11":"subcompact","_rn_":"91"},{"1":"ford","2":"mustang","3":"3.8","4":"1999","5":"6","6":"auto(l4)","7":"r","8":"18","9":"25","10":"r","11":"subcompact","_rn_":"92"},{"1":"hyundai","2":"sonata","3":"2.4","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"18","9":"26","10":"r","11":"midsize","_rn_":"109"},{"1":"hyundai","2":"sonata","3":"2.4","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"18","9":"27","10":"r","11":"midsize","_rn_":"110"},{"1":"hyundai","2":"sonata","3":"2.5","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"18","9":"26","10":"r","11":"midsize","_rn_":"113"},{"1":"hyundai","2":"sonata","3":"2.5","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"18","9":"26","10":"r","11":"midsize","_rn_":"114"},{"1":"nissan","2":"maxima","3":"3.0","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"18","9":"26","10":"r","11":"midsize","_rn_":"148"},{"1":"pontiac","2":"grand prix","3":"3.1","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"18","9":"26","10":"r","11":"midsize","_rn_":"155"},{"1":"pontiac","2":"grand prix","3":"3.8","4":"2008","5":"6","6":"auto(l4)","7":"f","8":"18","9":"28","10":"r","11":"midsize","_rn_":"158"},{"1":"subaru","2":"forester awd","3":"2.5","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"18","9":"25","10":"r","11":"suv","_rn_":"160"},{"1":"subaru","2":"forester awd","3":"2.5","4":"1999","5":"4","6":"auto(l4)","7":"4","8":"18","9":"24","10":"r","11":"suv","_rn_":"161"},{"1":"subaru","2":"forester awd","3":"2.5","4":"2008","5":"4","6":"auto(l4)","7":"4","8":"18","9":"23","10":"p","11":"suv","_rn_":"165"},{"1":"toyota","2":"camry","3":"3.0","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"18","9":"26","10":"r","11":"midsize","_rn_":"184"},{"1":"toyota","2":"camry","3":"3.0","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"18","9":"26","10":"r","11":"midsize","_rn_":"185"},{"1":"toyota","2":"camry solara","3":"3.0","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"18","9":"26","10":"r","11":"compact","_rn_":"191"},{"1":"toyota","2":"camry solara","3":"3.0","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"18","9":"26","10":"r","11":"compact","_rn_":"192"},{"1":"toyota","2":"camry solara","3":"3.3","4":"2008","5":"6","6":"auto(s5)","7":"f","8":"18","9":"27","10":"r","11":"compact","_rn_":"193"},{"1":"volkswagen","2":"passat","3":"1.8","4":"1999","5":"4","6":"auto(l5)","7":"f","8":"18","9":"29","10":"p","11":"midsize","_rn_":"229"},{"1":"volkswagen","2":"passat","3":"2.8","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"18","9":"26","10":"p","11":"midsize","_rn_":"233"},{"1":"audi","2":"a4 quattro","3":"2.8","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"17","9":"25","10":"p","11":"compact","_rn_":"13"},{"1":"audi","2":"a4 quattro","3":"3.1","4":"2008","5":"6","6":"auto(s6)","7":"4","8":"17","9":"25","10":"p","11":"compact","_rn_":"14"},{"1":"audi","2":"a6 quattro","3":"3.1","4":"2008","5":"6","6":"auto(s6)","7":"4","8":"17","9":"25","10":"p","11":"midsize","_rn_":"17"},{"1":"chevrolet","2":"malibu","3":"3.6","4":"2008","5":"6","6":"auto(s6)","7":"f","8":"17","9":"26","10":"r","11":"midsize","_rn_":"37"},{"1":"dodge","2":"caravan 2wd","3":"3.0","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"17","9":"24","10":"r","11":"minivan","_rn_":"39"},{"1":"dodge","2":"caravan 2wd","3":"3.3","4":"2008","5":"6","6":"auto(l4)","7":"f","8":"17","9":"24","10":"r","11":"minivan","_rn_":"42"},{"1":"dodge","2":"caravan 2wd","3":"3.3","4":"2008","5":"6","6":"auto(l4)","7":"f","8":"17","9":"24","10":"r","11":"minivan","_rn_":"43"},{"1":"ford","2":"mustang","3":"4.0","4":"2008","5":"6","6":"manual(m5)","7":"r","8":"17","9":"26","10":"r","11":"subcompact","_rn_":"93"},{"1":"hyundai","2":"tiburon","3":"2.7","4":"2008","5":"6","6":"auto(l4)","7":"f","8":"17","9":"24","10":"r","11":"subcompact","_rn_":"120"},{"1":"hyundai","2":"tiburon","3":"2.7","4":"2008","5":"6","6":"manual(m5)","7":"f","8":"17","9":"24","10":"r","11":"subcompact","_rn_":"122"},{"1":"jeep","2":"grand cherokee 4wd","3":"3.0","4":"2008","5":"6","6":"auto(l5)","7":"4","8":"17","9":"22","10":"d","11":"suv","_rn_":"123"},{"1":"pontiac","2":"grand prix","3":"3.8","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"17","9":"27","10":"r","11":"midsize","_rn_":"157"},{"1":"toyota","2":"toyota tacoma 4wd","3":"2.7","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"17","9":"22","10":"r","11":"pickup","_rn_":"203"},{"1":"volkswagen","2":"gti","3":"2.8","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"17","9":"24","10":"r","11":"compact","_rn_":"212"},{"1":"volkswagen","2":"jetta","3":"2.8","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"17","9":"24","10":"r","11":"compact","_rn_":"221"},{"1":"volkswagen","2":"passat","3":"3.6","4":"2008","5":"6","6":"auto(s6)","7":"f","8":"17","9":"26","10":"p","11":"midsize","_rn_":"234"},{"1":"audi","2":"a4","3":"2.8","4":"1999","5":"6","6":"auto(l5)","7":"f","8":"16","9":"26","10":"p","11":"compact","_rn_":"5"},{"1":"audi","2":"a4 quattro","3":"1.8","4":"1999","5":"4","6":"auto(l5)","7":"4","8":"16","9":"25","10":"p","11":"compact","_rn_":"9"},{"1":"audi","2":"a6 quattro","3":"4.2","4":"2008","5":"8","6":"auto(s6)","7":"4","8":"16","9":"23","10":"p","11":"midsize","_rn_":"18"},{"1":"chevrolet","2":"corvette","3":"5.7","4":"1999","5":"8","6":"manual(m6)","7":"r","8":"16","9":"26","10":"p","11":"2seater","_rn_":"24"},{"1":"chevrolet","2":"corvette","3":"6.2","4":"2008","5":"8","6":"manual(m6)","7":"r","8":"16","9":"26","10":"p","11":"2seater","_rn_":"26"},{"1":"dodge","2":"caravan 2wd","3":"3.3","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"16","9":"22","10":"r","11":"minivan","_rn_":"40"},{"1":"dodge","2":"caravan 2wd","3":"3.3","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"16","9":"22","10":"r","11":"minivan","_rn_":"41"},{"1":"dodge","2":"caravan 2wd","3":"3.8","4":"2008","5":"6","6":"auto(l6)","7":"f","8":"16","9":"23","10":"r","11":"minivan","_rn_":"47"},{"1":"dodge","2":"caravan 2wd","3":"4.0","4":"2008","5":"6","6":"auto(l6)","7":"f","8":"16","9":"23","10":"r","11":"minivan","_rn_":"48"},{"1":"ford","2":"mustang","3":"4.0","4":"2008","5":"6","6":"auto(l5)","7":"r","8":"16","9":"24","10":"r","11":"subcompact","_rn_":"94"},{"1":"hyundai","2":"tiburon","3":"2.7","4":"2008","5":"6","6":"manual(m6)","7":"f","8":"16","9":"24","10":"r","11":"subcompact","_rn_":"121"},{"1":"pontiac","2":"grand prix","3":"3.8","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"16","9":"26","10":"p","11":"midsize","_rn_":"156"},{"1":"pontiac","2":"grand prix","3":"5.3","4":"2008","5":"8","6":"auto(s4)","7":"f","8":"16","9":"25","10":"p","11":"midsize","_rn_":"159"},{"1":"toyota","2":"4runner 4wd","3":"2.7","4":"1999","5":"4","6":"auto(l4)","7":"4","8":"16","9":"20","10":"r","11":"suv","_rn_":"175"},{"1":"toyota","2":"4runner 4wd","3":"4.0","4":"2008","5":"6","6":"auto(l5)","7":"4","8":"16","9":"20","10":"r","11":"suv","_rn_":"178"},{"1":"toyota","2":"toyota tacoma 4wd","3":"2.7","4":"1999","5":"4","6":"auto(l4)","7":"4","8":"16","9":"20","10":"r","11":"pickup","_rn_":"202"},{"1":"toyota","2":"toyota tacoma 4wd","3":"4.0","4":"2008","5":"6","6":"auto(l5)","7":"4","8":"16","9":"20","10":"r","11":"pickup","_rn_":"207"},{"1":"volkswagen","2":"jetta","3":"2.8","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"16","9":"23","10":"r","11":"compact","_rn_":"220"},{"1":"volkswagen","2":"passat","3":"2.8","4":"1999","5":"6","6":"auto(l5)","7":"f","8":"16","9":"26","10":"p","11":"midsize","_rn_":"232"},{"1":"audi","2":"a4 quattro","3":"2.8","4":"1999","5":"6","6":"auto(l5)","7":"4","8":"15","9":"25","10":"p","11":"compact","_rn_":"12"},{"1":"audi","2":"a4 quattro","3":"3.1","4":"2008","5":"6","6":"manual(m6)","7":"4","8":"15","9":"25","10":"p","11":"compact","_rn_":"15"},{"1":"audi","2":"a6 quattro","3":"2.8","4":"1999","5":"6","6":"auto(l5)","7":"4","8":"15","9":"24","10":"p","11":"midsize","_rn_":"16"},{"1":"chevrolet","2":"corvette","3":"5.7","4":"1999","5":"8","6":"auto(l4)","7":"r","8":"15","9":"23","10":"p","11":"2seater","_rn_":"25"},{"1":"chevrolet","2":"corvette","3":"6.2","4":"2008","5":"8","6":"auto(s6)","7":"r","8":"15","9":"25","10":"p","11":"2seater","_rn_":"27"},{"1":"chevrolet","2":"corvette","3":"7.0","4":"2008","5":"8","6":"manual(m6)","7":"r","8":"15","9":"24","10":"p","11":"2seater","_rn_":"28"},{"1":"dodge","2":"caravan 2wd","3":"3.8","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"15","9":"22","10":"r","11":"minivan","_rn_":"45"},{"1":"dodge","2":"caravan 2wd","3":"3.8","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"15","9":"21","10":"r","11":"minivan","_rn_":"46"},{"1":"dodge","2":"dakota pickup 4wd","3":"3.7","4":"2008","5":"6","6":"manual(m6)","7":"4","8":"15","9":"19","10":"r","11":"pickup","_rn_":"49"},{"1":"ford","2":"explorer 4wd","3":"4.0","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"15","9":"19","10":"r","11":"suv","_rn_":"79"},{"1":"ford","2":"mustang","3":"4.6","4":"1999","5":"8","6":"auto(l4)","7":"r","8":"15","9":"21","10":"r","11":"subcompact","_rn_":"95"},{"1":"ford","2":"mustang","3":"4.6","4":"1999","5":"8","6":"manual(m5)","7":"r","8":"15","9":"22","10":"r","11":"subcompact","_rn_":"96"},{"1":"ford","2":"mustang","3":"4.6","4":"2008","5":"8","6":"manual(m5)","7":"r","8":"15","9":"23","10":"r","11":"subcompact","_rn_":"97"},{"1":"ford","2":"mustang","3":"4.6","4":"2008","5":"8","6":"auto(l5)","7":"r","8":"15","9":"22","10":"r","11":"subcompact","_rn_":"98"},{"1":"jeep","2":"grand cherokee 4wd","3":"3.7","4":"2008","5":"6","6":"auto(l5)","7":"4","8":"15","9":"19","10":"r","11":"suv","_rn_":"124"},{"1":"jeep","2":"grand cherokee 4wd","3":"4.0","4":"1999","5":"6","6":"auto(l4)","7":"4","8":"15","9":"20","10":"r","11":"suv","_rn_":"125"},{"1":"nissan","2":"pathfinder 4wd","3":"3.3","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"15","9":"17","10":"r","11":"suv","_rn_":"152"},{"1":"toyota","2":"4runner 4wd","3":"2.7","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"15","9":"20","10":"r","11":"suv","_rn_":"174"},{"1":"toyota","2":"4runner 4wd","3":"3.4","4":"1999","5":"6","6":"auto(l4)","7":"4","8":"15","9":"19","10":"r","11":"suv","_rn_":"176"},{"1":"toyota","2":"4runner 4wd","3":"3.4","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"15","9":"17","10":"r","11":"suv","_rn_":"177"},{"1":"toyota","2":"toyota tacoma 4wd","3":"2.7","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"15","9":"20","10":"r","11":"pickup","_rn_":"201"},{"1":"toyota","2":"toyota tacoma 4wd","3":"3.4","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"15","9":"17","10":"r","11":"pickup","_rn_":"204"},{"1":"toyota","2":"toyota tacoma 4wd","3":"3.4","4":"1999","5":"6","6":"auto(l4)","7":"4","8":"15","9":"19","10":"r","11":"pickup","_rn_":"205"},{"1":"toyota","2":"toyota tacoma 4wd","3":"4.0","4":"2008","5":"6","6":"manual(m6)","7":"4","8":"15","9":"18","10":"r","11":"pickup","_rn_":"206"},{"1":"chevrolet","2":"c1500 suburban 2wd","3":"5.3","4":"2008","5":"8","6":"auto(l4)","7":"r","8":"14","9":"20","10":"r","11":"suv","_rn_":"19"},{"1":"chevrolet","2":"c1500 suburban 2wd","3":"5.3","4":"2008","5":"8","6":"auto(l4)","7":"r","8":"14","9":"20","10":"r","11":"suv","_rn_":"21"},{"1":"chevrolet","2":"k1500 tahoe 4wd","3":"5.3","4":"2008","5":"8","6":"auto(l4)","7":"4","8":"14","9":"19","10":"r","11":"suv","_rn_":"29"},{"1":"chevrolet","2":"k1500 tahoe 4wd","3":"6.5","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"14","9":"17","10":"d","11":"suv","_rn_":"32"},{"1":"dodge","2":"dakota pickup 4wd","3":"3.7","4":"2008","5":"6","6":"auto(l4)","7":"4","8":"14","9":"18","10":"r","11":"pickup","_rn_":"50"},{"1":"dodge","2":"dakota pickup 4wd","3":"3.9","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"14","9":"17","10":"r","11":"pickup","_rn_":"52"},{"1":"dodge","2":"dakota pickup 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"14","9":"19","10":"r","11":"pickup","_rn_":"53"},{"1":"dodge","2":"dakota pickup 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"14","9":"19","10":"r","11":"pickup","_rn_":"54"},{"1":"ford","2":"explorer 4wd","3":"4.0","4":"1999","5":"6","6":"auto(l5)","7":"4","8":"14","9":"17","10":"r","11":"suv","_rn_":"78"},{"1":"ford","2":"explorer 4wd","3":"4.0","4":"1999","5":"6","6":"auto(l5)","7":"4","8":"14","9":"17","10":"r","11":"suv","_rn_":"80"},{"1":"ford","2":"f150 pickup 4wd","3":"4.2","4":"1999","5":"6","6":"auto(l4)","7":"4","8":"14","9":"17","10":"r","11":"pickup","_rn_":"84"},{"1":"ford","2":"f150 pickup 4wd","3":"4.2","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"14","9":"17","10":"r","11":"pickup","_rn_":"85"},{"1":"ford","2":"mustang","3":"5.4","4":"2008","5":"8","6":"manual(m6)","7":"r","8":"14","9":"20","10":"p","11":"subcompact","_rn_":"99"},{"1":"jeep","2":"grand cherokee 4wd","3":"4.7","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"14","9":"17","10":"r","11":"suv","_rn_":"126"},{"1":"jeep","2":"grand cherokee 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"14","9":"19","10":"r","11":"suv","_rn_":"128"},{"1":"mercury","2":"mountaineer 4wd","3":"4.0","4":"1999","5":"6","6":"auto(l5)","7":"4","8":"14","9":"17","10":"r","11":"suv","_rn_":"138"},{"1":"nissan","2":"pathfinder 4wd","3":"3.3","4":"1999","5":"6","6":"auto(l4)","7":"4","8":"14","9":"17","10":"r","11":"suv","_rn_":"151"},{"1":"nissan","2":"pathfinder 4wd","3":"4.0","4":"2008","5":"6","6":"auto(l5)","7":"4","8":"14","9":"20","10":"p","11":"suv","_rn_":"153"},{"1":"toyota","2":"4runner 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"14","9":"17","10":"r","11":"suv","_rn_":"179"},{"1":"chevrolet","2":"c1500 suburban 2wd","3":"5.7","4":"1999","5":"8","6":"auto(l4)","7":"r","8":"13","9":"17","10":"r","11":"suv","_rn_":"22"},{"1":"dodge","2":"dakota pickup 4wd","3":"3.9","4":"1999","5":"6","6":"auto(l4)","7":"4","8":"13","9":"17","10":"r","11":"pickup","_rn_":"51"},{"1":"dodge","2":"durango 4wd","3":"3.9","4":"1999","5":"6","6":"auto(l4)","7":"4","8":"13","9":"17","10":"r","11":"suv","_rn_":"58"},{"1":"dodge","2":"durango 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"13","9":"17","10":"r","11":"suv","_rn_":"59"},{"1":"dodge","2":"durango 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"13","9":"17","10":"r","11":"suv","_rn_":"61"},{"1":"dodge","2":"durango 4wd","3":"5.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"13","9":"18","10":"r","11":"suv","_rn_":"63"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"13","9":"17","10":"r","11":"pickup","_rn_":"67"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"13","9":"17","10":"r","11":"pickup","_rn_":"68"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"5.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"13","9":"17","10":"r","11":"pickup","_rn_":"73"},{"1":"ford","2":"explorer 4wd","3":"4.0","4":"2008","5":"6","6":"auto(l5)","7":"4","8":"13","9":"19","10":"r","11":"suv","_rn_":"81"},{"1":"ford","2":"explorer 4wd","3":"4.6","4":"2008","5":"8","6":"auto(l6)","7":"4","8":"13","9":"19","10":"r","11":"suv","_rn_":"82"},{"1":"ford","2":"explorer 4wd","3":"5.0","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"13","9":"17","10":"r","11":"suv","_rn_":"83"},{"1":"ford","2":"f150 pickup 4wd","3":"4.6","4":"1999","5":"8","6":"manual(m5)","7":"4","8":"13","9":"16","10":"r","11":"pickup","_rn_":"86"},{"1":"ford","2":"f150 pickup 4wd","3":"4.6","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"13","9":"16","10":"r","11":"pickup","_rn_":"87"},{"1":"ford","2":"f150 pickup 4wd","3":"4.6","4":"2008","5":"8","6":"auto(l4)","7":"4","8":"13","9":"17","10":"r","11":"pickup","_rn_":"88"},{"1":"ford","2":"f150 pickup 4wd","3":"5.4","4":"2008","5":"8","6":"auto(l4)","7":"4","8":"13","9":"17","10":"r","11":"pickup","_rn_":"90"},{"1":"jeep","2":"grand cherokee 4wd","3":"5.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"13","9":"18","10":"r","11":"suv","_rn_":"129"},{"1":"mercury","2":"mountaineer 4wd","3":"4.0","4":"2008","5":"6","6":"auto(l5)","7":"4","8":"13","9":"19","10":"r","11":"suv","_rn_":"139"},{"1":"mercury","2":"mountaineer 4wd","3":"4.6","4":"2008","5":"8","6":"auto(l6)","7":"4","8":"13","9":"19","10":"r","11":"suv","_rn_":"140"},{"1":"mercury","2":"mountaineer 4wd","3":"5.0","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"13","9":"17","10":"r","11":"suv","_rn_":"141"},{"1":"toyota","2":"land cruiser wagon 4wd","3":"5.7","4":"2008","5":"8","6":"auto(s6)","7":"4","8":"13","9":"18","10":"r","11":"suv","_rn_":"200"},{"1":"chevrolet","2":"c1500 suburban 2wd","3":"6.0","4":"2008","5":"8","6":"auto(l4)","7":"r","8":"12","9":"17","10":"r","11":"suv","_rn_":"23"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"manual(m6)","7":"4","8":"12","9":"16","10":"r","11":"pickup","_rn_":"65"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"manual(m6)","7":"4","8":"12","9":"16","10":"r","11":"pickup","_rn_":"69"},{"1":"ford","2":"expedition 2wd","3":"5.4","4":"2008","5":"8","6":"auto(l6)","7":"r","8":"12","9":"18","10":"r","11":"suv","_rn_":"77"},{"1":"land rover","2":"range rover","3":"4.2","4":"2008","5":"8","6":"auto(s6)","7":"4","8":"12","9":"18","10":"r","11":"suv","_rn_":"132"},{"1":"land rover","2":"range rover","3":"4.4","4":"2008","5":"8","6":"auto(s6)","7":"4","8":"12","9":"18","10":"r","11":"suv","_rn_":"133"},{"1":"lincoln","2":"navigator 2wd","3":"5.4","4":"2008","5":"8","6":"auto(l6)","7":"r","8":"12","9":"18","10":"r","11":"suv","_rn_":"137"},{"1":"nissan","2":"pathfinder 4wd","3":"5.6","4":"2008","5":"8","6":"auto(s5)","7":"4","8":"12","9":"18","10":"p","11":"suv","_rn_":"154"},{"1":"chevrolet","2":"c1500 suburban 2wd","3":"5.3","4":"2008","5":"8","6":"auto(l4)","7":"r","8":"11","9":"15","10":"e","11":"suv","_rn_":"20"},{"1":"chevrolet","2":"k1500 tahoe 4wd","3":"5.3","4":"2008","5":"8","6":"auto(l4)","7":"4","8":"11","9":"14","10":"e","11":"suv","_rn_":"30"},{"1":"chevrolet","2":"k1500 tahoe 4wd","3":"5.7","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11","9":"15","10":"r","11":"suv","_rn_":"31"},{"1":"dodge","2":"caravan 2wd","3":"3.3","4":"2008","5":"6","6":"auto(l4)","7":"f","8":"11","9":"17","10":"e","11":"minivan","_rn_":"44"},{"1":"dodge","2":"dakota pickup 4wd","3":"5.2","4":"1999","5":"8","6":"manual(m5)","7":"4","8":"11","9":"17","10":"r","11":"pickup","_rn_":"56"},{"1":"dodge","2":"dakota pickup 4wd","3":"5.2","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11","9":"15","10":"r","11":"pickup","_rn_":"57"},{"1":"dodge","2":"durango 4wd","3":"5.2","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11","9":"16","10":"r","11":"suv","_rn_":"62"},{"1":"dodge","2":"durango 4wd","3":"5.9","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11","9":"15","10":"r","11":"suv","_rn_":"64"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"5.2","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11","9":"15","10":"r","11":"pickup","_rn_":"71"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"5.2","4":"1999","5":"8","6":"manual(m5)","7":"4","8":"11","9":"16","10":"r","11":"pickup","_rn_":"72"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"5.9","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11","9":"15","10":"r","11":"pickup","_rn_":"74"},{"1":"ford","2":"expedition 2wd","3":"4.6","4":"1999","5":"8","6":"auto(l4)","7":"r","8":"11","9":"17","10":"r","11":"suv","_rn_":"75"},{"1":"ford","2":"expedition 2wd","3":"5.4","4":"1999","5":"8","6":"auto(l4)","7":"r","8":"11","9":"17","10":"r","11":"suv","_rn_":"76"},{"1":"ford","2":"f150 pickup 4wd","3":"5.4","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11","9":"15","10":"r","11":"pickup","_rn_":"89"},{"1":"jeep","2":"grand cherokee 4wd","3":"6.1","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"11","9":"14","10":"p","11":"suv","_rn_":"130"},{"1":"land rover","2":"range rover","3":"4.0","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11","9":"15","10":"p","11":"suv","_rn_":"131"},{"1":"land rover","2":"range rover","3":"4.6","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11","9":"15","10":"p","11":"suv","_rn_":"134"},{"1":"lincoln","2":"navigator 2wd","3":"5.4","4":"1999","5":"8","6":"auto(l4)","7":"r","8":"11","9":"17","10":"r","11":"suv","_rn_":"135"},{"1":"lincoln","2":"navigator 2wd","3":"5.4","4":"1999","5":"8","6":"auto(l4)","7":"r","8":"11","9":"16","10":"p","11":"suv","_rn_":"136"},{"1":"toyota","2":"land cruiser wagon 4wd","3":"4.7","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11","9":"15","10":"r","11":"suv","_rn_":"199"},{"1":"dodge","2":"dakota pickup 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"9","9":"12","10":"e","11":"pickup","_rn_":"55"},{"1":"dodge","2":"durango 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"9","9":"12","10":"e","11":"suv","_rn_":"60"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"9","9":"12","10":"e","11":"pickup","_rn_":"66"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"manual(m6)","7":"4","8":"9","9":"12","10":"e","11":"pickup","_rn_":"70"},{"1":"jeep","2":"grand cherokee 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"9","9":"12","10":"e","11":"suv","_rn_":"127"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

```r
#Sort by manufacturer, then by city MPG
mpg[order(mpg$manufacturer, -mpg$cty) ,]
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["manufacturer"],"name":[1],"type":["fct"],"align":["left"]},{"label":["model"],"name":[2],"type":["fct"],"align":["left"]},{"label":["displ"],"name":[3],"type":["dbl"],"align":["right"]},{"label":["year"],"name":[4],"type":["int"],"align":["right"]},{"label":["cyl"],"name":[5],"type":["int"],"align":["right"]},{"label":["trans"],"name":[6],"type":["chr"],"align":["left"]},{"label":["drv"],"name":[7],"type":["chr"],"align":["left"]},{"label":["cty"],"name":[8],"type":["int"],"align":["right"]},{"label":["hwy"],"name":[9],"type":["int"],"align":["right"]},{"label":["fl"],"name":[10],"type":["chr"],"align":["left"]},{"label":["class"],"name":[11],"type":["chr"],"align":["left"]}],"data":[{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"28","9":"33","10":"r","11":"subcompact","_rn_":"100"},{"1":"honda","2":"civic","3":"1.8","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"26","9":"34","10":"r","11":"subcompact","_rn_":"105"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"25","9":"32","10":"r","11":"subcompact","_rn_":"102"},{"1":"honda","2":"civic","3":"1.8","4":"2008","5":"4","6":"auto(l5)","7":"f","8":"25","9":"36","10":"r","11":"subcompact","_rn_":"106"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"24","9":"32","10":"r","11":"subcompact","_rn_":"101"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"24","9":"32","10":"r","11":"subcompact","_rn_":"104"},{"1":"honda","2":"civic","3":"1.8","4":"2008","5":"4","6":"auto(l5)","7":"f","8":"24","9":"36","10":"c","11":"subcompact","_rn_":"107"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"23","9":"29","10":"p","11":"subcompact","_rn_":"103"},{"1":"honda","2":"civic","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"21","9":"29","10":"p","11":"subcompact","_rn_":"108"},{"1":"toyota","2":"corolla","3":"1.8","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"28","9":"37","10":"r","11":"compact","_rn_":"197"},{"1":"toyota","2":"corolla","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"26","9":"35","10":"r","11":"compact","_rn_":"196"},{"1":"toyota","2":"corolla","3":"1.8","4":"2008","5":"4","6":"auto(l4)","7":"f","8":"26","9":"35","10":"r","11":"compact","_rn_":"198"},{"1":"toyota","2":"corolla","3":"1.8","4":"1999","5":"4","6":"auto(l3)","7":"f","8":"24","9":"30","10":"r","11":"compact","_rn_":"194"},{"1":"toyota","2":"corolla","3":"1.8","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"24","9":"33","10":"r","11":"compact","_rn_":"195"},{"1":"toyota","2":"camry solara","3":"2.4","4":"2008","5":"4","6":"auto(s5)","7":"f","8":"22","9":"31","10":"r","11":"compact","_rn_":"190"},{"1":"toyota","2":"camry","3":"2.2","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"midsize","_rn_":"180"},{"1":"toyota","2":"camry","3":"2.2","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"21","9":"27","10":"r","11":"midsize","_rn_":"181"},{"1":"toyota","2":"camry","3":"2.4","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"21","9":"31","10":"r","11":"midsize","_rn_":"182"},{"1":"toyota","2":"camry","3":"2.4","4":"2008","5":"4","6":"auto(l5)","7":"f","8":"21","9":"31","10":"r","11":"midsize","_rn_":"183"},{"1":"toyota","2":"camry solara","3":"2.2","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"21","9":"27","10":"r","11":"compact","_rn_":"187"},{"1":"toyota","2":"camry solara","3":"2.2","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"188"},{"1":"toyota","2":"camry solara","3":"2.4","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"21","9":"31","10":"r","11":"compact","_rn_":"189"},{"1":"toyota","2":"camry","3":"3.5","4":"2008","5":"6","6":"auto(s6)","7":"f","8":"19","9":"28","10":"r","11":"midsize","_rn_":"186"},{"1":"toyota","2":"camry","3":"3.0","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"18","9":"26","10":"r","11":"midsize","_rn_":"184"},{"1":"toyota","2":"camry","3":"3.0","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"18","9":"26","10":"r","11":"midsize","_rn_":"185"},{"1":"toyota","2":"camry solara","3":"3.0","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"18","9":"26","10":"r","11":"compact","_rn_":"191"},{"1":"toyota","2":"camry solara","3":"3.0","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"18","9":"26","10":"r","11":"compact","_rn_":"192"},{"1":"toyota","2":"camry solara","3":"3.3","4":"2008","5":"6","6":"auto(s5)","7":"f","8":"18","9":"27","10":"r","11":"compact","_rn_":"193"},{"1":"toyota","2":"toyota tacoma 4wd","3":"2.7","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"17","9":"22","10":"r","11":"pickup","_rn_":"203"},{"1":"toyota","2":"4runner 4wd","3":"2.7","4":"1999","5":"4","6":"auto(l4)","7":"4","8":"16","9":"20","10":"r","11":"suv","_rn_":"175"},{"1":"toyota","2":"4runner 4wd","3":"4.0","4":"2008","5":"6","6":"auto(l5)","7":"4","8":"16","9":"20","10":"r","11":"suv","_rn_":"178"},{"1":"toyota","2":"toyota tacoma 4wd","3":"2.7","4":"1999","5":"4","6":"auto(l4)","7":"4","8":"16","9":"20","10":"r","11":"pickup","_rn_":"202"},{"1":"toyota","2":"toyota tacoma 4wd","3":"4.0","4":"2008","5":"6","6":"auto(l5)","7":"4","8":"16","9":"20","10":"r","11":"pickup","_rn_":"207"},{"1":"toyota","2":"4runner 4wd","3":"2.7","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"15","9":"20","10":"r","11":"suv","_rn_":"174"},{"1":"toyota","2":"4runner 4wd","3":"3.4","4":"1999","5":"6","6":"auto(l4)","7":"4","8":"15","9":"19","10":"r","11":"suv","_rn_":"176"},{"1":"toyota","2":"4runner 4wd","3":"3.4","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"15","9":"17","10":"r","11":"suv","_rn_":"177"},{"1":"toyota","2":"toyota tacoma 4wd","3":"2.7","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"15","9":"20","10":"r","11":"pickup","_rn_":"201"},{"1":"toyota","2":"toyota tacoma 4wd","3":"3.4","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"15","9":"17","10":"r","11":"pickup","_rn_":"204"},{"1":"toyota","2":"toyota tacoma 4wd","3":"3.4","4":"1999","5":"6","6":"auto(l4)","7":"4","8":"15","9":"19","10":"r","11":"pickup","_rn_":"205"},{"1":"toyota","2":"toyota tacoma 4wd","3":"4.0","4":"2008","5":"6","6":"manual(m6)","7":"4","8":"15","9":"18","10":"r","11":"pickup","_rn_":"206"},{"1":"toyota","2":"4runner 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"14","9":"17","10":"r","11":"suv","_rn_":"179"},{"1":"toyota","2":"land cruiser wagon 4wd","3":"5.7","4":"2008","5":"8","6":"auto(s6)","7":"4","8":"13","9":"18","10":"r","11":"suv","_rn_":"200"},{"1":"toyota","2":"land cruiser wagon 4wd","3":"4.7","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11","9":"15","10":"r","11":"suv","_rn_":"199"},{"1":"nissan","2":"altima","3":"2.5","4":"2008","5":"4","6":"auto(av)","7":"f","8":"23","9":"31","10":"r","11":"midsize","_rn_":"144"},{"1":"nissan","2":"altima","3":"2.5","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"23","9":"32","10":"r","11":"midsize","_rn_":"145"},{"1":"nissan","2":"altima","3":"2.4","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"142"},{"1":"nissan","2":"altima","3":"2.4","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19","9":"27","10":"r","11":"compact","_rn_":"143"},{"1":"nissan","2":"altima","3":"3.5","4":"2008","5":"6","6":"manual(m6)","7":"f","8":"19","9":"27","10":"p","11":"midsize","_rn_":"146"},{"1":"nissan","2":"altima","3":"3.5","4":"2008","5":"6","6":"auto(av)","7":"f","8":"19","9":"26","10":"p","11":"midsize","_rn_":"147"},{"1":"nissan","2":"maxima","3":"3.0","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"19","9":"25","10":"r","11":"midsize","_rn_":"149"},{"1":"nissan","2":"maxima","3":"3.5","4":"2008","5":"6","6":"auto(av)","7":"f","8":"19","9":"25","10":"p","11":"midsize","_rn_":"150"},{"1":"nissan","2":"maxima","3":"3.0","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"18","9":"26","10":"r","11":"midsize","_rn_":"148"},{"1":"nissan","2":"pathfinder 4wd","3":"3.3","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"15","9":"17","10":"r","11":"suv","_rn_":"152"},{"1":"nissan","2":"pathfinder 4wd","3":"3.3","4":"1999","5":"6","6":"auto(l4)","7":"4","8":"14","9":"17","10":"r","11":"suv","_rn_":"151"},{"1":"nissan","2":"pathfinder 4wd","3":"4.0","4":"2008","5":"6","6":"auto(l5)","7":"4","8":"14","9":"20","10":"p","11":"suv","_rn_":"153"},{"1":"nissan","2":"pathfinder 4wd","3":"5.6","4":"2008","5":"8","6":"auto(s5)","7":"4","8":"12","9":"18","10":"p","11":"suv","_rn_":"154"},{"1":"subaru","2":"impreza awd","3":"2.2","4":"1999","5":"4","6":"auto(l4)","7":"4","8":"21","9":"26","10":"r","11":"subcompact","_rn_":"166"},{"1":"subaru","2":"forester awd","3":"2.5","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"20","9":"27","10":"r","11":"suv","_rn_":"162"},{"1":"subaru","2":"forester awd","3":"2.5","4":"2008","5":"4","6":"auto(l4)","7":"4","8":"20","9":"26","10":"r","11":"suv","_rn_":"164"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"2008","5":"4","6":"auto(s4)","7":"4","8":"20","9":"25","10":"p","11":"compact","_rn_":"170"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"2008","5":"4","6":"auto(s4)","7":"4","8":"20","9":"27","10":"r","11":"compact","_rn_":"171"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"20","9":"27","10":"r","11":"compact","_rn_":"173"},{"1":"subaru","2":"forester awd","3":"2.5","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"19","9":"25","10":"p","11":"suv","_rn_":"163"},{"1":"subaru","2":"impreza awd","3":"2.2","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"19","9":"26","10":"r","11":"subcompact","_rn_":"167"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"19","9":"26","10":"r","11":"subcompact","_rn_":"168"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"1999","5":"4","6":"auto(l4)","7":"4","8":"19","9":"26","10":"r","11":"subcompact","_rn_":"169"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"19","9":"25","10":"p","11":"compact","_rn_":"172"},{"1":"subaru","2":"forester awd","3":"2.5","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"18","9":"25","10":"r","11":"suv","_rn_":"160"},{"1":"subaru","2":"forester awd","3":"2.5","4":"1999","5":"4","6":"auto(l4)","7":"4","8":"18","9":"24","10":"r","11":"suv","_rn_":"161"},{"1":"subaru","2":"forester awd","3":"2.5","4":"2008","5":"4","6":"auto(l4)","7":"4","8":"18","9":"23","10":"p","11":"suv","_rn_":"165"},{"1":"hyundai","2":"sonata","3":"2.4","4":"2008","5":"4","6":"auto(l4)","7":"f","8":"21","9":"30","10":"r","11":"midsize","_rn_":"111"},{"1":"hyundai","2":"sonata","3":"2.4","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"21","9":"31","10":"r","11":"midsize","_rn_":"112"},{"1":"hyundai","2":"tiburon","3":"2.0","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"20","9":"28","10":"r","11":"subcompact","_rn_":"118"},{"1":"hyundai","2":"tiburon","3":"2.0","4":"2008","5":"4","6":"auto(l4)","7":"f","8":"20","9":"27","10":"r","11":"subcompact","_rn_":"119"},{"1":"hyundai","2":"sonata","3":"3.3","4":"2008","5":"6","6":"auto(l5)","7":"f","8":"19","9":"28","10":"r","11":"midsize","_rn_":"115"},{"1":"hyundai","2":"tiburon","3":"2.0","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19","9":"26","10":"r","11":"subcompact","_rn_":"116"},{"1":"hyundai","2":"tiburon","3":"2.0","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"19","9":"29","10":"r","11":"subcompact","_rn_":"117"},{"1":"hyundai","2":"sonata","3":"2.4","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"18","9":"26","10":"r","11":"midsize","_rn_":"109"},{"1":"hyundai","2":"sonata","3":"2.4","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"18","9":"27","10":"r","11":"midsize","_rn_":"110"},{"1":"hyundai","2":"sonata","3":"2.5","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"18","9":"26","10":"r","11":"midsize","_rn_":"113"},{"1":"hyundai","2":"sonata","3":"2.5","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"18","9":"26","10":"r","11":"midsize","_rn_":"114"},{"1":"hyundai","2":"tiburon","3":"2.7","4":"2008","5":"6","6":"auto(l4)","7":"f","8":"17","9":"24","10":"r","11":"subcompact","_rn_":"120"},{"1":"hyundai","2":"tiburon","3":"2.7","4":"2008","5":"6","6":"manual(m5)","7":"f","8":"17","9":"24","10":"r","11":"subcompact","_rn_":"122"},{"1":"hyundai","2":"tiburon","3":"2.7","4":"2008","5":"6","6":"manual(m6)","7":"f","8":"16","9":"24","10":"r","11":"subcompact","_rn_":"121"},{"1":"audi","2":"a4","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"p","11":"compact","_rn_":"2"},{"1":"audi","2":"a4","3":"2.0","4":"2008","5":"4","6":"auto(av)","7":"f","8":"21","9":"30","10":"p","11":"compact","_rn_":"4"},{"1":"audi","2":"a4","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"20","9":"31","10":"p","11":"compact","_rn_":"3"},{"1":"audi","2":"a4 quattro","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"4","8":"20","9":"28","10":"p","11":"compact","_rn_":"10"},{"1":"audi","2":"a4 quattro","3":"2.0","4":"2008","5":"4","6":"auto(s6)","7":"4","8":"19","9":"27","10":"p","11":"compact","_rn_":"11"},{"1":"audi","2":"a4","3":"1.8","4":"1999","5":"4","6":"auto(l5)","7":"f","8":"18","9":"29","10":"p","11":"compact","_rn_":"1"},{"1":"audi","2":"a4","3":"2.8","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"18","9":"26","10":"p","11":"compact","_rn_":"6"},{"1":"audi","2":"a4","3":"3.1","4":"2008","5":"6","6":"auto(av)","7":"f","8":"18","9":"27","10":"p","11":"compact","_rn_":"7"},{"1":"audi","2":"a4 quattro","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"18","9":"26","10":"p","11":"compact","_rn_":"8"},{"1":"audi","2":"a4 quattro","3":"2.8","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"17","9":"25","10":"p","11":"compact","_rn_":"13"},{"1":"audi","2":"a4 quattro","3":"3.1","4":"2008","5":"6","6":"auto(s6)","7":"4","8":"17","9":"25","10":"p","11":"compact","_rn_":"14"},{"1":"audi","2":"a6 quattro","3":"3.1","4":"2008","5":"6","6":"auto(s6)","7":"4","8":"17","9":"25","10":"p","11":"midsize","_rn_":"17"},{"1":"audi","2":"a4","3":"2.8","4":"1999","5":"6","6":"auto(l5)","7":"f","8":"16","9":"26","10":"p","11":"compact","_rn_":"5"},{"1":"audi","2":"a4 quattro","3":"1.8","4":"1999","5":"4","6":"auto(l5)","7":"4","8":"16","9":"25","10":"p","11":"compact","_rn_":"9"},{"1":"audi","2":"a6 quattro","3":"4.2","4":"2008","5":"8","6":"auto(s6)","7":"4","8":"16","9":"23","10":"p","11":"midsize","_rn_":"18"},{"1":"audi","2":"a4 quattro","3":"2.8","4":"1999","5":"6","6":"auto(l5)","7":"4","8":"15","9":"25","10":"p","11":"compact","_rn_":"12"},{"1":"audi","2":"a4 quattro","3":"3.1","4":"2008","5":"6","6":"manual(m6)","7":"4","8":"15","9":"25","10":"p","11":"compact","_rn_":"15"},{"1":"audi","2":"a6 quattro","3":"2.8","4":"1999","5":"6","6":"auto(l5)","7":"4","8":"15","9":"24","10":"p","11":"midsize","_rn_":"16"},{"1":"land rover","2":"range rover","3":"4.2","4":"2008","5":"8","6":"auto(s6)","7":"4","8":"12","9":"18","10":"r","11":"suv","_rn_":"132"},{"1":"land rover","2":"range rover","3":"4.4","4":"2008","5":"8","6":"auto(s6)","7":"4","8":"12","9":"18","10":"r","11":"suv","_rn_":"133"},{"1":"land rover","2":"range rover","3":"4.0","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11","9":"15","10":"p","11":"suv","_rn_":"131"},{"1":"land rover","2":"range rover","3":"4.6","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11","9":"15","10":"p","11":"suv","_rn_":"134"},{"1":"volkswagen","2":"new beetle","3":"1.9","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"35","9":"44","10":"d","11":"subcompact","_rn_":"222"},{"1":"volkswagen","2":"jetta","3":"1.9","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"33","9":"44","10":"d","11":"compact","_rn_":"213"},{"1":"volkswagen","2":"new beetle","3":"1.9","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"29","9":"41","10":"d","11":"subcompact","_rn_":"223"},{"1":"volkswagen","2":"gti","3":"2.0","4":"2008","5":"4","6":"auto(s6)","7":"f","8":"22","9":"29","10":"p","11":"compact","_rn_":"211"},{"1":"volkswagen","2":"jetta","3":"2.0","4":"2008","5":"4","6":"auto(s6)","7":"f","8":"22","9":"29","10":"p","11":"compact","_rn_":"216"},{"1":"volkswagen","2":"gti","3":"2.0","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"208"},{"1":"volkswagen","2":"gti","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"21","9":"29","10":"p","11":"compact","_rn_":"210"},{"1":"volkswagen","2":"jetta","3":"2.0","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"214"},{"1":"volkswagen","2":"jetta","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"21","9":"29","10":"p","11":"compact","_rn_":"217"},{"1":"volkswagen","2":"jetta","3":"2.5","4":"2008","5":"5","6":"auto(s6)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"218"},{"1":"volkswagen","2":"jetta","3":"2.5","4":"2008","5":"5","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"219"},{"1":"volkswagen","2":"new beetle","3":"2.0","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"subcompact","_rn_":"224"},{"1":"volkswagen","2":"passat","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"p","11":"midsize","_rn_":"228"},{"1":"volkswagen","2":"passat","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"21","9":"29","10":"p","11":"midsize","_rn_":"231"},{"1":"volkswagen","2":"new beetle","3":"2.5","4":"2008","5":"5","6":"manual(m5)","7":"f","8":"20","9":"28","10":"r","11":"subcompact","_rn_":"226"},{"1":"volkswagen","2":"new beetle","3":"2.5","4":"2008","5":"5","6":"auto(s6)","7":"f","8":"20","9":"29","10":"r","11":"subcompact","_rn_":"227"},{"1":"volkswagen","2":"gti","3":"2.0","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19","9":"26","10":"r","11":"compact","_rn_":"209"},{"1":"volkswagen","2":"jetta","3":"2.0","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19","9":"26","10":"r","11":"compact","_rn_":"215"},{"1":"volkswagen","2":"new beetle","3":"2.0","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19","9":"26","10":"r","11":"subcompact","_rn_":"225"},{"1":"volkswagen","2":"passat","3":"2.0","4":"2008","5":"4","6":"auto(s6)","7":"f","8":"19","9":"28","10":"p","11":"midsize","_rn_":"230"},{"1":"volkswagen","2":"passat","3":"1.8","4":"1999","5":"4","6":"auto(l5)","7":"f","8":"18","9":"29","10":"p","11":"midsize","_rn_":"229"},{"1":"volkswagen","2":"passat","3":"2.8","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"18","9":"26","10":"p","11":"midsize","_rn_":"233"},{"1":"volkswagen","2":"gti","3":"2.8","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"17","9":"24","10":"r","11":"compact","_rn_":"212"},{"1":"volkswagen","2":"jetta","3":"2.8","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"17","9":"24","10":"r","11":"compact","_rn_":"221"},{"1":"volkswagen","2":"passat","3":"3.6","4":"2008","5":"6","6":"auto(s6)","7":"f","8":"17","9":"26","10":"p","11":"midsize","_rn_":"234"},{"1":"volkswagen","2":"jetta","3":"2.8","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"16","9":"23","10":"r","11":"compact","_rn_":"220"},{"1":"volkswagen","2":"passat","3":"2.8","4":"1999","5":"6","6":"auto(l5)","7":"f","8":"16","9":"26","10":"p","11":"midsize","_rn_":"232"},{"1":"chevrolet","2":"malibu","3":"2.4","4":"2008","5":"4","6":"auto(l4)","7":"f","8":"22","9":"30","10":"r","11":"midsize","_rn_":"34"},{"1":"chevrolet","2":"malibu","3":"2.4","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19","9":"27","10":"r","11":"midsize","_rn_":"33"},{"1":"chevrolet","2":"malibu","3":"3.1","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"18","9":"26","10":"r","11":"midsize","_rn_":"35"},{"1":"chevrolet","2":"malibu","3":"3.5","4":"2008","5":"6","6":"auto(l4)","7":"f","8":"18","9":"29","10":"r","11":"midsize","_rn_":"36"},{"1":"chevrolet","2":"malibu","3":"3.6","4":"2008","5":"6","6":"auto(s6)","7":"f","8":"17","9":"26","10":"r","11":"midsize","_rn_":"37"},{"1":"chevrolet","2":"corvette","3":"5.7","4":"1999","5":"8","6":"manual(m6)","7":"r","8":"16","9":"26","10":"p","11":"2seater","_rn_":"24"},{"1":"chevrolet","2":"corvette","3":"6.2","4":"2008","5":"8","6":"manual(m6)","7":"r","8":"16","9":"26","10":"p","11":"2seater","_rn_":"26"},{"1":"chevrolet","2":"corvette","3":"5.7","4":"1999","5":"8","6":"auto(l4)","7":"r","8":"15","9":"23","10":"p","11":"2seater","_rn_":"25"},{"1":"chevrolet","2":"corvette","3":"6.2","4":"2008","5":"8","6":"auto(s6)","7":"r","8":"15","9":"25","10":"p","11":"2seater","_rn_":"27"},{"1":"chevrolet","2":"corvette","3":"7.0","4":"2008","5":"8","6":"manual(m6)","7":"r","8":"15","9":"24","10":"p","11":"2seater","_rn_":"28"},{"1":"chevrolet","2":"c1500 suburban 2wd","3":"5.3","4":"2008","5":"8","6":"auto(l4)","7":"r","8":"14","9":"20","10":"r","11":"suv","_rn_":"19"},{"1":"chevrolet","2":"c1500 suburban 2wd","3":"5.3","4":"2008","5":"8","6":"auto(l4)","7":"r","8":"14","9":"20","10":"r","11":"suv","_rn_":"21"},{"1":"chevrolet","2":"k1500 tahoe 4wd","3":"5.3","4":"2008","5":"8","6":"auto(l4)","7":"4","8":"14","9":"19","10":"r","11":"suv","_rn_":"29"},{"1":"chevrolet","2":"k1500 tahoe 4wd","3":"6.5","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"14","9":"17","10":"d","11":"suv","_rn_":"32"},{"1":"chevrolet","2":"c1500 suburban 2wd","3":"5.7","4":"1999","5":"8","6":"auto(l4)","7":"r","8":"13","9":"17","10":"r","11":"suv","_rn_":"22"},{"1":"chevrolet","2":"c1500 suburban 2wd","3":"6.0","4":"2008","5":"8","6":"auto(l4)","7":"r","8":"12","9":"17","10":"r","11":"suv","_rn_":"23"},{"1":"chevrolet","2":"c1500 suburban 2wd","3":"5.3","4":"2008","5":"8","6":"auto(l4)","7":"r","8":"11","9":"15","10":"e","11":"suv","_rn_":"20"},{"1":"chevrolet","2":"k1500 tahoe 4wd","3":"5.3","4":"2008","5":"8","6":"auto(l4)","7":"4","8":"11","9":"14","10":"e","11":"suv","_rn_":"30"},{"1":"chevrolet","2":"k1500 tahoe 4wd","3":"5.7","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11","9":"15","10":"r","11":"suv","_rn_":"31"},{"1":"dodge","2":"caravan 2wd","3":"2.4","4":"1999","5":"4","6":"auto(l3)","7":"f","8":"18","9":"24","10":"r","11":"minivan","_rn_":"38"},{"1":"dodge","2":"caravan 2wd","3":"3.0","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"17","9":"24","10":"r","11":"minivan","_rn_":"39"},{"1":"dodge","2":"caravan 2wd","3":"3.3","4":"2008","5":"6","6":"auto(l4)","7":"f","8":"17","9":"24","10":"r","11":"minivan","_rn_":"42"},{"1":"dodge","2":"caravan 2wd","3":"3.3","4":"2008","5":"6","6":"auto(l4)","7":"f","8":"17","9":"24","10":"r","11":"minivan","_rn_":"43"},{"1":"dodge","2":"caravan 2wd","3":"3.3","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"16","9":"22","10":"r","11":"minivan","_rn_":"40"},{"1":"dodge","2":"caravan 2wd","3":"3.3","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"16","9":"22","10":"r","11":"minivan","_rn_":"41"},{"1":"dodge","2":"caravan 2wd","3":"3.8","4":"2008","5":"6","6":"auto(l6)","7":"f","8":"16","9":"23","10":"r","11":"minivan","_rn_":"47"},{"1":"dodge","2":"caravan 2wd","3":"4.0","4":"2008","5":"6","6":"auto(l6)","7":"f","8":"16","9":"23","10":"r","11":"minivan","_rn_":"48"},{"1":"dodge","2":"caravan 2wd","3":"3.8","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"15","9":"22","10":"r","11":"minivan","_rn_":"45"},{"1":"dodge","2":"caravan 2wd","3":"3.8","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"15","9":"21","10":"r","11":"minivan","_rn_":"46"},{"1":"dodge","2":"dakota pickup 4wd","3":"3.7","4":"2008","5":"6","6":"manual(m6)","7":"4","8":"15","9":"19","10":"r","11":"pickup","_rn_":"49"},{"1":"dodge","2":"dakota pickup 4wd","3":"3.7","4":"2008","5":"6","6":"auto(l4)","7":"4","8":"14","9":"18","10":"r","11":"pickup","_rn_":"50"},{"1":"dodge","2":"dakota pickup 4wd","3":"3.9","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"14","9":"17","10":"r","11":"pickup","_rn_":"52"},{"1":"dodge","2":"dakota pickup 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"14","9":"19","10":"r","11":"pickup","_rn_":"53"},{"1":"dodge","2":"dakota pickup 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"14","9":"19","10":"r","11":"pickup","_rn_":"54"},{"1":"dodge","2":"dakota pickup 4wd","3":"3.9","4":"1999","5":"6","6":"auto(l4)","7":"4","8":"13","9":"17","10":"r","11":"pickup","_rn_":"51"},{"1":"dodge","2":"durango 4wd","3":"3.9","4":"1999","5":"6","6":"auto(l4)","7":"4","8":"13","9":"17","10":"r","11":"suv","_rn_":"58"},{"1":"dodge","2":"durango 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"13","9":"17","10":"r","11":"suv","_rn_":"59"},{"1":"dodge","2":"durango 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"13","9":"17","10":"r","11":"suv","_rn_":"61"},{"1":"dodge","2":"durango 4wd","3":"5.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"13","9":"18","10":"r","11":"suv","_rn_":"63"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"13","9":"17","10":"r","11":"pickup","_rn_":"67"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"13","9":"17","10":"r","11":"pickup","_rn_":"68"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"5.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"13","9":"17","10":"r","11":"pickup","_rn_":"73"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"manual(m6)","7":"4","8":"12","9":"16","10":"r","11":"pickup","_rn_":"65"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"manual(m6)","7":"4","8":"12","9":"16","10":"r","11":"pickup","_rn_":"69"},{"1":"dodge","2":"caravan 2wd","3":"3.3","4":"2008","5":"6","6":"auto(l4)","7":"f","8":"11","9":"17","10":"e","11":"minivan","_rn_":"44"},{"1":"dodge","2":"dakota pickup 4wd","3":"5.2","4":"1999","5":"8","6":"manual(m5)","7":"4","8":"11","9":"17","10":"r","11":"pickup","_rn_":"56"},{"1":"dodge","2":"dakota pickup 4wd","3":"5.2","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11","9":"15","10":"r","11":"pickup","_rn_":"57"},{"1":"dodge","2":"durango 4wd","3":"5.2","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11","9":"16","10":"r","11":"suv","_rn_":"62"},{"1":"dodge","2":"durango 4wd","3":"5.9","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11","9":"15","10":"r","11":"suv","_rn_":"64"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"5.2","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11","9":"15","10":"r","11":"pickup","_rn_":"71"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"5.2","4":"1999","5":"8","6":"manual(m5)","7":"4","8":"11","9":"16","10":"r","11":"pickup","_rn_":"72"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"5.9","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11","9":"15","10":"r","11":"pickup","_rn_":"74"},{"1":"dodge","2":"dakota pickup 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"9","9":"12","10":"e","11":"pickup","_rn_":"55"},{"1":"dodge","2":"durango 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"9","9":"12","10":"e","11":"suv","_rn_":"60"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"9","9":"12","10":"e","11":"pickup","_rn_":"66"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"manual(m6)","7":"4","8":"9","9":"12","10":"e","11":"pickup","_rn_":"70"},{"1":"ford","2":"mustang","3":"3.8","4":"1999","5":"6","6":"manual(m5)","7":"r","8":"18","9":"26","10":"r","11":"subcompact","_rn_":"91"},{"1":"ford","2":"mustang","3":"3.8","4":"1999","5":"6","6":"auto(l4)","7":"r","8":"18","9":"25","10":"r","11":"subcompact","_rn_":"92"},{"1":"ford","2":"mustang","3":"4.0","4":"2008","5":"6","6":"manual(m5)","7":"r","8":"17","9":"26","10":"r","11":"subcompact","_rn_":"93"},{"1":"ford","2":"mustang","3":"4.0","4":"2008","5":"6","6":"auto(l5)","7":"r","8":"16","9":"24","10":"r","11":"subcompact","_rn_":"94"},{"1":"ford","2":"explorer 4wd","3":"4.0","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"15","9":"19","10":"r","11":"suv","_rn_":"79"},{"1":"ford","2":"mustang","3":"4.6","4":"1999","5":"8","6":"auto(l4)","7":"r","8":"15","9":"21","10":"r","11":"subcompact","_rn_":"95"},{"1":"ford","2":"mustang","3":"4.6","4":"1999","5":"8","6":"manual(m5)","7":"r","8":"15","9":"22","10":"r","11":"subcompact","_rn_":"96"},{"1":"ford","2":"mustang","3":"4.6","4":"2008","5":"8","6":"manual(m5)","7":"r","8":"15","9":"23","10":"r","11":"subcompact","_rn_":"97"},{"1":"ford","2":"mustang","3":"4.6","4":"2008","5":"8","6":"auto(l5)","7":"r","8":"15","9":"22","10":"r","11":"subcompact","_rn_":"98"},{"1":"ford","2":"explorer 4wd","3":"4.0","4":"1999","5":"6","6":"auto(l5)","7":"4","8":"14","9":"17","10":"r","11":"suv","_rn_":"78"},{"1":"ford","2":"explorer 4wd","3":"4.0","4":"1999","5":"6","6":"auto(l5)","7":"4","8":"14","9":"17","10":"r","11":"suv","_rn_":"80"},{"1":"ford","2":"f150 pickup 4wd","3":"4.2","4":"1999","5":"6","6":"auto(l4)","7":"4","8":"14","9":"17","10":"r","11":"pickup","_rn_":"84"},{"1":"ford","2":"f150 pickup 4wd","3":"4.2","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"14","9":"17","10":"r","11":"pickup","_rn_":"85"},{"1":"ford","2":"mustang","3":"5.4","4":"2008","5":"8","6":"manual(m6)","7":"r","8":"14","9":"20","10":"p","11":"subcompact","_rn_":"99"},{"1":"ford","2":"explorer 4wd","3":"4.0","4":"2008","5":"6","6":"auto(l5)","7":"4","8":"13","9":"19","10":"r","11":"suv","_rn_":"81"},{"1":"ford","2":"explorer 4wd","3":"4.6","4":"2008","5":"8","6":"auto(l6)","7":"4","8":"13","9":"19","10":"r","11":"suv","_rn_":"82"},{"1":"ford","2":"explorer 4wd","3":"5.0","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"13","9":"17","10":"r","11":"suv","_rn_":"83"},{"1":"ford","2":"f150 pickup 4wd","3":"4.6","4":"1999","5":"8","6":"manual(m5)","7":"4","8":"13","9":"16","10":"r","11":"pickup","_rn_":"86"},{"1":"ford","2":"f150 pickup 4wd","3":"4.6","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"13","9":"16","10":"r","11":"pickup","_rn_":"87"},{"1":"ford","2":"f150 pickup 4wd","3":"4.6","4":"2008","5":"8","6":"auto(l4)","7":"4","8":"13","9":"17","10":"r","11":"pickup","_rn_":"88"},{"1":"ford","2":"f150 pickup 4wd","3":"5.4","4":"2008","5":"8","6":"auto(l4)","7":"4","8":"13","9":"17","10":"r","11":"pickup","_rn_":"90"},{"1":"ford","2":"expedition 2wd","3":"5.4","4":"2008","5":"8","6":"auto(l6)","7":"r","8":"12","9":"18","10":"r","11":"suv","_rn_":"77"},{"1":"ford","2":"expedition 2wd","3":"4.6","4":"1999","5":"8","6":"auto(l4)","7":"r","8":"11","9":"17","10":"r","11":"suv","_rn_":"75"},{"1":"ford","2":"expedition 2wd","3":"5.4","4":"1999","5":"8","6":"auto(l4)","7":"r","8":"11","9":"17","10":"r","11":"suv","_rn_":"76"},{"1":"ford","2":"f150 pickup 4wd","3":"5.4","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11","9":"15","10":"r","11":"pickup","_rn_":"89"},{"1":"jeep","2":"grand cherokee 4wd","3":"3.0","4":"2008","5":"6","6":"auto(l5)","7":"4","8":"17","9":"22","10":"d","11":"suv","_rn_":"123"},{"1":"jeep","2":"grand cherokee 4wd","3":"3.7","4":"2008","5":"6","6":"auto(l5)","7":"4","8":"15","9":"19","10":"r","11":"suv","_rn_":"124"},{"1":"jeep","2":"grand cherokee 4wd","3":"4.0","4":"1999","5":"6","6":"auto(l4)","7":"4","8":"15","9":"20","10":"r","11":"suv","_rn_":"125"},{"1":"jeep","2":"grand cherokee 4wd","3":"4.7","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"14","9":"17","10":"r","11":"suv","_rn_":"126"},{"1":"jeep","2":"grand cherokee 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"14","9":"19","10":"r","11":"suv","_rn_":"128"},{"1":"jeep","2":"grand cherokee 4wd","3":"5.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"13","9":"18","10":"r","11":"suv","_rn_":"129"},{"1":"jeep","2":"grand cherokee 4wd","3":"6.1","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"11","9":"14","10":"p","11":"suv","_rn_":"130"},{"1":"jeep","2":"grand cherokee 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"9","9":"12","10":"e","11":"suv","_rn_":"127"},{"1":"lincoln","2":"navigator 2wd","3":"5.4","4":"2008","5":"8","6":"auto(l6)","7":"r","8":"12","9":"18","10":"r","11":"suv","_rn_":"137"},{"1":"lincoln","2":"navigator 2wd","3":"5.4","4":"1999","5":"8","6":"auto(l4)","7":"r","8":"11","9":"17","10":"r","11":"suv","_rn_":"135"},{"1":"lincoln","2":"navigator 2wd","3":"5.4","4":"1999","5":"8","6":"auto(l4)","7":"r","8":"11","9":"16","10":"p","11":"suv","_rn_":"136"},{"1":"mercury","2":"mountaineer 4wd","3":"4.0","4":"1999","5":"6","6":"auto(l5)","7":"4","8":"14","9":"17","10":"r","11":"suv","_rn_":"138"},{"1":"mercury","2":"mountaineer 4wd","3":"4.0","4":"2008","5":"6","6":"auto(l5)","7":"4","8":"13","9":"19","10":"r","11":"suv","_rn_":"139"},{"1":"mercury","2":"mountaineer 4wd","3":"4.6","4":"2008","5":"8","6":"auto(l6)","7":"4","8":"13","9":"19","10":"r","11":"suv","_rn_":"140"},{"1":"mercury","2":"mountaineer 4wd","3":"5.0","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"13","9":"17","10":"r","11":"suv","_rn_":"141"},{"1":"pontiac","2":"grand prix","3":"3.1","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"18","9":"26","10":"r","11":"midsize","_rn_":"155"},{"1":"pontiac","2":"grand prix","3":"3.8","4":"2008","5":"6","6":"auto(l4)","7":"f","8":"18","9":"28","10":"r","11":"midsize","_rn_":"158"},{"1":"pontiac","2":"grand prix","3":"3.8","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"17","9":"27","10":"r","11":"midsize","_rn_":"157"},{"1":"pontiac","2":"grand prix","3":"3.8","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"16","9":"26","10":"p","11":"midsize","_rn_":"156"},{"1":"pontiac","2":"grand prix","3":"5.3","4":"2008","5":"8","6":"auto(s4)","7":"f","8":"16","9":"25","10":"p","11":"midsize","_rn_":"159"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

# Regular Expressions

If you've never heard of them, regular expressions (regex) are extremely powerful pattern matching tools used across programming languages. They can be cryptic, but are worth learning.

In R, there are several functions that use them, including "grep()" and "gsub()"

## Using Regex to Subset

The "grep" function is useful for this. We pass a vector to the function with strings we want to search in, and it returns a boolean vector we can use to subset a dataframe.

Let's look for any car with a manual transmission with any number of gears. This wouldn't be easily done using the previous subsetting tools we covered


```r
mpg[grep("manual", mpg$trans) ,]
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["manufacturer"],"name":[1],"type":["fct"],"align":["left"]},{"label":["model"],"name":[2],"type":["fct"],"align":["left"]},{"label":["displ"],"name":[3],"type":["dbl"],"align":["right"]},{"label":["year"],"name":[4],"type":["int"],"align":["right"]},{"label":["cyl"],"name":[5],"type":["int"],"align":["right"]},{"label":["trans"],"name":[6],"type":["chr"],"align":["left"]},{"label":["drv"],"name":[7],"type":["chr"],"align":["left"]},{"label":["cty"],"name":[8],"type":["int"],"align":["right"]},{"label":["hwy"],"name":[9],"type":["int"],"align":["right"]},{"label":["fl"],"name":[10],"type":["chr"],"align":["left"]},{"label":["class"],"name":[11],"type":["chr"],"align":["left"]}],"data":[{"1":"audi","2":"a4","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"p","11":"compact","_rn_":"2"},{"1":"audi","2":"a4","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"20","9":"31","10":"p","11":"compact","_rn_":"3"},{"1":"audi","2":"a4","3":"2.8","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"18","9":"26","10":"p","11":"compact","_rn_":"6"},{"1":"audi","2":"a4 quattro","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"18","9":"26","10":"p","11":"compact","_rn_":"8"},{"1":"audi","2":"a4 quattro","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"4","8":"20","9":"28","10":"p","11":"compact","_rn_":"10"},{"1":"audi","2":"a4 quattro","3":"2.8","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"17","9":"25","10":"p","11":"compact","_rn_":"13"},{"1":"audi","2":"a4 quattro","3":"3.1","4":"2008","5":"6","6":"manual(m6)","7":"4","8":"15","9":"25","10":"p","11":"compact","_rn_":"15"},{"1":"chevrolet","2":"corvette","3":"5.7","4":"1999","5":"8","6":"manual(m6)","7":"r","8":"16","9":"26","10":"p","11":"2seater","_rn_":"24"},{"1":"chevrolet","2":"corvette","3":"6.2","4":"2008","5":"8","6":"manual(m6)","7":"r","8":"16","9":"26","10":"p","11":"2seater","_rn_":"26"},{"1":"chevrolet","2":"corvette","3":"7.0","4":"2008","5":"8","6":"manual(m6)","7":"r","8":"15","9":"24","10":"p","11":"2seater","_rn_":"28"},{"1":"dodge","2":"dakota pickup 4wd","3":"3.7","4":"2008","5":"6","6":"manual(m6)","7":"4","8":"15","9":"19","10":"r","11":"pickup","_rn_":"49"},{"1":"dodge","2":"dakota pickup 4wd","3":"3.9","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"14","9":"17","10":"r","11":"pickup","_rn_":"52"},{"1":"dodge","2":"dakota pickup 4wd","3":"5.2","4":"1999","5":"8","6":"manual(m5)","7":"4","8":"11","9":"17","10":"r","11":"pickup","_rn_":"56"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"manual(m6)","7":"4","8":"12","9":"16","10":"r","11":"pickup","_rn_":"65"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"manual(m6)","7":"4","8":"12","9":"16","10":"r","11":"pickup","_rn_":"69"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"manual(m6)","7":"4","8":"9","9":"12","10":"e","11":"pickup","_rn_":"70"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"5.2","4":"1999","5":"8","6":"manual(m5)","7":"4","8":"11","9":"16","10":"r","11":"pickup","_rn_":"72"},{"1":"ford","2":"explorer 4wd","3":"4.0","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"15","9":"19","10":"r","11":"suv","_rn_":"79"},{"1":"ford","2":"f150 pickup 4wd","3":"4.2","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"14","9":"17","10":"r","11":"pickup","_rn_":"85"},{"1":"ford","2":"f150 pickup 4wd","3":"4.6","4":"1999","5":"8","6":"manual(m5)","7":"4","8":"13","9":"16","10":"r","11":"pickup","_rn_":"86"},{"1":"ford","2":"mustang","3":"3.8","4":"1999","5":"6","6":"manual(m5)","7":"r","8":"18","9":"26","10":"r","11":"subcompact","_rn_":"91"},{"1":"ford","2":"mustang","3":"4.0","4":"2008","5":"6","6":"manual(m5)","7":"r","8":"17","9":"26","10":"r","11":"subcompact","_rn_":"93"},{"1":"ford","2":"mustang","3":"4.6","4":"1999","5":"8","6":"manual(m5)","7":"r","8":"15","9":"22","10":"r","11":"subcompact","_rn_":"96"},{"1":"ford","2":"mustang","3":"4.6","4":"2008","5":"8","6":"manual(m5)","7":"r","8":"15","9":"23","10":"r","11":"subcompact","_rn_":"97"},{"1":"ford","2":"mustang","3":"5.4","4":"2008","5":"8","6":"manual(m6)","7":"r","8":"14","9":"20","10":"p","11":"subcompact","_rn_":"99"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"28","9":"33","10":"r","11":"subcompact","_rn_":"100"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"25","9":"32","10":"r","11":"subcompact","_rn_":"102"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"23","9":"29","10":"p","11":"subcompact","_rn_":"103"},{"1":"honda","2":"civic","3":"1.8","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"26","9":"34","10":"r","11":"subcompact","_rn_":"105"},{"1":"honda","2":"civic","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"21","9":"29","10":"p","11":"subcompact","_rn_":"108"},{"1":"hyundai","2":"sonata","3":"2.4","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"18","9":"27","10":"r","11":"midsize","_rn_":"110"},{"1":"hyundai","2":"sonata","3":"2.4","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"21","9":"31","10":"r","11":"midsize","_rn_":"112"},{"1":"hyundai","2":"sonata","3":"2.5","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"18","9":"26","10":"r","11":"midsize","_rn_":"114"},{"1":"hyundai","2":"tiburon","3":"2.0","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"19","9":"29","10":"r","11":"subcompact","_rn_":"117"},{"1":"hyundai","2":"tiburon","3":"2.0","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"20","9":"28","10":"r","11":"subcompact","_rn_":"118"},{"1":"hyundai","2":"tiburon","3":"2.7","4":"2008","5":"6","6":"manual(m6)","7":"f","8":"16","9":"24","10":"r","11":"subcompact","_rn_":"121"},{"1":"hyundai","2":"tiburon","3":"2.7","4":"2008","5":"6","6":"manual(m5)","7":"f","8":"17","9":"24","10":"r","11":"subcompact","_rn_":"122"},{"1":"nissan","2":"altima","3":"2.4","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"142"},{"1":"nissan","2":"altima","3":"2.5","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"23","9":"32","10":"r","11":"midsize","_rn_":"145"},{"1":"nissan","2":"altima","3":"3.5","4":"2008","5":"6","6":"manual(m6)","7":"f","8":"19","9":"27","10":"p","11":"midsize","_rn_":"146"},{"1":"nissan","2":"maxima","3":"3.0","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"19","9":"25","10":"r","11":"midsize","_rn_":"149"},{"1":"nissan","2":"pathfinder 4wd","3":"3.3","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"15","9":"17","10":"r","11":"suv","_rn_":"152"},{"1":"subaru","2":"forester awd","3":"2.5","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"18","9":"25","10":"r","11":"suv","_rn_":"160"},{"1":"subaru","2":"forester awd","3":"2.5","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"20","9":"27","10":"r","11":"suv","_rn_":"162"},{"1":"subaru","2":"forester awd","3":"2.5","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"19","9":"25","10":"p","11":"suv","_rn_":"163"},{"1":"subaru","2":"impreza awd","3":"2.2","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"19","9":"26","10":"r","11":"subcompact","_rn_":"167"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"19","9":"26","10":"r","11":"subcompact","_rn_":"168"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"19","9":"25","10":"p","11":"compact","_rn_":"172"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"20","9":"27","10":"r","11":"compact","_rn_":"173"},{"1":"toyota","2":"4runner 4wd","3":"2.7","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"15","9":"20","10":"r","11":"suv","_rn_":"174"},{"1":"toyota","2":"4runner 4wd","3":"3.4","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"15","9":"17","10":"r","11":"suv","_rn_":"177"},{"1":"toyota","2":"camry","3":"2.2","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"midsize","_rn_":"180"},{"1":"toyota","2":"camry","3":"2.4","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"21","9":"31","10":"r","11":"midsize","_rn_":"182"},{"1":"toyota","2":"camry","3":"3.0","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"18","9":"26","10":"r","11":"midsize","_rn_":"185"},{"1":"toyota","2":"camry solara","3":"2.2","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"188"},{"1":"toyota","2":"camry solara","3":"2.4","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"21","9":"31","10":"r","11":"compact","_rn_":"189"},{"1":"toyota","2":"camry solara","3":"3.0","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"18","9":"26","10":"r","11":"compact","_rn_":"192"},{"1":"toyota","2":"corolla","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"26","9":"35","10":"r","11":"compact","_rn_":"196"},{"1":"toyota","2":"corolla","3":"1.8","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"28","9":"37","10":"r","11":"compact","_rn_":"197"},{"1":"toyota","2":"toyota tacoma 4wd","3":"2.7","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"15","9":"20","10":"r","11":"pickup","_rn_":"201"},{"1":"toyota","2":"toyota tacoma 4wd","3":"2.7","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"17","9":"22","10":"r","11":"pickup","_rn_":"203"},{"1":"toyota","2":"toyota tacoma 4wd","3":"3.4","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"15","9":"17","10":"r","11":"pickup","_rn_":"204"},{"1":"toyota","2":"toyota tacoma 4wd","3":"4.0","4":"2008","5":"6","6":"manual(m6)","7":"4","8":"15","9":"18","10":"r","11":"pickup","_rn_":"206"},{"1":"volkswagen","2":"gti","3":"2.0","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"208"},{"1":"volkswagen","2":"gti","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"21","9":"29","10":"p","11":"compact","_rn_":"210"},{"1":"volkswagen","2":"gti","3":"2.8","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"17","9":"24","10":"r","11":"compact","_rn_":"212"},{"1":"volkswagen","2":"jetta","3":"1.9","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"33","9":"44","10":"d","11":"compact","_rn_":"213"},{"1":"volkswagen","2":"jetta","3":"2.0","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"214"},{"1":"volkswagen","2":"jetta","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"21","9":"29","10":"p","11":"compact","_rn_":"217"},{"1":"volkswagen","2":"jetta","3":"2.5","4":"2008","5":"5","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"compact","_rn_":"219"},{"1":"volkswagen","2":"jetta","3":"2.8","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"17","9":"24","10":"r","11":"compact","_rn_":"221"},{"1":"volkswagen","2":"new beetle","3":"1.9","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"35","9":"44","10":"d","11":"subcompact","_rn_":"222"},{"1":"volkswagen","2":"new beetle","3":"2.0","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"r","11":"subcompact","_rn_":"224"},{"1":"volkswagen","2":"new beetle","3":"2.5","4":"2008","5":"5","6":"manual(m5)","7":"f","8":"20","9":"28","10":"r","11":"subcompact","_rn_":"226"},{"1":"volkswagen","2":"passat","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"21","9":"29","10":"p","11":"midsize","_rn_":"228"},{"1":"volkswagen","2":"passat","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"21","9":"29","10":"p","11":"midsize","_rn_":"231"},{"1":"volkswagen","2":"passat","3":"2.8","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"18","9":"26","10":"p","11":"midsize","_rn_":"233"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

## Using Regex to Derive a New Variable

gsub() is a function that substitutes character strings using regular expressions.

Let's say we want to create a new variable for the sub-type of transmission (including the number of gears that a transmission has), regardless of whether it's an automatic or manual. One way is to use gsub() to delete the characters we don't want.

### Simple Example

Let's first delete any parentheses from the transmission variable to see how gsub() works

The first argment is the regular expression pattern in single quotes we want to replace, the second is the pattern we want to replace it with in single quotes, and the third argument is the input. The square brackets act like an "or" operator, so it will match any of the characters in the bracket. One tricky thing about regular expressions is the idea of "escaping." Because there are special characters in regex (parentheses is one), we need to specify that we want to use the "standard" meaning of the character within the pattern by putting a backslash in front of it. For technical details, in R on Windows, you actually need to double escape using two consecutive backslashes. We specify that we want to replace the parentheses with nothing using blank quotes, effectively deleting them. Finally, since the variable is long, let's just look at the first couple of items in the output using the head() function.


```r
head(mpg$trans)
```

```
## [1] "auto(l5)"   "manual(m5)" "manual(m6)" "auto(av)"   "auto(l5)"  
## [6] "manual(m5)"
```

```r
head(gsub('[\\(\\)]', '', mpg$trans))
```

```
## [1] "autol5"   "manualm5" "manualm6" "autoav"   "autol5"   "manualm5"
```

### Deriving a Variable for Number of Gears in Transmission

Now, let's isolate the number of gears by removing all other characters. One nice thing about regex is that you can specify a range of values, including letters. Here, we are searching for any lowercase letter from a to z and also any opening or closing parethesis, and replacing them with nothing -- this should leave us with the numeric value for the number of gears.


```r
head(gsub('[a-z\\(\\)]+', '', mpg$trans))
```

```
## [1] "5" "5" "6" ""  "5" "5"
```

```r
#Save the resulting vector to our dataset as a column called "gears"
mpg$gears <- gsub('[a-z\\(\\)]+', '', mpg$trans)
```

We can see that we have a problem -- there are some transmissions without a numeric value for number of gears (so-called continuously-variable transmissions).

### Using Subsetting to Assign New Values

Previously, we were subsetting to extract and/or view rows or columns of data, but we can also assign new values to subsets of data. In this case, we know that blank rows corresponding to transmissions with no gears, so we can subset those and change their value to NA.


```r
mpg$gears[mpg$gears == ""] <- NA

head(mpg$gears)
```

```
## [1] "5" "5" "6" NA  "5" "5"
```


# Dates

## Simple Examples

R can handle dates pretty well, but it can sometimes be a pain because of the number of different date formats that exist.

First, let's make some date character strings to work with


```r
dates1 <- c("2021-05-18", "2020-04-12", "2007-05-21")

dates2 <- c("05/18/2021", "04/12/2020", "05/21/2007")
```

First try the easiest method of as.Date()


```r
dates1.date <- as.Date(dates1)
```

We find that as.Date() works for the first set of dates . . .


```r
dates1.date[1] - dates1.date[2]
```

```
## Time difference of 401 days
```


```r
# dates2.date <- as.Date(dates2)   # <-- gives an error: "Error in charToDate(x) : character string is not in a standard unambiguous format"
```

. . . but not for the second set of dates, despite being in a fairly common format. We need to tell R what format these strings are in using a format type, which can be found in the help file of the strptime() function that is running under the hood. In this scenario, we have to define our delimiters as "/" as below:


```r
dates2.date <- as.Date(dates2, format = "%m/%d/%Y")

dates2.date[1] - dates2.date[2]
```

```
## Time difference of 401 days
```

## More Challenging Examples

Let's make a more challenging example to load as a date


```r
dates3 <- c("3/2/2021 1:00:00 PM", "3/19/2021 4:30:00 PM")
```

Let's try as.Date but it's probably going to fail


```r
dates3.date <- as.Date(dates3)
dates3.date
```

```
## [1] "0003-02-20" NA
```

It didn't give an error, but it also didn't really work. We'll need to use the formats from strptime() again, this time using %I:%M:%S for hour:minute:second and %p for "AM/PM"


```r
dates3.date <- as.Date(dates3, format = "%m/%d/%Y %I:%M:%S %p")

dates3.date[2] - dates3.date[1]
```

```
## Time difference of 17 days
```

## strptime()

Notice the limitation of as.Date() above -- it "rounds" the date to the nearest day. We can use strptime() to save a more accurate version of these dates.


```r
dates3.strptime <- strptime(dates3, format = "%m/%d/%Y %I:%M:%S %p")

dates3.strptime[2] - dates3.strptime[1]
```

```
## Time difference of 17.10417 days
```

# Reshaping Datasets

There are several tools for reshaping datasets from wide to long format. I will focus on the 'reshape2' package, which is pretty intuitive.

## To Long Format

Let's say that we want a single column for MPG values with another column to indicate city vs highway. This is often the format needed for modeling and for plotting in ggplot (we'll cover that later).

We specify city and highway MPG as the "measure" variables (whose values we want to stack vertically) and other variables of interest as "ID" variables which will be separate columns to help make sense of the output dataset (these aren't actually required in this scenario)


```r
mpg.cty.hwy <- melt(mpg, id.vars = c("manufacturer", "model", "year", "cyl", "trans"), measure.vars = c("cty", "hwy"), value.name = "mpg", variable.name = "mpg.type")

head(mpg.cty.hwy)
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["manufacturer"],"name":[1],"type":["fct"],"align":["left"]},{"label":["model"],"name":[2],"type":["fct"],"align":["left"]},{"label":["year"],"name":[3],"type":["int"],"align":["right"]},{"label":["cyl"],"name":[4],"type":["int"],"align":["right"]},{"label":["trans"],"name":[5],"type":["chr"],"align":["left"]},{"label":["mpg.type"],"name":[6],"type":["fct"],"align":["left"]},{"label":["mpg"],"name":[7],"type":["int"],"align":["right"]}],"data":[{"1":"audi","2":"a4","3":"1999","4":"4","5":"auto(l5)","6":"cty","7":"18","_rn_":"1"},{"1":"audi","2":"a4","3":"1999","4":"4","5":"manual(m5)","6":"cty","7":"21","_rn_":"2"},{"1":"audi","2":"a4","3":"2008","4":"4","5":"manual(m6)","6":"cty","7":"20","_rn_":"3"},{"1":"audi","2":"a4","3":"2008","4":"4","5":"auto(av)","6":"cty","7":"21","_rn_":"4"},{"1":"audi","2":"a4","3":"1999","4":"6","5":"auto(l5)","6":"cty","7":"16","_rn_":"5"},{"1":"audi","2":"a4","3":"1999","4":"6","5":"manual(m5)","6":"cty","7":"18","_rn_":"6"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

The output should have 2x as many rows as the input dataframe -- check that.


```r
nrow(mpg.cty.hwy) / nrow(mpg)
```

```
## [1] 2
```

## Long to Wide

Working off of the MPG dataset, let's make a wide version with year on the columns and city MPG in the cells.

The first step is to create a molten dataframe. Here, we want city MPG as the measure/value variable and we also keep all other identifying variables as "ID" variables.


```r
mpg.melt <- melt(mpg, measure.vars = "cty", id.vars = c("manufacturer", "model", "displ", "cyl", "trans", "drv", "fl", "class", "year"))

head(mpg.melt)
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["manufacturer"],"name":[1],"type":["fct"],"align":["left"]},{"label":["model"],"name":[2],"type":["fct"],"align":["left"]},{"label":["displ"],"name":[3],"type":["dbl"],"align":["right"]},{"label":["cyl"],"name":[4],"type":["int"],"align":["right"]},{"label":["trans"],"name":[5],"type":["chr"],"align":["left"]},{"label":["drv"],"name":[6],"type":["chr"],"align":["left"]},{"label":["fl"],"name":[7],"type":["chr"],"align":["left"]},{"label":["class"],"name":[8],"type":["chr"],"align":["left"]},{"label":["year"],"name":[9],"type":["int"],"align":["right"]},{"label":["variable"],"name":[10],"type":["fct"],"align":["left"]},{"label":["value"],"name":[11],"type":["int"],"align":["right"]}],"data":[{"1":"audi","2":"a4","3":"1.8","4":"4","5":"auto(l5)","6":"f","7":"p","8":"compact","9":"1999","10":"cty","11":"18","_rn_":"1"},{"1":"audi","2":"a4","3":"1.8","4":"4","5":"manual(m5)","6":"f","7":"p","8":"compact","9":"1999","10":"cty","11":"21","_rn_":"2"},{"1":"audi","2":"a4","3":"2.0","4":"4","5":"manual(m6)","6":"f","7":"p","8":"compact","9":"2008","10":"cty","11":"20","_rn_":"3"},{"1":"audi","2":"a4","3":"2.0","4":"4","5":"auto(av)","6":"f","7":"p","8":"compact","9":"2008","10":"cty","11":"21","_rn_":"4"},{"1":"audi","2":"a4","3":"2.8","4":"6","5":"auto(l5)","6":"f","7":"p","8":"compact","9":"1999","10":"cty","11":"16","_rn_":"5"},{"1":"audi","2":"a4","3":"2.8","4":"6","5":"manual(m5)","6":"f","7":"p","8":"compact","9":"1999","10":"cty","11":"18","_rn_":"6"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

Now we can cast the molten (long) format into a dataframe with our desired shape -- anything to the right of the \~ will be on the columns of the dataframe. The measure variable is implicitly city MPG because that's how we created the molten dataframe. Note that we need to use an aggregate function to deal with repeated values in case there are any (which there are) -- we choose to simply average their values. If there weren't repeated values, this would give us a clean wide dataframe.


```r
mpg.year.wide <- dcast(mpg.melt, manufacturer + model + displ + cyl + trans + drv + fl + class ~ year, fun.aggregate = mean)

head(mpg.year.wide)
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["manufacturer"],"name":[1],"type":["fct"],"align":["left"]},{"label":["model"],"name":[2],"type":["fct"],"align":["left"]},{"label":["displ"],"name":[3],"type":["dbl"],"align":["right"]},{"label":["cyl"],"name":[4],"type":["int"],"align":["right"]},{"label":["trans"],"name":[5],"type":["chr"],"align":["left"]},{"label":["drv"],"name":[6],"type":["chr"],"align":["left"]},{"label":["fl"],"name":[7],"type":["chr"],"align":["left"]},{"label":["class"],"name":[8],"type":["chr"],"align":["left"]},{"label":["1999"],"name":[9],"type":["dbl"],"align":["right"]},{"label":["2008"],"name":[10],"type":["dbl"],"align":["right"]}],"data":[{"1":"honda","2":"civic","3":"1.6","4":"4","5":"auto(l4)","6":"f","7":"r","8":"subcompact","9":"24.0","10":"NaN","_rn_":"1"},{"1":"honda","2":"civic","3":"1.6","4":"4","5":"manual(m5)","6":"f","7":"p","8":"subcompact","9":"23.0","10":"NaN","_rn_":"2"},{"1":"honda","2":"civic","3":"1.6","4":"4","5":"manual(m5)","6":"f","7":"r","8":"subcompact","9":"26.5","10":"NaN","_rn_":"3"},{"1":"honda","2":"civic","3":"1.8","4":"4","5":"auto(l5)","6":"f","7":"c","8":"subcompact","9":"NaN","10":"24","_rn_":"4"},{"1":"honda","2":"civic","3":"1.8","4":"4","5":"auto(l5)","6":"f","7":"r","8":"subcompact","9":"NaN","10":"25","_rn_":"5"},{"1":"honda","2":"civic","3":"1.8","4":"4","5":"manual(m5)","6":"f","7":"r","8":"subcompact","9":"NaN","10":"26","_rn_":"6"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

We could also accomplish the same task with one line using recast()


```r
mpg.year.wide2 <- recast(mpg, manufacturer + model + displ + cyl + trans + drv + fl + class ~ year, measure.var = "cty", fun.aggregate = mean)

identical(mpg.year.wide, mpg.year.wide2)
```

```
## [1] TRUE
```

# Figures: ggplot2

ggplot2 is a very nice plotting tool and very widely used.

## qplot

qplot is the fastest way to make a plot in ggplot2 -- it's mostly meant for quick checks, but not for final polished figures (as you can see below)

Let's make a scatterplot of displacement vs city MPG


```r
qplot(displ, cty, data = mpg)
```

![](BCTIP_R_Session_I_files/figure-html/unnamed-chunk-54-1.png)<!-- -->

A boxplot of city MPG among manufacturers


```r
qplot(manufacturer, cty, geom = "boxplot", data = mpg)
```

![](BCTIP_R_Session_I_files/figure-html/unnamed-chunk-55-1.png)<!-- -->

We can see that these figures could use some improvements -- that will be our next topic.

## Polished figures with ggplot() function

For more polished figures in ggplot2, the best method is the generic ggplot() function. When building plots, ggplot2 takes a layered approach -- different components are built on top of each other graphically and this is reflected in both the code structure and visually in how objects are overlayed.

There are simply too many options in ggplot to cover comprehensively, so I'll try to cover the most common and useful options that I've encountered in the examples below.

### Polished scatterplot

For the plot below, we use the following options:

ggplot() builds the base figure (axes and panel) and sets the "aesthetic" variables which will be shown visually.

geom_point() builds the points on top of the base figure and inherets the aesthetics. We specify size = 1.2 to make the dots smaller than default. We don't include it in aes() to avoid creating a legend.

scale_x\_continuous() allows for customization of a continuous x axis -- here we're setting the breaks for the labels to be every 0.25 L

theme_bw() makes the overall plot have a black and white look, good for publications

theme() allows customization of many different aspects of the plot -- here, we're setting the x-axis text to be size 8 (smaller than default) and getting rid of the minor grid lines that occur between two x-axis ticks

xlab() and ylab() set the axis labels

labs(title = "") creates a nice looking title and also has the option for a subtitle if needed

Note that the plot is being saved as an object for later use, but also the whole block of code is wrapped in (), which also displays the output of the code.


```r
(displ.cty.scatter <- ggplot(mpg, aes(x=displ, y=cty))+
  geom_point(size = 1.2)+
  scale_x_continuous(
    breaks = seq.default(from=0, to=10, by=0.25)
  ) +
  theme_bw()+
  theme(
    axis.text.x = element_text(size=8),
    panel.grid.minor.x = element_blank()
  ) +
  xlab("Engine displacement (L)")+
  ylab("City MPG")+
  labs(title = "Engine Displacement vs. City MPG")
)
```

![](BCTIP_R_Session_I_files/figure-html/unnamed-chunk-56-1.png)<!-- -->

Add a linear regression line with geom_smooth()


```r
(displ.cty.scatter <- ggplot(mpg, aes(x=displ, y=cty))+
  geom_point(size = 1.2)+
  scale_x_continuous(
    breaks = seq.default(from=0, to=10, by=0.25)
  ) +
  theme_bw()+
  theme(
    axis.text.x = element_text(size=8),
    panel.grid.minor.x = element_blank()
  ) +
  xlab("Engine displacement (L)")+
  ylab("City MPG")+
  labs(title = "Engine Displacement vs. City MPG")+
  geom_smooth(method = "lm")
)
```

```
## `geom_smooth()` using formula 'y ~ x'
```

![](BCTIP_R_Session_I_files/figure-html/unnamed-chunk-57-1.png)<!-- -->

Now, let's save the plot to our local machine with ggsave() -- there are options for PDF, JPG, PNG, and more.


```r
ggsave(displ.cty.scatter, file = "C:\\Users\\ts415\\Documents\\displ.cty.scatter.pdf", width=8, height=5)
```

```
## `geom_smooth()` using formula 'y ~ x'
```

```r
ggsave(displ.cty.scatter, file = "C:\\Users\\ts415\\Documents\\displ.cty.scatter.jpg", width=8, height=5)
```

```
## `geom_smooth()` using formula 'y ~ x'
```

```r
ggsave(displ.cty.scatter, file = "C:\\Users\\ts415\\Documents\\displ.cty.scatter.png", width=8, height=5)
```

```
## `geom_smooth()` using formula 'y ~ x'
```

#### Adding Complexity

Let's add some complexity by stratifying the plot by both year and manufacturer. We'll start with the same base code, but include:

geom_point(aes(color = as.factor(year))) to differentiate years by color and add a lengend. Note that year wasn't considered a factor, so we have to convert it on the fly within the call to geom_point(). This results in the label in the legend including "as.factor(year)", so we'll manually change that below with scale_color_discrete(name = "Year").

breaks = seq.default(from=0, to=10, by=0.5) to make the x-axis ticks every 0.5

scale_color_discrete(name = "Year") to manually change the title of the legend for the color aesthetic.

axis.text.x = element_text(size=6, angle=90, hjust=1, vjust=0.4) to turn the axis text sideways and line it up with the tick marks

facet_wrap(\~ manufacturer) to make separate panels by manufacturer

labs(subtitle = "by Year and by Manufacturer") to add a subtitle to reflect the updated plot


```r
(displ.cty.scatter2 <- ggplot(mpg, aes(x=displ, y=cty))+
  geom_point(aes(color = as.factor(year)), size = 1.2)+
  scale_x_continuous(
    breaks = seq.default(from=0, to=10, by=0.5)
  ) +
  scale_color_discrete(
    name = "Year"
  )+
  theme_bw()+
  theme(
    axis.text.x = element_text(size=6, angle=90, hjust=1, vjust=0.4),
    panel.grid.minor.x = element_blank()
  ) +
  xlab("Engine displacement (L)")+
  ylab("City MPG")+
  labs(title = "Engine Displacement vs. City MPG by Year", subtitle = "by Year and by Manufacturer")+
  facet_wrap(~ manufacturer)
)
```

![](BCTIP_R_Session_I_files/figure-html/unnamed-chunk-59-1.png)<!-- -->

Save the plot as a PDF


```r
ggsave(displ.cty.scatter2, file = "C:\\Users\\ts415\\Documents\\displ.cty.scatter2.pdf", width=8, height=5)
```

### Polished Boxplots

Making polished boxplots is similar to the polished scatterplots above, except we use geom_boxplot() instead of geom_point(). We also make some other changes:

Make the color aesthetic in geom_boxplot vary by 'cyl' for number of cylinders -- convert this variable to a factor from numeric on the fly.

Change the title of the legend, the axis labels, and the plot titles

Remove 'scale_x\_continuous' because we no longer need to define the breaks on a numeric X axis

Use axis.text.x = element_text(size=9, angle=90, hjust=1, vjust=0.4) to rotate the x-axis text by 90 degrees and align it to the tick marks.


```r
ggplot(mpg, aes(x = manufacturer, y = cty))+
  geom_boxplot(aes(color = as.factor(cyl)))+
  scale_color_discrete(
    name = "Number of\nCylinders"
  )+
  theme_bw()+
  theme(
    axis.text.x = element_text(size=9, angle=90, hjust=1, vjust=0.4),
    panel.grid.minor.x = element_blank()
  ) +
  xlab("Manufacturer")+
  ylab("City MPG")+
  labs(title = "City MPG by Manufacturer", subtitle = "and by Number of Cylinders")
```

![](BCTIP_R_Session_I_files/figure-html/unnamed-chunk-61-1.png)<!-- -->

# Feedback From Homework

## Be Careful with Logical Operators

Make sure you think carefully about how R is interpreting your combination of logical operators. 

- is.na() returns a vector of boolean values where missing values receive a value of TRUE
- ! finds the complement, effectively flipping all of the boolean values in the vector
- & finds the intersection
- | finds the union


```r
a = c(1, 2, NA)
b = c(NA, 2, 3)

is.na(a)
```

```
## [1] FALSE FALSE  TRUE
```

```r
is.na(b)
```

```
## [1]  TRUE FALSE FALSE
```

```r
is.na(a) | is.na(b)
```

```
## [1]  TRUE FALSE  TRUE
```

```r
is.na(a) & is.na(b)
```

```
## [1] FALSE FALSE FALSE
```

```r
!is.na(a) & !is.na(b)
```

```
## [1] FALSE  TRUE FALSE
```

```r
!(is.na(a) | is.na(b))
```

```
## [1] FALSE  TRUE FALSE
```


```r
temp <- data.frame(
          a = c(1, 2, NA),
          b = c(NA, 2, 3)
)

#Usage of OR operator that does not properly remove NAs
temp[!is.na(temp$a) | !is.na(temp$b) ,]
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["a"],"name":[1],"type":["dbl"],"align":["right"]},{"label":["b"],"name":[2],"type":["dbl"],"align":["right"]}],"data":[{"1":"1","2":"NA","_rn_":"1"},{"1":"2","2":"2","_rn_":"2"},{"1":"NA","2":"3","_rn_":"3"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

```r
#AND operator does properly remove NAs
#This finds rows where either a or b is not missing, which is actually all rows
temp[!is.na(temp$a) & !is.na(temp$b) ,]
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["a"],"name":[1],"type":["dbl"],"align":["right"]},{"label":["b"],"name":[2],"type":["dbl"],"align":["right"]}],"data":[{"1":"2","2":"2","_rn_":"2"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

```r
#Wrapping the entire OR conditional statement in the ! operator correctly removes NAs
temp[!(is.na(temp$a) | is.na(temp$b)) ,]
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["a"],"name":[1],"type":["dbl"],"align":["right"]},{"label":["b"],"name":[2],"type":["dbl"],"align":["right"]}],"data":[{"1":"2","2":"2","_rn_":"2"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

## Tidyverse & Base R

For those who used Tidyverse tools and syntax, I would encourage you to try to solve the HW problems with only 'base R.' 

# Looping (and not looping)

## Not Looping

You may have heard this before, but most functions in R are "vectorized" and R is relatively slow at performing for loops, so try to avoid them. Let's walk through some non-loop methods to accomplish common tasks.

### Operations on Columns of a Dataframe

This is the most obvious thing NOT to do in R using a loop. Let's derive a new variable for displacement (volume) per cylinder in the engine.


```r
mpg$vol.per.cyl <- mpg$displ / mpg$cyl
```

### Operations on Rows Using Conditional Logic

This may be a tempting task to accomplish with a for loop, but an alternative is using the subsetting methods above.

Let's increase MPG of city driving ONLY for manual transmissions by 10%. You could write a for loop to check the condition of each item, then perform the operation, but subsetting is much faster.

First let's use conditional subsetting to find the city MPG for all cars with manual transmissions using regular expressions


```r
mpg$cty[grep('manual', mpg$trans)]
```

```
##  [1] 21 20 18 18 20 17 15 16 16 15 15 14 11 12 12  9 11 15 14 13 18 17 15 15 14
## [26] 28 25 23 26 21 18 21 18 19 20 16 17 21 23 19 19 15 18 20 19 19 19 19 20 15
## [51] 15 21 21 18 21 21 18 26 28 15 17 15 15 21 21 17 33 21 21 21 17 35 21 20 21
## [76] 21 18
```

Now we can build on this code by reassigning/overwriting their values with 10% increased values


```r
mpg$cty[grep('manual', mpg$trans)] <- mpg$cty[grep('manual', mpg$trans)]*1.1
```

Check the results

```r
mpg$cty[grep('manual', mpg$trans)]
```

```
##  [1] 23.1 22.0 19.8 19.8 22.0 18.7 16.5 17.6 17.6 16.5 16.5 15.4 12.1 13.2 13.2
## [16]  9.9 12.1 16.5 15.4 14.3 19.8 18.7 16.5 16.5 15.4 30.8 27.5 25.3 28.6 23.1
## [31] 19.8 23.1 19.8 20.9 22.0 17.6 18.7 23.1 25.3 20.9 20.9 16.5 19.8 22.0 20.9
## [46] 20.9 20.9 20.9 22.0 16.5 16.5 23.1 23.1 19.8 23.1 23.1 19.8 28.6 30.8 16.5
## [61] 18.7 16.5 16.5 23.1 23.1 18.7 36.3 23.1 23.1 23.1 18.7 38.5 23.1 22.0 23.1
## [76] 23.1 19.8
```


## Looping

If you MUST loop in R, then you should know there are faster and slower, as well as verbose and less verbose, ways of doing so.

In general, using for() is both slow and verbose, but that makes it easier to begin learning.

### for

A for loop has two main parts: 1) A set of values on which to loop and 2) An execution block that says what to do for each value. An object must be initialized outside of the loop to save the loop output to. It's not a problem if the initial length of the container doesn't match the number of iterations of the loop. To save the output, you typically use the [] to assign the loop output to an index in the output container.

Here's a contrived loop example


```r
#Initialize the output container as a single NA value
output <- NA

#Save the ith value of i to the ith index of output
for (i in 1:10) {
  output[i] <- i
}

output
```

```
##  [1]  1  2  3  4  5  6  7  8  9 10
```

A more useful example would be to loop through the car manufacturers and calculate their mean city MPG. Let's try that.


```r
#Initialize the output container
manufac.mpg <- NA

#Create values for i that range from 1 to the number of unique manufacturers
for (i in 1:length(unique(mpg$manufacturer))) {
# for (i in seq(unique(mpg$manufacturer))) {      # <- A less verbose method of generating indices
  
  #Subset the city MPG variable to only include values for the ith unique manufacturer, and find the mean of those values. Save this in the ith index of our container object.
  manufac.mpg[i] <- mean(mpg$cty[mpg$manufacturer == unique(mpg$manufacturer)[i]])
}

#We have to assign the manufacturer names by hand using the same order as in the loop
names(manufac.mpg) <- unique(mpg$manufacturer)

manufac.mpg
```

```
##       audi  chevrolet      dodge       ford      honda    hyundai       jeep 
##   18.32778   15.24737   13.36216   14.48400   25.81111   19.56429   13.50000 
## land rover    lincoln    mercury     nissan    pontiac     subaru     toyota 
##   11.50000   11.33333   13.25000   18.82308   17.00000   20.24286   19.31176 
## volkswagen 
##   22.06667
```

I tend to always loop over integer indices, but some people loop over the actual values instead. Either way is acceptable, but sometimes within the loop you need to index another variable by the same loop index value (i in the example above) and in that case you need to loop over integers instead of actual values.

### Apply Family

The apply family of functions provide an alternative to for loops that are typically much faster to execute because of underlying efficiencies in R. They are also less verbose typically, but the downside is that they are more difficult to understand.

#### tapply

tapply is a very handy tool for doing an operation on one variable over the values of another -- exactly what we did in our for loop above


```r
tapply(mpg$cty, mpg$manufacturer, mean)
```

```
##      honda     toyota     nissan     subaru    hyundai       audi land rover 
##   25.81111   19.31176   18.82308   20.24286   19.56429   18.32778   11.50000 
## volkswagen  chevrolet      dodge       ford       jeep    lincoln    mercury 
##   22.06667   15.24737   13.36216   14.48400   13.50000   11.33333   13.25000 
##    pontiac 
##   17.00000
```

To perform more complex operations, you have to use a different syntax


```r
tapply(mpg$cty, mpg$manufacturer, FUN = function(i) mean(i)/sd(i))
```

```
##      honda     toyota     nissan     subaru    hyundai       audi land rover 
##   9.969016   4.371707   4.786692  16.138060  11.253376   7.295285  19.918584 
## volkswagen  chevrolet      dodge       ford       jeep    lincoln    mercury 
##   4.205244   5.011688   5.449192   6.184588   5.384637  19.629909  26.500000 
##    pontiac 
##  17.000000
```

#### lapply and sapply

lapply and sapply are more flexible versions of tapply in that you aren't limited to performing the operations on a single column in a dataframe. These can completely replace for loops and are quite a bit faster. lapply always returns a list and is very efficient at looping over list items, but can be used in other scenarios as well. sapply works similarly but attempts to return the simplest possible object as output (a vector when possible).

##### Simple Examples

Here's a very simple example to show the syntax of lapply


```r
lapply(1:3, FUN = function(x) x+1)
```

```
## [[1]]
## [1] 2
## 
## [[2]]
## [1] 3
## 
## [[3]]
## [1] 4
```

The syntax for sapply is similar, but it tries to return the simplest possible object -- in this case a vector


```r
sapply(1:3, FUN = function(x) x+1)
```

```
## [1] 2 3 4
```

##### More Complex Examples

For example, let's find the  difference in average city MPG for manual and automatic transmissions for each year in our dataset.


```r
#Loop over unique values of year and define the function to apply
lapply(unique(mpg$year), FUN = function(x) {
  
  #First, create a temporary subset of the data consisting only of the xth unique year
  temp.dat <- mpg[mpg$year == x ,]
  
  #Next, calculate the difference in city MPG between manual transmissions and automatic transmissions by subsetting with grep
  mpg.diff <- mean(temp.dat$cty[grep("manual", temp.dat$trans)], na.rm = T) -
              mean(temp.dat$cty[grep("auto", temp.dat$trans)], na.rm = T)
  
  #Output the year (x) as an integer and the MPG difference we calculated as columns
  return(cbind(as.integer(x), mpg.diff))
})
```

```
## [[1]]
##           mpg.diff
## [1,] 1999 4.800566
## 
## [[2]]
##           mpg.diff
## [1,] 2008 4.297342
```

Notice that the output is a list denoted by the double square brakets [[]]. We can wrangle this into a matrix by wrapping the loop in the handy the do.call() function and specifying to row bind the list items.


```r
#Wrap the lapply loop in do.call to combine the resulting list by row binding them, implicitly creating a matrix..
do.call('rbind', 
  #Loop over unique values of year and define the function to apply
  lapply(unique(mpg$year), FUN = function(x) {
    
    #First, create a temporary subset of the data consisting only of the xth unique year
    temp.dat <- mpg[mpg$year == x ,]
    
    #Next, calculate the difference in city MPG between manual transmissions and automatic transmissions by subsetting with grep
    mpg.diff <- mean(temp.dat$cty[grep("manual", temp.dat$trans)], na.rm = T) -
                mean(temp.dat$cty[grep("auto", temp.dat$trans)], na.rm = T)
    
    #Output the year (x) as an integer and the MPG difference we calculated as columns
    return(cbind(as.integer(x), mpg.diff))
  })
)
```

```
##           mpg.diff
## [1,] 1999 4.800566
## [2,] 2008 4.297342
```

sapply can simplify the output without using do.call() but it column binds the output by default instead of row binding it.


```r
sapply(unique(mpg$year), FUN = function(x) {
  
  #First, create a temporary subset of the data consisting only of the xth unique year
  temp.dat <- mpg[mpg$year == x ,]
  
  #Next, calculate the difference in city MPG between manual transmissions and automatic transmissions by subsetting with grep
  mpg.diff <- mean(temp.dat$cty[grep("manual", temp.dat$trans)], na.rm = T) -
              mean(temp.dat$cty[grep("auto", temp.dat$trans)], na.rm = T)
  
  #Output the year (x) as an integer and the MPG difference we calculated as columns
  return(cbind(as.integer(x), mpg.diff))
})
```

```
##             [,1]        [,2]
## [1,] 1999.000000 2008.000000
## [2,]    4.800566    4.297342
```

#### lapply on a list

If you already have a list of items, lapply is very useful to modify them.

First, let's make a list where each list item is a dataframe corresponding to a single manufacturer using lapply.


```r
mpg.manufac <- lapply(unique(mpg$manufacturer), FUN = function(x) {
  mpg[mpg$manufacturer == x,]
})

#Make the names of the list items their corresponding manufacturer
names(mpg.manufac) <- unique(mpg$manufacturer)
```

Let's take a look at the first list item -- it should be a dataframe containing a single manufacturer


```r
mpg.manufac[[1]]
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["manufacturer"],"name":[1],"type":["fct"],"align":["left"]},{"label":["model"],"name":[2],"type":["fct"],"align":["left"]},{"label":["displ"],"name":[3],"type":["dbl"],"align":["right"]},{"label":["year"],"name":[4],"type":["int"],"align":["right"]},{"label":["cyl"],"name":[5],"type":["int"],"align":["right"]},{"label":["trans"],"name":[6],"type":["chr"],"align":["left"]},{"label":["drv"],"name":[7],"type":["chr"],"align":["left"]},{"label":["cty"],"name":[8],"type":["dbl"],"align":["right"]},{"label":["hwy"],"name":[9],"type":["int"],"align":["right"]},{"label":["fl"],"name":[10],"type":["chr"],"align":["left"]},{"label":["class"],"name":[11],"type":["chr"],"align":["left"]},{"label":["gears"],"name":[12],"type":["chr"],"align":["left"]},{"label":["vol.per.cyl"],"name":[13],"type":["dbl"],"align":["right"]}],"data":[{"1":"audi","2":"a4","3":"1.8","4":"1999","5":"4","6":"auto(l5)","7":"f","8":"18.0","9":"29","10":"p","11":"compact","12":"5","13":"0.4500000","_rn_":"1"},{"1":"audi","2":"a4","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"23.1","9":"29","10":"p","11":"compact","12":"5","13":"0.4500000","_rn_":"2"},{"1":"audi","2":"a4","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"22.0","9":"31","10":"p","11":"compact","12":"6","13":"0.5000000","_rn_":"3"},{"1":"audi","2":"a4","3":"2.0","4":"2008","5":"4","6":"auto(av)","7":"f","8":"21.0","9":"30","10":"p","11":"compact","12":"NA","13":"0.5000000","_rn_":"4"},{"1":"audi","2":"a4","3":"2.8","4":"1999","5":"6","6":"auto(l5)","7":"f","8":"16.0","9":"26","10":"p","11":"compact","12":"5","13":"0.4666667","_rn_":"5"},{"1":"audi","2":"a4","3":"2.8","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"19.8","9":"26","10":"p","11":"compact","12":"5","13":"0.4666667","_rn_":"6"},{"1":"audi","2":"a4","3":"3.1","4":"2008","5":"6","6":"auto(av)","7":"f","8":"18.0","9":"27","10":"p","11":"compact","12":"NA","13":"0.5166667","_rn_":"7"},{"1":"audi","2":"a4 quattro","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"19.8","9":"26","10":"p","11":"compact","12":"5","13":"0.4500000","_rn_":"8"},{"1":"audi","2":"a4 quattro","3":"1.8","4":"1999","5":"4","6":"auto(l5)","7":"4","8":"16.0","9":"25","10":"p","11":"compact","12":"5","13":"0.4500000","_rn_":"9"},{"1":"audi","2":"a4 quattro","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"4","8":"22.0","9":"28","10":"p","11":"compact","12":"6","13":"0.5000000","_rn_":"10"},{"1":"audi","2":"a4 quattro","3":"2.0","4":"2008","5":"4","6":"auto(s6)","7":"4","8":"19.0","9":"27","10":"p","11":"compact","12":"6","13":"0.5000000","_rn_":"11"},{"1":"audi","2":"a4 quattro","3":"2.8","4":"1999","5":"6","6":"auto(l5)","7":"4","8":"15.0","9":"25","10":"p","11":"compact","12":"5","13":"0.4666667","_rn_":"12"},{"1":"audi","2":"a4 quattro","3":"2.8","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"18.7","9":"25","10":"p","11":"compact","12":"5","13":"0.4666667","_rn_":"13"},{"1":"audi","2":"a4 quattro","3":"3.1","4":"2008","5":"6","6":"auto(s6)","7":"4","8":"17.0","9":"25","10":"p","11":"compact","12":"6","13":"0.5166667","_rn_":"14"},{"1":"audi","2":"a4 quattro","3":"3.1","4":"2008","5":"6","6":"manual(m6)","7":"4","8":"16.5","9":"25","10":"p","11":"compact","12":"6","13":"0.5166667","_rn_":"15"},{"1":"audi","2":"a6 quattro","3":"2.8","4":"1999","5":"6","6":"auto(l5)","7":"4","8":"15.0","9":"24","10":"p","11":"midsize","12":"5","13":"0.4666667","_rn_":"16"},{"1":"audi","2":"a6 quattro","3":"3.1","4":"2008","5":"6","6":"auto(s6)","7":"4","8":"17.0","9":"25","10":"p","11":"midsize","12":"6","13":"0.5166667","_rn_":"17"},{"1":"audi","2":"a6 quattro","3":"4.2","4":"2008","5":"8","6":"auto(s6)","7":"4","8":"16.0","9":"23","10":"p","11":"midsize","12":"6","13":"0.5250000","_rn_":"18"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

Now let's say we want to find the difference in city MPG between each car for a manufacturer and that manufacturer's mean city MPG, then save it as a new column in each of the manufacturer's dataframes in the list


```r
mpg.manufac <- lapply(mpg.manufac, FUN = function(x) {
  x$cty.diff <- x$cty - mean(x$cty)
  x
})
```

Now let's look at Honda's dataframe to see the outcome


```r
mpg.manufac[['honda']][, c("manufacturer", "model", "year", "cty", "cty.diff")]
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["manufacturer"],"name":[1],"type":["fct"],"align":["left"]},{"label":["model"],"name":[2],"type":["fct"],"align":["left"]},{"label":["year"],"name":[3],"type":["int"],"align":["right"]},{"label":["cty"],"name":[4],"type":["dbl"],"align":["right"]},{"label":["cty.diff"],"name":[5],"type":["dbl"],"align":["right"]}],"data":[{"1":"honda","2":"civic","3":"1999","4":"30.8","5":"4.9888889","_rn_":"100"},{"1":"honda","2":"civic","3":"1999","4":"24.0","5":"-1.8111111","_rn_":"101"},{"1":"honda","2":"civic","3":"1999","4":"27.5","5":"1.6888889","_rn_":"102"},{"1":"honda","2":"civic","3":"1999","4":"25.3","5":"-0.5111111","_rn_":"103"},{"1":"honda","2":"civic","3":"1999","4":"24.0","5":"-1.8111111","_rn_":"104"},{"1":"honda","2":"civic","3":"2008","4":"28.6","5":"2.7888889","_rn_":"105"},{"1":"honda","2":"civic","3":"2008","4":"25.0","5":"-0.8111111","_rn_":"106"},{"1":"honda","2":"civic","3":"2008","4":"24.0","5":"-1.8111111","_rn_":"107"},{"1":"honda","2":"civic","3":"2008","4":"23.1","5":"-2.7111111","_rn_":"108"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

##### Combining a List of Dataframes into One Dataframe

We could then recombine these list items into a large dataframe using do.call('rbind')


```r
as.data.frame(
  do.call('rbind', mpg.manufac)
)
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["manufacturer"],"name":[1],"type":["fct"],"align":["left"]},{"label":["model"],"name":[2],"type":["fct"],"align":["left"]},{"label":["displ"],"name":[3],"type":["dbl"],"align":["right"]},{"label":["year"],"name":[4],"type":["int"],"align":["right"]},{"label":["cyl"],"name":[5],"type":["int"],"align":["right"]},{"label":["trans"],"name":[6],"type":["chr"],"align":["left"]},{"label":["drv"],"name":[7],"type":["chr"],"align":["left"]},{"label":["cty"],"name":[8],"type":["dbl"],"align":["right"]},{"label":["hwy"],"name":[9],"type":["int"],"align":["right"]},{"label":["fl"],"name":[10],"type":["chr"],"align":["left"]},{"label":["class"],"name":[11],"type":["chr"],"align":["left"]},{"label":["gears"],"name":[12],"type":["chr"],"align":["left"]},{"label":["vol.per.cyl"],"name":[13],"type":["dbl"],"align":["right"]},{"label":["cty.diff"],"name":[14],"type":["dbl"],"align":["right"]}],"data":[{"1":"audi","2":"a4","3":"1.8","4":"1999","5":"4","6":"auto(l5)","7":"f","8":"18.0","9":"29","10":"p","11":"compact","12":"5","13":"0.4500000","14":"-0.32777778","_rn_":"audi.1"},{"1":"audi","2":"a4","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"23.1","9":"29","10":"p","11":"compact","12":"5","13":"0.4500000","14":"4.77222222","_rn_":"audi.2"},{"1":"audi","2":"a4","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"22.0","9":"31","10":"p","11":"compact","12":"6","13":"0.5000000","14":"3.67222222","_rn_":"audi.3"},{"1":"audi","2":"a4","3":"2.0","4":"2008","5":"4","6":"auto(av)","7":"f","8":"21.0","9":"30","10":"p","11":"compact","12":"NA","13":"0.5000000","14":"2.67222222","_rn_":"audi.4"},{"1":"audi","2":"a4","3":"2.8","4":"1999","5":"6","6":"auto(l5)","7":"f","8":"16.0","9":"26","10":"p","11":"compact","12":"5","13":"0.4666667","14":"-2.32777778","_rn_":"audi.5"},{"1":"audi","2":"a4","3":"2.8","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"19.8","9":"26","10":"p","11":"compact","12":"5","13":"0.4666667","14":"1.47222222","_rn_":"audi.6"},{"1":"audi","2":"a4","3":"3.1","4":"2008","5":"6","6":"auto(av)","7":"f","8":"18.0","9":"27","10":"p","11":"compact","12":"NA","13":"0.5166667","14":"-0.32777778","_rn_":"audi.7"},{"1":"audi","2":"a4 quattro","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"19.8","9":"26","10":"p","11":"compact","12":"5","13":"0.4500000","14":"1.47222222","_rn_":"audi.8"},{"1":"audi","2":"a4 quattro","3":"1.8","4":"1999","5":"4","6":"auto(l5)","7":"4","8":"16.0","9":"25","10":"p","11":"compact","12":"5","13":"0.4500000","14":"-2.32777778","_rn_":"audi.9"},{"1":"audi","2":"a4 quattro","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"4","8":"22.0","9":"28","10":"p","11":"compact","12":"6","13":"0.5000000","14":"3.67222222","_rn_":"audi.10"},{"1":"audi","2":"a4 quattro","3":"2.0","4":"2008","5":"4","6":"auto(s6)","7":"4","8":"19.0","9":"27","10":"p","11":"compact","12":"6","13":"0.5000000","14":"0.67222222","_rn_":"audi.11"},{"1":"audi","2":"a4 quattro","3":"2.8","4":"1999","5":"6","6":"auto(l5)","7":"4","8":"15.0","9":"25","10":"p","11":"compact","12":"5","13":"0.4666667","14":"-3.32777778","_rn_":"audi.12"},{"1":"audi","2":"a4 quattro","3":"2.8","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"18.7","9":"25","10":"p","11":"compact","12":"5","13":"0.4666667","14":"0.37222222","_rn_":"audi.13"},{"1":"audi","2":"a4 quattro","3":"3.1","4":"2008","5":"6","6":"auto(s6)","7":"4","8":"17.0","9":"25","10":"p","11":"compact","12":"6","13":"0.5166667","14":"-1.32777778","_rn_":"audi.14"},{"1":"audi","2":"a4 quattro","3":"3.1","4":"2008","5":"6","6":"manual(m6)","7":"4","8":"16.5","9":"25","10":"p","11":"compact","12":"6","13":"0.5166667","14":"-1.82777778","_rn_":"audi.15"},{"1":"audi","2":"a6 quattro","3":"2.8","4":"1999","5":"6","6":"auto(l5)","7":"4","8":"15.0","9":"24","10":"p","11":"midsize","12":"5","13":"0.4666667","14":"-3.32777778","_rn_":"audi.16"},{"1":"audi","2":"a6 quattro","3":"3.1","4":"2008","5":"6","6":"auto(s6)","7":"4","8":"17.0","9":"25","10":"p","11":"midsize","12":"6","13":"0.5166667","14":"-1.32777778","_rn_":"audi.17"},{"1":"audi","2":"a6 quattro","3":"4.2","4":"2008","5":"8","6":"auto(s6)","7":"4","8":"16.0","9":"23","10":"p","11":"midsize","12":"6","13":"0.5250000","14":"-2.32777778","_rn_":"audi.18"},{"1":"chevrolet","2":"c1500 suburban 2wd","3":"5.3","4":"2008","5":"8","6":"auto(l4)","7":"r","8":"14.0","9":"20","10":"r","11":"suv","12":"4","13":"0.6625000","14":"-1.24736842","_rn_":"chevrolet.19"},{"1":"chevrolet","2":"c1500 suburban 2wd","3":"5.3","4":"2008","5":"8","6":"auto(l4)","7":"r","8":"11.0","9":"15","10":"e","11":"suv","12":"4","13":"0.6625000","14":"-4.24736842","_rn_":"chevrolet.20"},{"1":"chevrolet","2":"c1500 suburban 2wd","3":"5.3","4":"2008","5":"8","6":"auto(l4)","7":"r","8":"14.0","9":"20","10":"r","11":"suv","12":"4","13":"0.6625000","14":"-1.24736842","_rn_":"chevrolet.21"},{"1":"chevrolet","2":"c1500 suburban 2wd","3":"5.7","4":"1999","5":"8","6":"auto(l4)","7":"r","8":"13.0","9":"17","10":"r","11":"suv","12":"4","13":"0.7125000","14":"-2.24736842","_rn_":"chevrolet.22"},{"1":"chevrolet","2":"c1500 suburban 2wd","3":"6.0","4":"2008","5":"8","6":"auto(l4)","7":"r","8":"12.0","9":"17","10":"r","11":"suv","12":"4","13":"0.7500000","14":"-3.24736842","_rn_":"chevrolet.23"},{"1":"chevrolet","2":"corvette","3":"5.7","4":"1999","5":"8","6":"manual(m6)","7":"r","8":"17.6","9":"26","10":"p","11":"2seater","12":"6","13":"0.7125000","14":"2.35263158","_rn_":"chevrolet.24"},{"1":"chevrolet","2":"corvette","3":"5.7","4":"1999","5":"8","6":"auto(l4)","7":"r","8":"15.0","9":"23","10":"p","11":"2seater","12":"4","13":"0.7125000","14":"-0.24736842","_rn_":"chevrolet.25"},{"1":"chevrolet","2":"corvette","3":"6.2","4":"2008","5":"8","6":"manual(m6)","7":"r","8":"17.6","9":"26","10":"p","11":"2seater","12":"6","13":"0.7750000","14":"2.35263158","_rn_":"chevrolet.26"},{"1":"chevrolet","2":"corvette","3":"6.2","4":"2008","5":"8","6":"auto(s6)","7":"r","8":"15.0","9":"25","10":"p","11":"2seater","12":"6","13":"0.7750000","14":"-0.24736842","_rn_":"chevrolet.27"},{"1":"chevrolet","2":"corvette","3":"7.0","4":"2008","5":"8","6":"manual(m6)","7":"r","8":"16.5","9":"24","10":"p","11":"2seater","12":"6","13":"0.8750000","14":"1.25263158","_rn_":"chevrolet.28"},{"1":"chevrolet","2":"k1500 tahoe 4wd","3":"5.3","4":"2008","5":"8","6":"auto(l4)","7":"4","8":"14.0","9":"19","10":"r","11":"suv","12":"4","13":"0.6625000","14":"-1.24736842","_rn_":"chevrolet.29"},{"1":"chevrolet","2":"k1500 tahoe 4wd","3":"5.3","4":"2008","5":"8","6":"auto(l4)","7":"4","8":"11.0","9":"14","10":"e","11":"suv","12":"4","13":"0.6625000","14":"-4.24736842","_rn_":"chevrolet.30"},{"1":"chevrolet","2":"k1500 tahoe 4wd","3":"5.7","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11.0","9":"15","10":"r","11":"suv","12":"4","13":"0.7125000","14":"-4.24736842","_rn_":"chevrolet.31"},{"1":"chevrolet","2":"k1500 tahoe 4wd","3":"6.5","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"14.0","9":"17","10":"d","11":"suv","12":"4","13":"0.8125000","14":"-1.24736842","_rn_":"chevrolet.32"},{"1":"chevrolet","2":"malibu","3":"2.4","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19.0","9":"27","10":"r","11":"midsize","12":"4","13":"0.6000000","14":"3.75263158","_rn_":"chevrolet.33"},{"1":"chevrolet","2":"malibu","3":"2.4","4":"2008","5":"4","6":"auto(l4)","7":"f","8":"22.0","9":"30","10":"r","11":"midsize","12":"4","13":"0.6000000","14":"6.75263158","_rn_":"chevrolet.34"},{"1":"chevrolet","2":"malibu","3":"3.1","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"18.0","9":"26","10":"r","11":"midsize","12":"4","13":"0.5166667","14":"2.75263158","_rn_":"chevrolet.35"},{"1":"chevrolet","2":"malibu","3":"3.5","4":"2008","5":"6","6":"auto(l4)","7":"f","8":"18.0","9":"29","10":"r","11":"midsize","12":"4","13":"0.5833333","14":"2.75263158","_rn_":"chevrolet.36"},{"1":"chevrolet","2":"malibu","3":"3.6","4":"2008","5":"6","6":"auto(s6)","7":"f","8":"17.0","9":"26","10":"r","11":"midsize","12":"6","13":"0.6000000","14":"1.75263158","_rn_":"chevrolet.37"},{"1":"dodge","2":"caravan 2wd","3":"2.4","4":"1999","5":"4","6":"auto(l3)","7":"f","8":"18.0","9":"24","10":"r","11":"minivan","12":"3","13":"0.6000000","14":"4.63783784","_rn_":"dodge.38"},{"1":"dodge","2":"caravan 2wd","3":"3.0","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"17.0","9":"24","10":"r","11":"minivan","12":"4","13":"0.5000000","14":"3.63783784","_rn_":"dodge.39"},{"1":"dodge","2":"caravan 2wd","3":"3.3","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"16.0","9":"22","10":"r","11":"minivan","12":"4","13":"0.5500000","14":"2.63783784","_rn_":"dodge.40"},{"1":"dodge","2":"caravan 2wd","3":"3.3","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"16.0","9":"22","10":"r","11":"minivan","12":"4","13":"0.5500000","14":"2.63783784","_rn_":"dodge.41"},{"1":"dodge","2":"caravan 2wd","3":"3.3","4":"2008","5":"6","6":"auto(l4)","7":"f","8":"17.0","9":"24","10":"r","11":"minivan","12":"4","13":"0.5500000","14":"3.63783784","_rn_":"dodge.42"},{"1":"dodge","2":"caravan 2wd","3":"3.3","4":"2008","5":"6","6":"auto(l4)","7":"f","8":"17.0","9":"24","10":"r","11":"minivan","12":"4","13":"0.5500000","14":"3.63783784","_rn_":"dodge.43"},{"1":"dodge","2":"caravan 2wd","3":"3.3","4":"2008","5":"6","6":"auto(l4)","7":"f","8":"11.0","9":"17","10":"e","11":"minivan","12":"4","13":"0.5500000","14":"-2.36216216","_rn_":"dodge.44"},{"1":"dodge","2":"caravan 2wd","3":"3.8","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"15.0","9":"22","10":"r","11":"minivan","12":"4","13":"0.6333333","14":"1.63783784","_rn_":"dodge.45"},{"1":"dodge","2":"caravan 2wd","3":"3.8","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"15.0","9":"21","10":"r","11":"minivan","12":"4","13":"0.6333333","14":"1.63783784","_rn_":"dodge.46"},{"1":"dodge","2":"caravan 2wd","3":"3.8","4":"2008","5":"6","6":"auto(l6)","7":"f","8":"16.0","9":"23","10":"r","11":"minivan","12":"6","13":"0.6333333","14":"2.63783784","_rn_":"dodge.47"},{"1":"dodge","2":"caravan 2wd","3":"4.0","4":"2008","5":"6","6":"auto(l6)","7":"f","8":"16.0","9":"23","10":"r","11":"minivan","12":"6","13":"0.6666667","14":"2.63783784","_rn_":"dodge.48"},{"1":"dodge","2":"dakota pickup 4wd","3":"3.7","4":"2008","5":"6","6":"manual(m6)","7":"4","8":"16.5","9":"19","10":"r","11":"pickup","12":"6","13":"0.6166667","14":"3.13783784","_rn_":"dodge.49"},{"1":"dodge","2":"dakota pickup 4wd","3":"3.7","4":"2008","5":"6","6":"auto(l4)","7":"4","8":"14.0","9":"18","10":"r","11":"pickup","12":"4","13":"0.6166667","14":"0.63783784","_rn_":"dodge.50"},{"1":"dodge","2":"dakota pickup 4wd","3":"3.9","4":"1999","5":"6","6":"auto(l4)","7":"4","8":"13.0","9":"17","10":"r","11":"pickup","12":"4","13":"0.6500000","14":"-0.36216216","_rn_":"dodge.51"},{"1":"dodge","2":"dakota pickup 4wd","3":"3.9","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"15.4","9":"17","10":"r","11":"pickup","12":"5","13":"0.6500000","14":"2.03783784","_rn_":"dodge.52"},{"1":"dodge","2":"dakota pickup 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"14.0","9":"19","10":"r","11":"pickup","12":"5","13":"0.5875000","14":"0.63783784","_rn_":"dodge.53"},{"1":"dodge","2":"dakota pickup 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"14.0","9":"19","10":"r","11":"pickup","12":"5","13":"0.5875000","14":"0.63783784","_rn_":"dodge.54"},{"1":"dodge","2":"dakota pickup 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"9.0","9":"12","10":"e","11":"pickup","12":"5","13":"0.5875000","14":"-4.36216216","_rn_":"dodge.55"},{"1":"dodge","2":"dakota pickup 4wd","3":"5.2","4":"1999","5":"8","6":"manual(m5)","7":"4","8":"12.1","9":"17","10":"r","11":"pickup","12":"5","13":"0.6500000","14":"-1.26216216","_rn_":"dodge.56"},{"1":"dodge","2":"dakota pickup 4wd","3":"5.2","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11.0","9":"15","10":"r","11":"pickup","12":"4","13":"0.6500000","14":"-2.36216216","_rn_":"dodge.57"},{"1":"dodge","2":"durango 4wd","3":"3.9","4":"1999","5":"6","6":"auto(l4)","7":"4","8":"13.0","9":"17","10":"r","11":"suv","12":"4","13":"0.6500000","14":"-0.36216216","_rn_":"dodge.58"},{"1":"dodge","2":"durango 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"13.0","9":"17","10":"r","11":"suv","12":"5","13":"0.5875000","14":"-0.36216216","_rn_":"dodge.59"},{"1":"dodge","2":"durango 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"9.0","9":"12","10":"e","11":"suv","12":"5","13":"0.5875000","14":"-4.36216216","_rn_":"dodge.60"},{"1":"dodge","2":"durango 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"13.0","9":"17","10":"r","11":"suv","12":"5","13":"0.5875000","14":"-0.36216216","_rn_":"dodge.61"},{"1":"dodge","2":"durango 4wd","3":"5.2","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11.0","9":"16","10":"r","11":"suv","12":"4","13":"0.6500000","14":"-2.36216216","_rn_":"dodge.62"},{"1":"dodge","2":"durango 4wd","3":"5.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"13.0","9":"18","10":"r","11":"suv","12":"5","13":"0.7125000","14":"-0.36216216","_rn_":"dodge.63"},{"1":"dodge","2":"durango 4wd","3":"5.9","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11.0","9":"15","10":"r","11":"suv","12":"4","13":"0.7375000","14":"-2.36216216","_rn_":"dodge.64"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"manual(m6)","7":"4","8":"13.2","9":"16","10":"r","11":"pickup","12":"6","13":"0.5875000","14":"-0.16216216","_rn_":"dodge.65"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"9.0","9":"12","10":"e","11":"pickup","12":"5","13":"0.5875000","14":"-4.36216216","_rn_":"dodge.66"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"13.0","9":"17","10":"r","11":"pickup","12":"5","13":"0.5875000","14":"-0.36216216","_rn_":"dodge.67"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"13.0","9":"17","10":"r","11":"pickup","12":"5","13":"0.5875000","14":"-0.36216216","_rn_":"dodge.68"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"manual(m6)","7":"4","8":"13.2","9":"16","10":"r","11":"pickup","12":"6","13":"0.5875000","14":"-0.16216216","_rn_":"dodge.69"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"4.7","4":"2008","5":"8","6":"manual(m6)","7":"4","8":"9.9","9":"12","10":"e","11":"pickup","12":"6","13":"0.5875000","14":"-3.46216216","_rn_":"dodge.70"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"5.2","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11.0","9":"15","10":"r","11":"pickup","12":"4","13":"0.6500000","14":"-2.36216216","_rn_":"dodge.71"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"5.2","4":"1999","5":"8","6":"manual(m5)","7":"4","8":"12.1","9":"16","10":"r","11":"pickup","12":"5","13":"0.6500000","14":"-1.26216216","_rn_":"dodge.72"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"5.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"13.0","9":"17","10":"r","11":"pickup","12":"5","13":"0.7125000","14":"-0.36216216","_rn_":"dodge.73"},{"1":"dodge","2":"ram 1500 pickup 4wd","3":"5.9","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11.0","9":"15","10":"r","11":"pickup","12":"4","13":"0.7375000","14":"-2.36216216","_rn_":"dodge.74"},{"1":"ford","2":"expedition 2wd","3":"4.6","4":"1999","5":"8","6":"auto(l4)","7":"r","8":"11.0","9":"17","10":"r","11":"suv","12":"4","13":"0.5750000","14":"-3.48400000","_rn_":"ford.75"},{"1":"ford","2":"expedition 2wd","3":"5.4","4":"1999","5":"8","6":"auto(l4)","7":"r","8":"11.0","9":"17","10":"r","11":"suv","12":"4","13":"0.6750000","14":"-3.48400000","_rn_":"ford.76"},{"1":"ford","2":"expedition 2wd","3":"5.4","4":"2008","5":"8","6":"auto(l6)","7":"r","8":"12.0","9":"18","10":"r","11":"suv","12":"6","13":"0.6750000","14":"-2.48400000","_rn_":"ford.77"},{"1":"ford","2":"explorer 4wd","3":"4.0","4":"1999","5":"6","6":"auto(l5)","7":"4","8":"14.0","9":"17","10":"r","11":"suv","12":"5","13":"0.6666667","14":"-0.48400000","_rn_":"ford.78"},{"1":"ford","2":"explorer 4wd","3":"4.0","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"16.5","9":"19","10":"r","11":"suv","12":"5","13":"0.6666667","14":"2.01600000","_rn_":"ford.79"},{"1":"ford","2":"explorer 4wd","3":"4.0","4":"1999","5":"6","6":"auto(l5)","7":"4","8":"14.0","9":"17","10":"r","11":"suv","12":"5","13":"0.6666667","14":"-0.48400000","_rn_":"ford.80"},{"1":"ford","2":"explorer 4wd","3":"4.0","4":"2008","5":"6","6":"auto(l5)","7":"4","8":"13.0","9":"19","10":"r","11":"suv","12":"5","13":"0.6666667","14":"-1.48400000","_rn_":"ford.81"},{"1":"ford","2":"explorer 4wd","3":"4.6","4":"2008","5":"8","6":"auto(l6)","7":"4","8":"13.0","9":"19","10":"r","11":"suv","12":"6","13":"0.5750000","14":"-1.48400000","_rn_":"ford.82"},{"1":"ford","2":"explorer 4wd","3":"5.0","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"13.0","9":"17","10":"r","11":"suv","12":"4","13":"0.6250000","14":"-1.48400000","_rn_":"ford.83"},{"1":"ford","2":"f150 pickup 4wd","3":"4.2","4":"1999","5":"6","6":"auto(l4)","7":"4","8":"14.0","9":"17","10":"r","11":"pickup","12":"4","13":"0.7000000","14":"-0.48400000","_rn_":"ford.84"},{"1":"ford","2":"f150 pickup 4wd","3":"4.2","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"15.4","9":"17","10":"r","11":"pickup","12":"5","13":"0.7000000","14":"0.91600000","_rn_":"ford.85"},{"1":"ford","2":"f150 pickup 4wd","3":"4.6","4":"1999","5":"8","6":"manual(m5)","7":"4","8":"14.3","9":"16","10":"r","11":"pickup","12":"5","13":"0.5750000","14":"-0.18400000","_rn_":"ford.86"},{"1":"ford","2":"f150 pickup 4wd","3":"4.6","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"13.0","9":"16","10":"r","11":"pickup","12":"4","13":"0.5750000","14":"-1.48400000","_rn_":"ford.87"},{"1":"ford","2":"f150 pickup 4wd","3":"4.6","4":"2008","5":"8","6":"auto(l4)","7":"4","8":"13.0","9":"17","10":"r","11":"pickup","12":"4","13":"0.5750000","14":"-1.48400000","_rn_":"ford.88"},{"1":"ford","2":"f150 pickup 4wd","3":"5.4","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11.0","9":"15","10":"r","11":"pickup","12":"4","13":"0.6750000","14":"-3.48400000","_rn_":"ford.89"},{"1":"ford","2":"f150 pickup 4wd","3":"5.4","4":"2008","5":"8","6":"auto(l4)","7":"4","8":"13.0","9":"17","10":"r","11":"pickup","12":"4","13":"0.6750000","14":"-1.48400000","_rn_":"ford.90"},{"1":"ford","2":"mustang","3":"3.8","4":"1999","5":"6","6":"manual(m5)","7":"r","8":"19.8","9":"26","10":"r","11":"subcompact","12":"5","13":"0.6333333","14":"5.31600000","_rn_":"ford.91"},{"1":"ford","2":"mustang","3":"3.8","4":"1999","5":"6","6":"auto(l4)","7":"r","8":"18.0","9":"25","10":"r","11":"subcompact","12":"4","13":"0.6333333","14":"3.51600000","_rn_":"ford.92"},{"1":"ford","2":"mustang","3":"4.0","4":"2008","5":"6","6":"manual(m5)","7":"r","8":"18.7","9":"26","10":"r","11":"subcompact","12":"5","13":"0.6666667","14":"4.21600000","_rn_":"ford.93"},{"1":"ford","2":"mustang","3":"4.0","4":"2008","5":"6","6":"auto(l5)","7":"r","8":"16.0","9":"24","10":"r","11":"subcompact","12":"5","13":"0.6666667","14":"1.51600000","_rn_":"ford.94"},{"1":"ford","2":"mustang","3":"4.6","4":"1999","5":"8","6":"auto(l4)","7":"r","8":"15.0","9":"21","10":"r","11":"subcompact","12":"4","13":"0.5750000","14":"0.51600000","_rn_":"ford.95"},{"1":"ford","2":"mustang","3":"4.6","4":"1999","5":"8","6":"manual(m5)","7":"r","8":"16.5","9":"22","10":"r","11":"subcompact","12":"5","13":"0.5750000","14":"2.01600000","_rn_":"ford.96"},{"1":"ford","2":"mustang","3":"4.6","4":"2008","5":"8","6":"manual(m5)","7":"r","8":"16.5","9":"23","10":"r","11":"subcompact","12":"5","13":"0.5750000","14":"2.01600000","_rn_":"ford.97"},{"1":"ford","2":"mustang","3":"4.6","4":"2008","5":"8","6":"auto(l5)","7":"r","8":"15.0","9":"22","10":"r","11":"subcompact","12":"5","13":"0.5750000","14":"0.51600000","_rn_":"ford.98"},{"1":"ford","2":"mustang","3":"5.4","4":"2008","5":"8","6":"manual(m6)","7":"r","8":"15.4","9":"20","10":"p","11":"subcompact","12":"6","13":"0.6750000","14":"0.91600000","_rn_":"ford.99"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"30.8","9":"33","10":"r","11":"subcompact","12":"5","13":"0.4000000","14":"4.98888889","_rn_":"honda.100"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"24.0","9":"32","10":"r","11":"subcompact","12":"4","13":"0.4000000","14":"-1.81111111","_rn_":"honda.101"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"27.5","9":"32","10":"r","11":"subcompact","12":"5","13":"0.4000000","14":"1.68888889","_rn_":"honda.102"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"25.3","9":"29","10":"p","11":"subcompact","12":"5","13":"0.4000000","14":"-0.51111111","_rn_":"honda.103"},{"1":"honda","2":"civic","3":"1.6","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"24.0","9":"32","10":"r","11":"subcompact","12":"4","13":"0.4000000","14":"-1.81111111","_rn_":"honda.104"},{"1":"honda","2":"civic","3":"1.8","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"28.6","9":"34","10":"r","11":"subcompact","12":"5","13":"0.4500000","14":"2.78888889","_rn_":"honda.105"},{"1":"honda","2":"civic","3":"1.8","4":"2008","5":"4","6":"auto(l5)","7":"f","8":"25.0","9":"36","10":"r","11":"subcompact","12":"5","13":"0.4500000","14":"-0.81111111","_rn_":"honda.106"},{"1":"honda","2":"civic","3":"1.8","4":"2008","5":"4","6":"auto(l5)","7":"f","8":"24.0","9":"36","10":"c","11":"subcompact","12":"5","13":"0.4500000","14":"-1.81111111","_rn_":"honda.107"},{"1":"honda","2":"civic","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"23.1","9":"29","10":"p","11":"subcompact","12":"6","13":"0.5000000","14":"-2.71111111","_rn_":"honda.108"},{"1":"hyundai","2":"sonata","3":"2.4","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"18.0","9":"26","10":"r","11":"midsize","12":"4","13":"0.6000000","14":"-1.56428571","_rn_":"hyundai.109"},{"1":"hyundai","2":"sonata","3":"2.4","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"19.8","9":"27","10":"r","11":"midsize","12":"5","13":"0.6000000","14":"0.23571429","_rn_":"hyundai.110"},{"1":"hyundai","2":"sonata","3":"2.4","4":"2008","5":"4","6":"auto(l4)","7":"f","8":"21.0","9":"30","10":"r","11":"midsize","12":"4","13":"0.6000000","14":"1.43571429","_rn_":"hyundai.111"},{"1":"hyundai","2":"sonata","3":"2.4","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"23.1","9":"31","10":"r","11":"midsize","12":"5","13":"0.6000000","14":"3.53571429","_rn_":"hyundai.112"},{"1":"hyundai","2":"sonata","3":"2.5","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"18.0","9":"26","10":"r","11":"midsize","12":"4","13":"0.4166667","14":"-1.56428571","_rn_":"hyundai.113"},{"1":"hyundai","2":"sonata","3":"2.5","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"19.8","9":"26","10":"r","11":"midsize","12":"5","13":"0.4166667","14":"0.23571429","_rn_":"hyundai.114"},{"1":"hyundai","2":"sonata","3":"3.3","4":"2008","5":"6","6":"auto(l5)","7":"f","8":"19.0","9":"28","10":"r","11":"midsize","12":"5","13":"0.5500000","14":"-0.56428571","_rn_":"hyundai.115"},{"1":"hyundai","2":"tiburon","3":"2.0","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19.0","9":"26","10":"r","11":"subcompact","12":"4","13":"0.5000000","14":"-0.56428571","_rn_":"hyundai.116"},{"1":"hyundai","2":"tiburon","3":"2.0","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"20.9","9":"29","10":"r","11":"subcompact","12":"5","13":"0.5000000","14":"1.33571429","_rn_":"hyundai.117"},{"1":"hyundai","2":"tiburon","3":"2.0","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"22.0","9":"28","10":"r","11":"subcompact","12":"5","13":"0.5000000","14":"2.43571429","_rn_":"hyundai.118"},{"1":"hyundai","2":"tiburon","3":"2.0","4":"2008","5":"4","6":"auto(l4)","7":"f","8":"20.0","9":"27","10":"r","11":"subcompact","12":"4","13":"0.5000000","14":"0.43571429","_rn_":"hyundai.119"},{"1":"hyundai","2":"tiburon","3":"2.7","4":"2008","5":"6","6":"auto(l4)","7":"f","8":"17.0","9":"24","10":"r","11":"subcompact","12":"4","13":"0.4500000","14":"-2.56428571","_rn_":"hyundai.120"},{"1":"hyundai","2":"tiburon","3":"2.7","4":"2008","5":"6","6":"manual(m6)","7":"f","8":"17.6","9":"24","10":"r","11":"subcompact","12":"6","13":"0.4500000","14":"-1.96428571","_rn_":"hyundai.121"},{"1":"hyundai","2":"tiburon","3":"2.7","4":"2008","5":"6","6":"manual(m5)","7":"f","8":"18.7","9":"24","10":"r","11":"subcompact","12":"5","13":"0.4500000","14":"-0.86428571","_rn_":"hyundai.122"},{"1":"jeep","2":"grand cherokee 4wd","3":"3.0","4":"2008","5":"6","6":"auto(l5)","7":"4","8":"17.0","9":"22","10":"d","11":"suv","12":"5","13":"0.5000000","14":"3.50000000","_rn_":"jeep.123"},{"1":"jeep","2":"grand cherokee 4wd","3":"3.7","4":"2008","5":"6","6":"auto(l5)","7":"4","8":"15.0","9":"19","10":"r","11":"suv","12":"5","13":"0.6166667","14":"1.50000000","_rn_":"jeep.124"},{"1":"jeep","2":"grand cherokee 4wd","3":"4.0","4":"1999","5":"6","6":"auto(l4)","7":"4","8":"15.0","9":"20","10":"r","11":"suv","12":"4","13":"0.6666667","14":"1.50000000","_rn_":"jeep.125"},{"1":"jeep","2":"grand cherokee 4wd","3":"4.7","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"14.0","9":"17","10":"r","11":"suv","12":"4","13":"0.5875000","14":"0.50000000","_rn_":"jeep.126"},{"1":"jeep","2":"grand cherokee 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"9.0","9":"12","10":"e","11":"suv","12":"5","13":"0.5875000","14":"-4.50000000","_rn_":"jeep.127"},{"1":"jeep","2":"grand cherokee 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"14.0","9":"19","10":"r","11":"suv","12":"5","13":"0.5875000","14":"0.50000000","_rn_":"jeep.128"},{"1":"jeep","2":"grand cherokee 4wd","3":"5.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"13.0","9":"18","10":"r","11":"suv","12":"5","13":"0.7125000","14":"-0.50000000","_rn_":"jeep.129"},{"1":"jeep","2":"grand cherokee 4wd","3":"6.1","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"11.0","9":"14","10":"p","11":"suv","12":"5","13":"0.7625000","14":"-2.50000000","_rn_":"jeep.130"},{"1":"land rover","2":"range rover","3":"4.0","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11.0","9":"15","10":"p","11":"suv","12":"4","13":"0.5000000","14":"-0.50000000","_rn_":"land rover.131"},{"1":"land rover","2":"range rover","3":"4.2","4":"2008","5":"8","6":"auto(s6)","7":"4","8":"12.0","9":"18","10":"r","11":"suv","12":"6","13":"0.5250000","14":"0.50000000","_rn_":"land rover.132"},{"1":"land rover","2":"range rover","3":"4.4","4":"2008","5":"8","6":"auto(s6)","7":"4","8":"12.0","9":"18","10":"r","11":"suv","12":"6","13":"0.5500000","14":"0.50000000","_rn_":"land rover.133"},{"1":"land rover","2":"range rover","3":"4.6","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11.0","9":"15","10":"p","11":"suv","12":"4","13":"0.5750000","14":"-0.50000000","_rn_":"land rover.134"},{"1":"lincoln","2":"navigator 2wd","3":"5.4","4":"1999","5":"8","6":"auto(l4)","7":"r","8":"11.0","9":"17","10":"r","11":"suv","12":"4","13":"0.6750000","14":"-0.33333333","_rn_":"lincoln.135"},{"1":"lincoln","2":"navigator 2wd","3":"5.4","4":"1999","5":"8","6":"auto(l4)","7":"r","8":"11.0","9":"16","10":"p","11":"suv","12":"4","13":"0.6750000","14":"-0.33333333","_rn_":"lincoln.136"},{"1":"lincoln","2":"navigator 2wd","3":"5.4","4":"2008","5":"8","6":"auto(l6)","7":"r","8":"12.0","9":"18","10":"r","11":"suv","12":"6","13":"0.6750000","14":"0.66666667","_rn_":"lincoln.137"},{"1":"mercury","2":"mountaineer 4wd","3":"4.0","4":"1999","5":"6","6":"auto(l5)","7":"4","8":"14.0","9":"17","10":"r","11":"suv","12":"5","13":"0.6666667","14":"0.75000000","_rn_":"mercury.138"},{"1":"mercury","2":"mountaineer 4wd","3":"4.0","4":"2008","5":"6","6":"auto(l5)","7":"4","8":"13.0","9":"19","10":"r","11":"suv","12":"5","13":"0.6666667","14":"-0.25000000","_rn_":"mercury.139"},{"1":"mercury","2":"mountaineer 4wd","3":"4.6","4":"2008","5":"8","6":"auto(l6)","7":"4","8":"13.0","9":"19","10":"r","11":"suv","12":"6","13":"0.5750000","14":"-0.25000000","_rn_":"mercury.140"},{"1":"mercury","2":"mountaineer 4wd","3":"5.0","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"13.0","9":"17","10":"r","11":"suv","12":"4","13":"0.6250000","14":"-0.25000000","_rn_":"mercury.141"},{"1":"nissan","2":"altima","3":"2.4","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"23.1","9":"29","10":"r","11":"compact","12":"5","13":"0.6000000","14":"4.27692308","_rn_":"nissan.142"},{"1":"nissan","2":"altima","3":"2.4","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19.0","9":"27","10":"r","11":"compact","12":"4","13":"0.6000000","14":"0.17692308","_rn_":"nissan.143"},{"1":"nissan","2":"altima","3":"2.5","4":"2008","5":"4","6":"auto(av)","7":"f","8":"23.0","9":"31","10":"r","11":"midsize","12":"NA","13":"0.6250000","14":"4.17692308","_rn_":"nissan.144"},{"1":"nissan","2":"altima","3":"2.5","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"25.3","9":"32","10":"r","11":"midsize","12":"6","13":"0.6250000","14":"6.47692308","_rn_":"nissan.145"},{"1":"nissan","2":"altima","3":"3.5","4":"2008","5":"6","6":"manual(m6)","7":"f","8":"20.9","9":"27","10":"p","11":"midsize","12":"6","13":"0.5833333","14":"2.07692308","_rn_":"nissan.146"},{"1":"nissan","2":"altima","3":"3.5","4":"2008","5":"6","6":"auto(av)","7":"f","8":"19.0","9":"26","10":"p","11":"midsize","12":"NA","13":"0.5833333","14":"0.17692308","_rn_":"nissan.147"},{"1":"nissan","2":"maxima","3":"3.0","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"18.0","9":"26","10":"r","11":"midsize","12":"4","13":"0.5000000","14":"-0.82307692","_rn_":"nissan.148"},{"1":"nissan","2":"maxima","3":"3.0","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"20.9","9":"25","10":"r","11":"midsize","12":"5","13":"0.5000000","14":"2.07692308","_rn_":"nissan.149"},{"1":"nissan","2":"maxima","3":"3.5","4":"2008","5":"6","6":"auto(av)","7":"f","8":"19.0","9":"25","10":"p","11":"midsize","12":"NA","13":"0.5833333","14":"0.17692308","_rn_":"nissan.150"},{"1":"nissan","2":"pathfinder 4wd","3":"3.3","4":"1999","5":"6","6":"auto(l4)","7":"4","8":"14.0","9":"17","10":"r","11":"suv","12":"4","13":"0.5500000","14":"-4.82307692","_rn_":"nissan.151"},{"1":"nissan","2":"pathfinder 4wd","3":"3.3","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"16.5","9":"17","10":"r","11":"suv","12":"5","13":"0.5500000","14":"-2.32307692","_rn_":"nissan.152"},{"1":"nissan","2":"pathfinder 4wd","3":"4.0","4":"2008","5":"6","6":"auto(l5)","7":"4","8":"14.0","9":"20","10":"p","11":"suv","12":"5","13":"0.6666667","14":"-4.82307692","_rn_":"nissan.153"},{"1":"nissan","2":"pathfinder 4wd","3":"5.6","4":"2008","5":"8","6":"auto(s5)","7":"4","8":"12.0","9":"18","10":"p","11":"suv","12":"5","13":"0.7000000","14":"-6.82307692","_rn_":"nissan.154"},{"1":"pontiac","2":"grand prix","3":"3.1","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"18.0","9":"26","10":"r","11":"midsize","12":"4","13":"0.5166667","14":"1.00000000","_rn_":"pontiac.155"},{"1":"pontiac","2":"grand prix","3":"3.8","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"16.0","9":"26","10":"p","11":"midsize","12":"4","13":"0.6333333","14":"-1.00000000","_rn_":"pontiac.156"},{"1":"pontiac","2":"grand prix","3":"3.8","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"17.0","9":"27","10":"r","11":"midsize","12":"4","13":"0.6333333","14":"0.00000000","_rn_":"pontiac.157"},{"1":"pontiac","2":"grand prix","3":"3.8","4":"2008","5":"6","6":"auto(l4)","7":"f","8":"18.0","9":"28","10":"r","11":"midsize","12":"4","13":"0.6333333","14":"1.00000000","_rn_":"pontiac.158"},{"1":"pontiac","2":"grand prix","3":"5.3","4":"2008","5":"8","6":"auto(s4)","7":"f","8":"16.0","9":"25","10":"p","11":"midsize","12":"4","13":"0.6625000","14":"-1.00000000","_rn_":"pontiac.159"},{"1":"subaru","2":"forester awd","3":"2.5","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"19.8","9":"25","10":"r","11":"suv","12":"5","13":"0.6250000","14":"-0.44285714","_rn_":"subaru.160"},{"1":"subaru","2":"forester awd","3":"2.5","4":"1999","5":"4","6":"auto(l4)","7":"4","8":"18.0","9":"24","10":"r","11":"suv","12":"4","13":"0.6250000","14":"-2.24285714","_rn_":"subaru.161"},{"1":"subaru","2":"forester awd","3":"2.5","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"22.0","9":"27","10":"r","11":"suv","12":"5","13":"0.6250000","14":"1.75714286","_rn_":"subaru.162"},{"1":"subaru","2":"forester awd","3":"2.5","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"20.9","9":"25","10":"p","11":"suv","12":"5","13":"0.6250000","14":"0.65714286","_rn_":"subaru.163"},{"1":"subaru","2":"forester awd","3":"2.5","4":"2008","5":"4","6":"auto(l4)","7":"4","8":"20.0","9":"26","10":"r","11":"suv","12":"4","13":"0.6250000","14":"-0.24285714","_rn_":"subaru.164"},{"1":"subaru","2":"forester awd","3":"2.5","4":"2008","5":"4","6":"auto(l4)","7":"4","8":"18.0","9":"23","10":"p","11":"suv","12":"4","13":"0.6250000","14":"-2.24285714","_rn_":"subaru.165"},{"1":"subaru","2":"impreza awd","3":"2.2","4":"1999","5":"4","6":"auto(l4)","7":"4","8":"21.0","9":"26","10":"r","11":"subcompact","12":"4","13":"0.5500000","14":"0.75714286","_rn_":"subaru.166"},{"1":"subaru","2":"impreza awd","3":"2.2","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"20.9","9":"26","10":"r","11":"subcompact","12":"5","13":"0.5500000","14":"0.65714286","_rn_":"subaru.167"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"20.9","9":"26","10":"r","11":"subcompact","12":"5","13":"0.6250000","14":"0.65714286","_rn_":"subaru.168"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"1999","5":"4","6":"auto(l4)","7":"4","8":"19.0","9":"26","10":"r","11":"subcompact","12":"4","13":"0.6250000","14":"-1.24285714","_rn_":"subaru.169"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"2008","5":"4","6":"auto(s4)","7":"4","8":"20.0","9":"25","10":"p","11":"compact","12":"4","13":"0.6250000","14":"-0.24285714","_rn_":"subaru.170"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"2008","5":"4","6":"auto(s4)","7":"4","8":"20.0","9":"27","10":"r","11":"compact","12":"4","13":"0.6250000","14":"-0.24285714","_rn_":"subaru.171"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"20.9","9":"25","10":"p","11":"compact","12":"5","13":"0.6250000","14":"0.65714286","_rn_":"subaru.172"},{"1":"subaru","2":"impreza awd","3":"2.5","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"22.0","9":"27","10":"r","11":"compact","12":"5","13":"0.6250000","14":"1.75714286","_rn_":"subaru.173"},{"1":"toyota","2":"4runner 4wd","3":"2.7","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"16.5","9":"20","10":"r","11":"suv","12":"5","13":"0.6750000","14":"-2.81176471","_rn_":"toyota.174"},{"1":"toyota","2":"4runner 4wd","3":"2.7","4":"1999","5":"4","6":"auto(l4)","7":"4","8":"16.0","9":"20","10":"r","11":"suv","12":"4","13":"0.6750000","14":"-3.31176471","_rn_":"toyota.175"},{"1":"toyota","2":"4runner 4wd","3":"3.4","4":"1999","5":"6","6":"auto(l4)","7":"4","8":"15.0","9":"19","10":"r","11":"suv","12":"4","13":"0.5666667","14":"-4.31176471","_rn_":"toyota.176"},{"1":"toyota","2":"4runner 4wd","3":"3.4","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"16.5","9":"17","10":"r","11":"suv","12":"5","13":"0.5666667","14":"-2.81176471","_rn_":"toyota.177"},{"1":"toyota","2":"4runner 4wd","3":"4.0","4":"2008","5":"6","6":"auto(l5)","7":"4","8":"16.0","9":"20","10":"r","11":"suv","12":"5","13":"0.6666667","14":"-3.31176471","_rn_":"toyota.178"},{"1":"toyota","2":"4runner 4wd","3":"4.7","4":"2008","5":"8","6":"auto(l5)","7":"4","8":"14.0","9":"17","10":"r","11":"suv","12":"5","13":"0.5875000","14":"-5.31176471","_rn_":"toyota.179"},{"1":"toyota","2":"camry","3":"2.2","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"23.1","9":"29","10":"r","11":"midsize","12":"5","13":"0.5500000","14":"3.78823529","_rn_":"toyota.180"},{"1":"toyota","2":"camry","3":"2.2","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"21.0","9":"27","10":"r","11":"midsize","12":"4","13":"0.5500000","14":"1.68823529","_rn_":"toyota.181"},{"1":"toyota","2":"camry","3":"2.4","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"23.1","9":"31","10":"r","11":"midsize","12":"5","13":"0.6000000","14":"3.78823529","_rn_":"toyota.182"},{"1":"toyota","2":"camry","3":"2.4","4":"2008","5":"4","6":"auto(l5)","7":"f","8":"21.0","9":"31","10":"r","11":"midsize","12":"5","13":"0.6000000","14":"1.68823529","_rn_":"toyota.183"},{"1":"toyota","2":"camry","3":"3.0","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"18.0","9":"26","10":"r","11":"midsize","12":"4","13":"0.5000000","14":"-1.31176471","_rn_":"toyota.184"},{"1":"toyota","2":"camry","3":"3.0","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"19.8","9":"26","10":"r","11":"midsize","12":"5","13":"0.5000000","14":"0.48823529","_rn_":"toyota.185"},{"1":"toyota","2":"camry","3":"3.5","4":"2008","5":"6","6":"auto(s6)","7":"f","8":"19.0","9":"28","10":"r","11":"midsize","12":"6","13":"0.5833333","14":"-0.31176471","_rn_":"toyota.186"},{"1":"toyota","2":"camry solara","3":"2.2","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"21.0","9":"27","10":"r","11":"compact","12":"4","13":"0.5500000","14":"1.68823529","_rn_":"toyota.187"},{"1":"toyota","2":"camry solara","3":"2.2","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"23.1","9":"29","10":"r","11":"compact","12":"5","13":"0.5500000","14":"3.78823529","_rn_":"toyota.188"},{"1":"toyota","2":"camry solara","3":"2.4","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"23.1","9":"31","10":"r","11":"compact","12":"5","13":"0.6000000","14":"3.78823529","_rn_":"toyota.189"},{"1":"toyota","2":"camry solara","3":"2.4","4":"2008","5":"4","6":"auto(s5)","7":"f","8":"22.0","9":"31","10":"r","11":"compact","12":"5","13":"0.6000000","14":"2.68823529","_rn_":"toyota.190"},{"1":"toyota","2":"camry solara","3":"3.0","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"18.0","9":"26","10":"r","11":"compact","12":"4","13":"0.5000000","14":"-1.31176471","_rn_":"toyota.191"},{"1":"toyota","2":"camry solara","3":"3.0","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"19.8","9":"26","10":"r","11":"compact","12":"5","13":"0.5000000","14":"0.48823529","_rn_":"toyota.192"},{"1":"toyota","2":"camry solara","3":"3.3","4":"2008","5":"6","6":"auto(s5)","7":"f","8":"18.0","9":"27","10":"r","11":"compact","12":"5","13":"0.5500000","14":"-1.31176471","_rn_":"toyota.193"},{"1":"toyota","2":"corolla","3":"1.8","4":"1999","5":"4","6":"auto(l3)","7":"f","8":"24.0","9":"30","10":"r","11":"compact","12":"3","13":"0.4500000","14":"4.68823529","_rn_":"toyota.194"},{"1":"toyota","2":"corolla","3":"1.8","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"24.0","9":"33","10":"r","11":"compact","12":"4","13":"0.4500000","14":"4.68823529","_rn_":"toyota.195"},{"1":"toyota","2":"corolla","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"28.6","9":"35","10":"r","11":"compact","12":"5","13":"0.4500000","14":"9.28823529","_rn_":"toyota.196"},{"1":"toyota","2":"corolla","3":"1.8","4":"2008","5":"4","6":"manual(m5)","7":"f","8":"30.8","9":"37","10":"r","11":"compact","12":"5","13":"0.4500000","14":"11.48823529","_rn_":"toyota.197"},{"1":"toyota","2":"corolla","3":"1.8","4":"2008","5":"4","6":"auto(l4)","7":"f","8":"26.0","9":"35","10":"r","11":"compact","12":"4","13":"0.4500000","14":"6.68823529","_rn_":"toyota.198"},{"1":"toyota","2":"land cruiser wagon 4wd","3":"4.7","4":"1999","5":"8","6":"auto(l4)","7":"4","8":"11.0","9":"15","10":"r","11":"suv","12":"4","13":"0.5875000","14":"-8.31176471","_rn_":"toyota.199"},{"1":"toyota","2":"land cruiser wagon 4wd","3":"5.7","4":"2008","5":"8","6":"auto(s6)","7":"4","8":"13.0","9":"18","10":"r","11":"suv","12":"6","13":"0.7125000","14":"-6.31176471","_rn_":"toyota.200"},{"1":"toyota","2":"toyota tacoma 4wd","3":"2.7","4":"1999","5":"4","6":"manual(m5)","7":"4","8":"16.5","9":"20","10":"r","11":"pickup","12":"5","13":"0.6750000","14":"-2.81176471","_rn_":"toyota.201"},{"1":"toyota","2":"toyota tacoma 4wd","3":"2.7","4":"1999","5":"4","6":"auto(l4)","7":"4","8":"16.0","9":"20","10":"r","11":"pickup","12":"4","13":"0.6750000","14":"-3.31176471","_rn_":"toyota.202"},{"1":"toyota","2":"toyota tacoma 4wd","3":"2.7","4":"2008","5":"4","6":"manual(m5)","7":"4","8":"18.7","9":"22","10":"r","11":"pickup","12":"5","13":"0.6750000","14":"-0.61176471","_rn_":"toyota.203"},{"1":"toyota","2":"toyota tacoma 4wd","3":"3.4","4":"1999","5":"6","6":"manual(m5)","7":"4","8":"16.5","9":"17","10":"r","11":"pickup","12":"5","13":"0.5666667","14":"-2.81176471","_rn_":"toyota.204"},{"1":"toyota","2":"toyota tacoma 4wd","3":"3.4","4":"1999","5":"6","6":"auto(l4)","7":"4","8":"15.0","9":"19","10":"r","11":"pickup","12":"4","13":"0.5666667","14":"-4.31176471","_rn_":"toyota.205"},{"1":"toyota","2":"toyota tacoma 4wd","3":"4.0","4":"2008","5":"6","6":"manual(m6)","7":"4","8":"16.5","9":"18","10":"r","11":"pickup","12":"6","13":"0.6666667","14":"-2.81176471","_rn_":"toyota.206"},{"1":"toyota","2":"toyota tacoma 4wd","3":"4.0","4":"2008","5":"6","6":"auto(l5)","7":"4","8":"16.0","9":"20","10":"r","11":"pickup","12":"5","13":"0.6666667","14":"-3.31176471","_rn_":"toyota.207"},{"1":"volkswagen","2":"gti","3":"2.0","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"23.1","9":"29","10":"r","11":"compact","12":"5","13":"0.5000000","14":"1.03333333","_rn_":"volkswagen.208"},{"1":"volkswagen","2":"gti","3":"2.0","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19.0","9":"26","10":"r","11":"compact","12":"4","13":"0.5000000","14":"-3.06666667","_rn_":"volkswagen.209"},{"1":"volkswagen","2":"gti","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"23.1","9":"29","10":"p","11":"compact","12":"6","13":"0.5000000","14":"1.03333333","_rn_":"volkswagen.210"},{"1":"volkswagen","2":"gti","3":"2.0","4":"2008","5":"4","6":"auto(s6)","7":"f","8":"22.0","9":"29","10":"p","11":"compact","12":"6","13":"0.5000000","14":"-0.06666667","_rn_":"volkswagen.211"},{"1":"volkswagen","2":"gti","3":"2.8","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"18.7","9":"24","10":"r","11":"compact","12":"5","13":"0.4666667","14":"-3.36666667","_rn_":"volkswagen.212"},{"1":"volkswagen","2":"jetta","3":"1.9","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"36.3","9":"44","10":"d","11":"compact","12":"5","13":"0.4750000","14":"14.23333333","_rn_":"volkswagen.213"},{"1":"volkswagen","2":"jetta","3":"2.0","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"23.1","9":"29","10":"r","11":"compact","12":"5","13":"0.5000000","14":"1.03333333","_rn_":"volkswagen.214"},{"1":"volkswagen","2":"jetta","3":"2.0","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19.0","9":"26","10":"r","11":"compact","12":"4","13":"0.5000000","14":"-3.06666667","_rn_":"volkswagen.215"},{"1":"volkswagen","2":"jetta","3":"2.0","4":"2008","5":"4","6":"auto(s6)","7":"f","8":"22.0","9":"29","10":"p","11":"compact","12":"6","13":"0.5000000","14":"-0.06666667","_rn_":"volkswagen.216"},{"1":"volkswagen","2":"jetta","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"23.1","9":"29","10":"p","11":"compact","12":"6","13":"0.5000000","14":"1.03333333","_rn_":"volkswagen.217"},{"1":"volkswagen","2":"jetta","3":"2.5","4":"2008","5":"5","6":"auto(s6)","7":"f","8":"21.0","9":"29","10":"r","11":"compact","12":"6","13":"0.5000000","14":"-1.06666667","_rn_":"volkswagen.218"},{"1":"volkswagen","2":"jetta","3":"2.5","4":"2008","5":"5","6":"manual(m5)","7":"f","8":"23.1","9":"29","10":"r","11":"compact","12":"5","13":"0.5000000","14":"1.03333333","_rn_":"volkswagen.219"},{"1":"volkswagen","2":"jetta","3":"2.8","4":"1999","5":"6","6":"auto(l4)","7":"f","8":"16.0","9":"23","10":"r","11":"compact","12":"4","13":"0.4666667","14":"-6.06666667","_rn_":"volkswagen.220"},{"1":"volkswagen","2":"jetta","3":"2.8","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"18.7","9":"24","10":"r","11":"compact","12":"5","13":"0.4666667","14":"-3.36666667","_rn_":"volkswagen.221"},{"1":"volkswagen","2":"new beetle","3":"1.9","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"38.5","9":"44","10":"d","11":"subcompact","12":"5","13":"0.4750000","14":"16.43333333","_rn_":"volkswagen.222"},{"1":"volkswagen","2":"new beetle","3":"1.9","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"29.0","9":"41","10":"d","11":"subcompact","12":"4","13":"0.4750000","14":"6.93333333","_rn_":"volkswagen.223"},{"1":"volkswagen","2":"new beetle","3":"2.0","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"23.1","9":"29","10":"r","11":"subcompact","12":"5","13":"0.5000000","14":"1.03333333","_rn_":"volkswagen.224"},{"1":"volkswagen","2":"new beetle","3":"2.0","4":"1999","5":"4","6":"auto(l4)","7":"f","8":"19.0","9":"26","10":"r","11":"subcompact","12":"4","13":"0.5000000","14":"-3.06666667","_rn_":"volkswagen.225"},{"1":"volkswagen","2":"new beetle","3":"2.5","4":"2008","5":"5","6":"manual(m5)","7":"f","8":"22.0","9":"28","10":"r","11":"subcompact","12":"5","13":"0.5000000","14":"-0.06666667","_rn_":"volkswagen.226"},{"1":"volkswagen","2":"new beetle","3":"2.5","4":"2008","5":"5","6":"auto(s6)","7":"f","8":"20.0","9":"29","10":"r","11":"subcompact","12":"6","13":"0.5000000","14":"-2.06666667","_rn_":"volkswagen.227"},{"1":"volkswagen","2":"passat","3":"1.8","4":"1999","5":"4","6":"manual(m5)","7":"f","8":"23.1","9":"29","10":"p","11":"midsize","12":"5","13":"0.4500000","14":"1.03333333","_rn_":"volkswagen.228"},{"1":"volkswagen","2":"passat","3":"1.8","4":"1999","5":"4","6":"auto(l5)","7":"f","8":"18.0","9":"29","10":"p","11":"midsize","12":"5","13":"0.4500000","14":"-4.06666667","_rn_":"volkswagen.229"},{"1":"volkswagen","2":"passat","3":"2.0","4":"2008","5":"4","6":"auto(s6)","7":"f","8":"19.0","9":"28","10":"p","11":"midsize","12":"6","13":"0.5000000","14":"-3.06666667","_rn_":"volkswagen.230"},{"1":"volkswagen","2":"passat","3":"2.0","4":"2008","5":"4","6":"manual(m6)","7":"f","8":"23.1","9":"29","10":"p","11":"midsize","12":"6","13":"0.5000000","14":"1.03333333","_rn_":"volkswagen.231"},{"1":"volkswagen","2":"passat","3":"2.8","4":"1999","5":"6","6":"auto(l5)","7":"f","8":"16.0","9":"26","10":"p","11":"midsize","12":"5","13":"0.4666667","14":"-6.06666667","_rn_":"volkswagen.232"},{"1":"volkswagen","2":"passat","3":"2.8","4":"1999","5":"6","6":"manual(m5)","7":"f","8":"19.8","9":"26","10":"p","11":"midsize","12":"5","13":"0.4666667","14":"-2.26666667","_rn_":"volkswagen.233"},{"1":"volkswagen","2":"passat","3":"3.6","4":"2008","5":"6","6":"auto(s6)","7":"f","8":"17.0","9":"26","10":"p","11":"midsize","12":"6","13":"0.6000000","14":"-5.06666667","_rn_":"volkswagen.234"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

#### apply

The main purpose of apply is to loop over either rows or columns of a matrix or dataframe. Use MARGIN = 1 for rows and MARGIN = 2 for columns

First, let's make a matrix from just the numeric variables in the MPG dataset


```r
mpg.mat <- as.matrix(mpg[, c("displ", "cyl", "cty", "hwy", "vol.per.cyl")])
```

Now let's find the means of each of these variables by looping over the columns with apply


```r
apply(mpg.mat, MARGIN = 2, FUN = function(x) mean(x))
```

```
##       displ         cyl         cty         hwy vol.per.cyl 
##   3.4717949   5.8888889  17.4735043  23.4401709   0.5779736
```

##### Using a pre-specified function

So far, we've been declaring our custom function within the call the apply/lapply/sapply function, but we could also define the function beforehand and then apply it later.

Define a function to calculate mean and SD, then concatenate them using the paste0() function.


```r
meanSD <- function(x) {
  meanx <- mean(x)
  sdx <- sd(x)
  meanSDstring <- paste0(round(meanx, 2), " (", round(sdx, 2), ")")
}
```

Now apply the function to the mpg.mat matrix we created above.


```r
apply(mpg.mat, MARGIN = 2, FUN = meanSD)
```

```
##          displ            cyl            cty            hwy    vol.per.cyl 
##  "3.47 (1.29)"  "5.89 (1.61)" "17.47 (4.74)" "23.44 (5.95)"  "0.58 (0.09)"
```

Note that this will work for all other functions in the apply family as well.


# Appendix

## Matrix Algebra

R can perform matrix algebra very efficiently -- let's set up a simulated regression dataset and find Y using matrix multiplication with %\*%


```r
#Set a seed to obtain reproducible results
set.seed(53711)

#Simulated X matrix
X <- as.matrix(
  ncols = 3, nrows = 100,
  cbind(
    rep(1, 100),
    rnorm(n=100),
    rnorm(n=100)
  )
)

#Population beta coefficients
beta <- c(0.5, 1.2, -0.8)

#Vector of normal errors
epsilon <- rnorm(n=100, sd=0.2)

#Create Y vector as XB + e using matrix multiplication
Y <- X %*% beta + epsilon

#Run a multiple linear regression to estimate betas
coef(lm(Y ~ X[,2] + X[,3]))
```

```
## (Intercept)      X[, 2]      X[, 3] 
##   0.4838470   1.2045071  -0.8037617
```
